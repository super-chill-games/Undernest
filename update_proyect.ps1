$UndernestDir = Get-Location
$UndernestUProject = Join-Path "$UndernestDir" "Undernest.uproject"

echo "Updating Undernest repository ($UndernestDir)"

git fetch
git pull

Set-Location ../scgplugins

$SCGPlugins = Get-Location
echo "Changing location to $SCGPlugins"
echo "Updating Plugins repository"

git fetch
git pull

Set-Location $UndernestDir
echo "Will regenerate solution"

$UE5Path = (Get-ItemProperty 'Registry::HKEY_LOCAL_MACHINE\SOFTWARE\EpicGames\Unreal Engine\5.1' -Name 'InstalledDirectory').'InstalledDirectory'.Replace('"', '')
$PathToBuildBat = (Join-Path "$UE5Path" "Engine/Build/BatchFiles/Build.bat")
Start-Process -FilePath "$PathToBuildBat" -Wait -ArgumentList "-projectfiles -project=$UndernestUProject -game -rocket"

if (Test-Path "Saved") {
}
else {
    mkdir "Saved"
}
$CompileSTDOutput = (Join-Path "$UndernestDir" "Saved/last_compile_std.txt")
$CompileSTDErr = (Join-Path "$UndernestDir" "Saved/last_compile_err.txt")

if (Test-Path $CompileSTDOutput) {
    Remove-Item $CompileSTDOutput
}

if (Test-Path $CompileSTDErr) {
    Remove-Item $CompileSTDErr
}

$UATPath = (Join-Path "$UE5Path" "Engine/Build/BatchFiles/RunUAT.bat")
$UATProcess = (Start-Process -FilePath "$UATPath" -RedirectStandardOutput $CompileSTDOutput -RedirectStandardError $CompileSTDErr -PassThru -Wait -ArgumentList "BuildEditor -Project=$UndernestUProject -Target=`"UndernestEditor DebugGame Win64`" -Configuration=DebugGame -Platform=Win64 -WaitMutex -FromMsBuild")

$UATExitCode = $UATProcess.ExitCode
echo "Build terminated with code $UATExitCode"
if ($UATExitCode -eq 0) {
    echo "Launching Undernest..."
    $DebugGameEditor = (Join-Path "$UE5Path" "Engine/Binaries/Win64/UnrealEditor-Win64-DebugGame.exe")
    Start-Process -FilePath "$DebugGameEditor" -Wait -ArgumentList "-log $UndernestUProject"
}
else {
    echo "There were errors in the compilation phase. You can see the log at $CompileSTDOutput"
}


