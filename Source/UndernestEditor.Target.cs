// Copyright Super Chill Games. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UndernestEditorTarget : TargetRules
{
	public UndernestEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Undernest", "UndernestEditor", } );
		
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
	}
}
