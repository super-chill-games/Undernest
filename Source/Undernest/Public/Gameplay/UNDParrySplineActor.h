﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types/PMTCollisionTypes.h"
#include "UNDParrySplineActor.generated.h"

class USplineComponent;
/** Simple actor class used to generate a spline flight when the main character parries an attack. */
UCLASS()
class UNDERNEST_API AUNDParrySplineActor : public AActor
{
	GENERATED_BODY()

public:
	AUNDParrySplineActor();

	/** Generate a flying path and retrieve the spline modified. */
	UFUNCTION(BlueprintCallable, Category = "UND|Parry")
	USplineComponent* GenerateFlyingPath(const AActor* InOwner, const AActor* Target);

	/** Retrieves our spline component. */
	UFUNCTION(BlueprintPure, Category = "UND|Parry")
	USplineComponent* GetSplineComponent() const;

protected:

	/** Project the given location to a proper point where the player can land. */
	FVector ProjectBackLocationToFloor(const AActor* InOwner, const AActor* Target, const FVector& BackLocation);

	/** Max curvature applied to the flight. */
	UPROPERTY(EditAnywhere, Category = "UND", meta = (UIMin = 0.0, ClampMin = 0.0))
	float MaxCurvature = 500.0f;

	/** Distance for the curvature. */
	UPROPERTY(EditAnywhere, Category = "UND", meta = (UIMin = 0.0, ClampMin = 0.0))
	float CurvatureDistance = 200.0f;

	/** Trace config used to detect a floor. */
	UPROPERTY(EditAnywhere, Category = "UND", meta = (UIMin = 0.0, ClampMin = 0.0))
	FPMTCTraceConfiguration FloorTrace;

	/** Spline component we are going to modify to generate a flying path. */
	UPROPERTY(VisibleAnywhere, Category = "UND")
	TObjectPtr<USplineComponent> SplineComponent;
};
