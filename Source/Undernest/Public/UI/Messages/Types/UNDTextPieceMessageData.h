// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

// PMTC Includes
#include "Utilities/PMTCCommonTypes.h"

#include "UNDTextPieceMessageData.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct UNDERNEST_API FUNDTextPieceMessageData : public FPMTCCustomData
{
	GENERATED_BODY()

public:
	
	FUNDTextPieceMessageData() {}

	FUNDTextPieceMessageData(UObject* InOwner, const FText& InMessage) : Owner(InOwner), Message(InMessage) {}

	UScriptStruct* GetScriptStruct() const override
	{
		return FUNDTextPieceMessageData::StaticStruct();
	}

	/** Who sent us, so we can identify later for removal. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UObject> Owner = nullptr;

	/** Message of the text piece. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Message;
};
