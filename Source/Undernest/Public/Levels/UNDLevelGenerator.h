// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

// UE Includes
#include "GameplayTagContainer.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "UNDLevelGenerator.generated.h"

class UUNDLevelDefinition;

DECLARE_LOG_CATEGORY_EXTERN(UNDLevelGeneratorLog, Log, All);

/**
 * Object that takes care of generating levels.
 */
UCLASS(Blueprintable, BlueprintType)
class UNDERNEST_API UUNDLevelGenerator : public UObject
{
	GENERATED_BODY()

public:

	UUNDLevelGenerator();

	/** Retrieves the current area type. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelGenerator", meta = (DefaultToSelf = "WorldContextObject"))
	static FGameplayTag GetCurrentLevelAreaType(const UObject* WorldContextObject);

	/** Retrieves the current level definition. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelGenerator", meta = (DefaultToSelf = "WorldContextObject"))
	static TSubclassOf<UUNDLevelDefinition> GetCurrentLevelDefinition(const UObject* WorldContextObject);

	/** Reset all played levels. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|LevelGenerator")
	void ResetLevelGenerator();

	/** Initialize level manager. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|LevelGenerator")
	void InitializeLevelGenerator();

	/** Reset all exterior levels we have played so far. */
	UFUNCTION(BlueprintCallable, Category = "UND|LevelGenerator")
	void ResetExteriorLevels();

	/** Set the area we are going to be playing with. */
	UFUNCTION(BlueprintCallable, Category = "UND|LevelGenerator")
	void SetCurrentArea(const FGameplayTag& InArea);

	/** Mark the given world as played. This is only called when playing in an editor window,
	* so we can generate properly any level. */
	void MarkLevelAsPlayed(UWorld& World);

	/** Travel to the given level definition. */
	UFUNCTION(BlueprintCallable, Category = "UND|LevelGenerator", meta = (WorldContext = "WorldContextObject"))
	bool TravelToLevel(UObject* WorldContextObject, TSubclassOf<UUNDLevelDefinition> InLevelDefinition, float Delay = 0.0f, bool bTravelAnywayIfRepeated = false);

	/** Retrieves current area. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	const FGameplayTag& GetCurrentArea() const { return CurrentArea; }

	/** Retrieves all those played levels that have the any of the given tags. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TArray<TSubclassOf<UUNDLevelDefinition>> GetPlayedLevelsWithAnyTags(const FGameplayTagContainer& Tags) const;

	/** Tries to generate the next level to play. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TSubclassOf<UUNDLevelDefinition> TryGenerateNextAreaToPlay(FRandomStream Stream) const;

	/** Tries to generate the next exterior area to play. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TSubclassOf<UUNDLevelDefinition> TryGenerateNextExteriorAreaToPlay(FRandomStream Stream) const;

	/** Retrieves tutorial level. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TSubclassOf<UUNDLevelDefinition> GetTutorialLevel() const { return TutorialLevel; }

	/** Retrieves laboratory level. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TSubclassOf<UUNDLevelDefinition> GetLaboratoryLevel() const { return LaboratoryLevel; }

	/** Retrieves hub level. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TSubclassOf<UUNDLevelDefinition> GetHubLevel() const { return HubWorldLevel; }

	/** Retrieves tutorial level. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelManager")
	TSubclassOf<UUNDLevelDefinition> GetWorldSelectorLevel() const { return WorldSelectorLevel; }

	/** Whether we are allowed to reuse rest areas. If we don't,
	* then no more rest areas through the final area will be chosen. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|LevelManager")
	bool CanReuseRestAreas() const;

protected:

	/** Get all levels that have the following tags. */
	TArray<TSubclassOf<UUNDLevelDefinition>> FilterLevelsWithTags(
		const TArray<TSubclassOf<UUNDLevelDefinition>>& InLevelsToFilter,
		const FGameplayTagContainer& Tags) const;

	/** Filter the given levels by having any of the given tags. */
	TArray<TSubclassOf<UUNDLevelDefinition>> FilterLevelsWithAnyTags(
		const TArray<TSubclassOf<UUNDLevelDefinition>>& InLevelsToFilter,
		const FGameplayTagContainer& Tags) const;

	/** Filter the given levels by removing those that were already played. */
	TArray<TSubclassOf<UUNDLevelDefinition>> FilterPlayedLevels(
		const TArray<TSubclassOf<UUNDLevelDefinition>>& InLevelsToFilter) const;

	/** All possible levels we have. Hard reference
	* since we need them to be loaded to retrieve relevant information. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TArray<TSubclassOf<UUNDLevelDefinition>> Levels;

	/** Level used for the tutorial. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TSubclassOf<UUNDLevelDefinition> TutorialLevel;

	/** Definition used for the hub world. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TSubclassOf<UUNDLevelDefinition> HubWorldLevel;

	/** Definition used for the laboratory world. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TSubclassOf<UUNDLevelDefinition> LaboratoryLevel;
	
	/** Level where we are going to find the doors to go to a new kind of area. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TSubclassOf<UUNDLevelDefinition> WorldSelectorLevel;

	/** After how many played levels we should get into a rest area. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	FInt32Range RestAreaPerPlayedLevel;

	/** After how many played levels we should get into a boss area. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	FInt32Range BossAreaPerPlayedLevel;

	/** After how many played levels we should get into the last area. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	FInt32Range FinalAreaPerPlayedLevel;

	/** How many exterior levels we should play before getting into the world selector. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	FInt32Range ExteriorLevels;

	/** Whether we have to choose a rest area after a boss area. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	bool bForceRestAreaAfterBoss = true;

	/** Whether we randomize rest areas. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	bool bRandomizeRestAreas = true;

	/** Whether we randomize combat areas. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	bool bRandomizeCombatAreas = true;

	/** Whether we are allowed to use rest areas if we used all of them. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	bool bCanReuseRestAreas = true;

	/** All levels we have played so far. */
	UPROPERTY(Transient)
	TArray<TSubclassOf<UUNDLevelDefinition>> PlayedLevels;

	/** Track the area we are playing now. */
	FGameplayTag CurrentArea;
};

/**
 * Helper subsystem used to handle the level generation.
 */
UCLASS(Config = Game)
class UNDERNEST_API UUNDLevelGenerationGISubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:

	UUNDLevelGenerationGISubsystem();

	/** Instantiate level generator. */
	void Initialize(FSubsystemCollectionBase& Collection) override;

	/** Reset all data regarding level generation. */
	void ResetLevelGenerationToDefaults();

	/** Retrieves level generator. */
	UFUNCTION(BlueprintPure, Category = "UND|LevelGenerator")
	UUNDLevelGenerator* GetLevelGenerator() const { return LevelGenerator; }

protected:

	/** Class that holds references to required stuff to handle level generation. */
	UPROPERTY(globalconfig, EditAnywhere, Category = "UND", meta = (MetaClass = "/Script/Undernest.UNDLevelGenerator", DisplayName = "Level Generator Class"))
	FSoftClassPath LevelGeneratorClass;

	/** Main object used for level generation. */
	UPROPERTY(Transient)
	TObjectPtr<UUNDLevelGenerator> LevelGenerator = nullptr;
};
