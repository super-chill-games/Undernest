// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Levels/PMTCLevelScriptActor.h"

// UE Includes
#include "GameplayTagContainer.h"

#include "UNDLevelScriptActor.generated.h"

class ALevelSequenceActor;

/**
 * Base class for all levels.
 */
UCLASS()
class UNDERNEST_API AUNDLevelScriptActor : public APMTCLevelScriptActor
{
	GENERATED_BODY()

public:

	AUNDLevelScriptActor();

	/** Notify our corruption state. */
	void PostInitializeComponents() override;

	/** Notify that level got loaded. */
	void BeginPlay() override;

	/** Play the level sequence. */
	UFUNCTION(BlueprintCallable, Category = "UND|Level")
	void RequestPlayIntroLevelSequence();

	/** Retrieves the sequence actor that should be played when this level is entered. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|Level")
	ALevelSequenceActor* GetIntroLevelSequence() const;

	/** Whether we are able to play the intro sequence when this level loads. By
	* default this is true if there's a intro level sequence. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|Level")
	bool ShouldPlayIntroSequence() const;

protected:

	/** Called when the intro level sequence has finished playing. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Level|Sequence")
	void HandleIntroLevelSequenceFinished();

	/** Whether we play the level sequence on begin play, if one exists. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|LevelSequence")
	bool bPlayLevelSequenceOnBeginPlay = true;

	/** Event we are going to send when this level loads. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|GameFlow")
	FGameplayTag GameFlowLevelLoadEvent;

	/** Which state does the corruption start in this level. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Corruption")
	FGameplayTag InitialCorruptionState;
};
