// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

// UE Includes
#include "GameplayTagContainer.h"

// PMTC Includes
#include "Types/PMTSlateTypes.h"

#include "UNDLevelDefinition.generated.h"

class UTexture2D;

/**
 * Holds static data regarding a level. This can be helpful to hold relevant info regarding a level,
 * from text to data needed to spawn/use this level for generation.
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class UNDERNEST_API UUNDLevelDefinition : public UObject
{
	GENERATED_BODY()

public:

	/** Retrieves level. */
	UFUNCTION(BlueprintPure, Category = "UND|Level")
	const TSoftObjectPtr<UWorld>& GetLevel() const { return Level; }

	/** Retrieves level image name. */
	UFUNCTION(BlueprintPure, Category = "UND|Level")
	FPMTSoftSlateBrush GetLevelImageName() const { return LevelImageName; }

	/** Retrieves the name of this level. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|Level")
	FText GetLevelName() const;

	/** Retrieves the description of this level. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|Level")
	FText GetLevelDescription() const;

	/** Retrieves tags that describe this level. */
	UFUNCTION(BlueprintPure, Category = "UND|Level")
	const FGameplayTagContainer& GetDescriptorTags() const { return DescriptorTags; }

protected:

	/** Which level we are referencing. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Level")
	TSoftObjectPtr<UWorld> Level;
	
	/** Level's name. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Metadata")
	FText LevelName;

	/** Level's description. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Metadata")
	FText LevelDescription;

	/** Level's image that show its name.
	* TODO(Brian): This must be removed once we import the proper assets to display the name
	* and localize it. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Metadata")
	FPMTSoftSlateBrush LevelImageName;

	/** Tags that describe this level. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Metadata")
	FGameplayTagContainer DescriptorTags;
};
