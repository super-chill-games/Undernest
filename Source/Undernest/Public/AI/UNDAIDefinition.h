// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AI/PMTAIAIDefinition.h"
#include "UNDAIDefinition.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDAIDefinition : public UPMTAIAIDefinition
{
	GENERATED_BODY()
	
};
