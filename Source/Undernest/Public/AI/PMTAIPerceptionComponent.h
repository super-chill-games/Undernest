// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "PMTAIPerceptionComponent.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UPMTAIPerceptionComponent : public UAIPerceptionComponent
{
	GENERATED_BODY()
	
};
