// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AI/PMTAIBaseAIController.h"
#include "UNDAIController.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API AUNDAIController : public APMTAIBaseAIController
{
	GENERATED_BODY()

public:

	AUNDAIController(const FObjectInitializer& InObjectInitializer);
	
};
