// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Debug/PMTRTUCheatManager.h"
#include "UNDCheatManager.generated.h"

class UGameplayEffect;

/**
 * Hold number of cheats to playtest.
 */
UCLASS()
class UNDERNEST_API UUNDCheatManager : public UPMTRTUCheatManager
{
	GENERATED_BODY()
	
public:

	/** Kill every enemy found in the level. */
	UFUNCTION(BlueprintCallable, exec)
	void KillEveryEnemy();

	/** Make damage to ourself. */
	UFUNCTION(BlueprintCallable, exec)
	void MakeDamage(float Damage);

	/** Make ourselves immunity from everything. */
	UFUNCTION(BlueprintCallable, exec)
	void ToggleGodMode();

protected:

	/** Gameplay effect that will be applied to kill instantly something. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> InstantKillGE;

	/** Gameplay effect that will be applied for self damage. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> SelfDamageGE;

	/** GE that will grant us god mode. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> GodModeGE;
};
