// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

// UE Includes
#include "GameplayTagContainer.h"
#include "Engine/World.h"

#include "UNDCorruptionManager.generated.h"

class UMaterialParameterCollection;

/**
 * Holds static data for corruption and some other utilities.
 */
UCLASS(BlueprintType, Blueprintable)
class UNDERNEST_API UUNDCorruptionManager : public UObject
{
	GENERATED_BODY()

public:

	UUNDCorruptionManager();

	/** Set the current corruption state and notify every actor about this change. */
	UFUNCTION(BlueprintCallable, Category = "UND|Corruption")
	void SetCorruptionState(const FGameplayTag& NewCorruptionState);

	/** Retrieves current corruption state. */
	UFUNCTION(BlueprintPure, Category = "UND|Corruption")
	const FGameplayTag& GetCorruptionState() const { return CurrentCorruptionState; }

	/** Update corruption's radius. */
	UFUNCTION(BlueprintCallable, Category = "UND|Corruption")
	void UpdateCorruptionRadius(float NewRadius);

	/** Update corruption's center. */
	UFUNCTION(BlueprintCallable, Category = "UND|Corruption")
	void UpdateCorruptionCenter(const FVector& Center);

	/** Initialize this manager. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Corruption")
	void InitializeCorruptionManager();

protected:

	/** Called when a corrupted actor has been spawned. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Corruption")
	void HandleCorruptedActorSpawned(AActor* InActor);

	/** Called when a corrupted actor has been destroyed. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Corruption")
	void HandleCorruptedActorDestroyed(AActor* InActor);

	/** Check if it inherits from corruption interface. */
	void OnActorSpawned(AActor* InActor);

	/** Register all corruptable actors. */
	void OnActorsInitialized(const UWorld::FActorsInitializedParams& InitializedParams);

	/** Remove from our array. */
	UFUNCTION()
	void OnCorruptionActorEndPlay(AActor* Actor, EEndPlayReason::Type EndPlayReason);

	/** Parameter collection. */
	UPROPERTY(EditDefaultsOnly, Category = "Material")
	TObjectPtr<UMaterialParameterCollection> CorruptionParameterCollection;

	/** Name of the parameter used to control the corruption radius. */
	UPROPERTY(EditDefaultsOnly, Category = "Material")
	FName CorruptionRadiusParameterName;

	/** Name of the parameter used to control the corruption center. */
	UPROPERTY(EditDefaultsOnly, Category = "Material")
	FName CorruptionCenterParameterName;

	/** Current corruption state. */
	UPROPERTY(EditDefaultsOnly, Category = "Corruption")
	FGameplayTag CurrentCorruptionState;

	/** All actors that inherit from IUNDCorruptionInterface. */
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>> CorruptionActors;
};
