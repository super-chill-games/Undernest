// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Actors/UNDDestructibleActor.h"
#include "UNDCorruptionSourceActor.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API AUNDCorruptionSourceActor : public AUNDDestructibleActor
{
	GENERATED_BODY()
	
};
