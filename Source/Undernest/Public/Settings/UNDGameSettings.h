﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "UNDGameSettings.generated.h"

/**
 * Settings for the game.
 */
UCLASS(Config = Game, defaultconfig)
class UNDERNEST_API UUNDGameSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:

	UUNDGameSettings();

	/** Retrieves the resource multiplier for fungi. */
	UFUNCTION(BlueprintPure, Category = "UND|Settings")
	float GetFungiResourceMultiplier() const { return FungiResourceMultiplier; }

	/** Retrieves the resource multiplier for fungi. */
	UFUNCTION(BlueprintPure, Category = "UND|Settings")
	float GetNonFungiStunDamageMultiplier() const { return NonFungiStunDamageMultiplier; }

protected:

	/** By how much do we multiply the resource gained by default. */
	UPROPERTY(Config, EditAnywhere, Category = "Fungi")
	float FungiResourceMultiplier = 5.0f;

	/** By how much do we multiply the damage done by absorb if we are not fungi-stunned. */
	UPROPERTY(Config, EditAnywhere, Category = "Fungi", meta = (UIMin = 0.0, ClampMin = 0.0))
	float NonFungiStunDamageMultiplier = 0.5f;
};
