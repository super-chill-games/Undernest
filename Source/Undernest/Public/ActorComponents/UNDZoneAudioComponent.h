// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// PMTAU Includes
#include "Sound/PMTAUUESoundGlobals.h"

#include "UNDZoneAudioComponent.generated.h"

class UAudioComponent;
class USoundBase;

/** Component that takes care of controlling the audio for the zones. */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable, Abstract)
class UNDERNEST_API UUNDZoneAudioComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UUNDZoneAudioComponent();

	/** Play the sound used for the combat, while fading out any other sound. */
	UFUNCTION(BlueprintCallable, BlueprintCosmetic, Category = "UND|Audio")
	UAudioComponent* PlayCombatSound(float FadeInDuration = 2.0f, float FadeOutDuration = 2.0f);

	/** Play the sound used for calm, while fading out any other sound. */
	UFUNCTION(BlueprintCallable, BlueprintCosmetic, Category = "UND|Audio")
	UAudioComponent* PlayCalmSound(float FadeInDuration = 2.0f, float FadeOutDuration = 2.0f);

	/** Play the sound used for rest, while fading out any other sound. */
	UFUNCTION(BlueprintCallable, BlueprintCosmetic, Category = "UND|Audio")
	UAudioComponent* PlayRestSound(float FadeInDuration = 2.0f, float FadeOutDuration = 2.0f);

	/** Stop every sound we have played. */
	UFUNCTION(BlueprintCallable, BlueprintCosmetic, Category = "UND|Audio")
	void StopEverySound(float FadeOutDuration = 2.0f);

	/** Retrieves the audio component that is playing. */
	UFUNCTION(BlueprintPure, Category = "UND|Audio")
	UAudioComponent* GetCurrentAudioBeingPlayed() const;

protected:

	/** Bind to gameflow events. */
	void InitializeComponent() override;

	/** Try to fade out current sound and play the given component. */
	UAudioComponent* TryPlaySound(TObjectPtr<UAudioComponent>& Component, const FPMTAUSoundCustomData& Audio, float FadeInDuration, float FadeOutDuration);

	/** Verifies if the given audio component is being played. */
	bool IsPlayingAudioComponent(const TObjectPtr<UAudioComponent>& AudioComponent) const;

	/** Spawn the audio component for the given sound data. */
	UAudioComponent* SpawnAudioComponent(const FPMTAUSoundCustomData& SoundData) const;

	/** Try to play and fade out audios. */
	void OnGameFlowEvent(const FGameplayTag& Event);

	/** Sound used while we are on combat. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTAUSoundCustomData CombatSound;

	/** Sound used when combat has finished. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTAUSoundCustomData CalmSound;

	/** Sound used when we entered to a rest zone. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTAUSoundCustomData RestSound;

	/** When this event is fired, then we play the combat sound. */
	UPROPERTY(EditAnywhere, Category = "UND|GameFlow", meta = (Categories = "GameFlow"))
	FGameplayTag CombatSoundOnGameflowEvent;

	/** When this event is fired, then we play the rest sound. */
	UPROPERTY(EditAnywhere, Category = "UND|GameFlow", meta = (Categories = "GameFlow"))
	FGameplayTag RestSoundOnGameflowEvent;

	/** When this event is fired, then we play the calm sound. */
	UPROPERTY(EditAnywhere, Category = "UND|GameFlow", meta = (Categories = "GameFlow"))
	FGameplayTag CalmSoundOnGameflowEvent;

	/** When this event is fired, we stop the audio. */
	UPROPERTY(EditAnywhere, Category = "UND|GameFlow", meta = (Categories = "GameFlow"))
	FGameplayTag StopAudioGameFlowEvent;

	/** Audio component used to play the combat sound. */
	UPROPERTY(Transient)
	TObjectPtr<UAudioComponent> CombatAudioComponent = nullptr;

	/** Audio component used to play the calm sound. */
	UPROPERTY(Transient)
	TObjectPtr<UAudioComponent> CalmAudioComponent = nullptr;

	/** Audio component used to play the rest sound. */
	UPROPERTY(Transient)
	TObjectPtr<UAudioComponent> RestAudioComponent = nullptr;
};
