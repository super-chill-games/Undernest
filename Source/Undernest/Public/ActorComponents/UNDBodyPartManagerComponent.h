// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ActorComponents/PMTGBodyPartManagerComponent.h"
#include "UNDBodyPartManagerComponent.generated.h"

class UPMTDDialogueGraph;

/** Simple structure that set a mesh for a component for a body part. */
USTRUCT(BlueprintType)
struct UNDERNEST_API FUNDMeshForBodyPartComponent
{
	GENERATED_BODY()

	/** Name of the relevant component. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ComponentName;

	/** Mesh we are going to change. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UStaticMesh> StaticMesh;

	/** Skeletal mesh we are going to change. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<USkeletalMesh> SkeletalMesh;
};

/** Wrap all meshes that should be changed based on a health state. */
USTRUCT(BlueprintType)
struct UNDERNEST_API FUNDMeshByHealthState
{
	GENERATED_BODY()

public:
	
	/** Which state the owner body part should have. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Categories = "State"))
	FGameplayTag StateTag;

	/** Retrieves all meshes that should be changed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FUNDMeshForBodyPartComponent> MeshesPerComponent;
};

UCLASS(EditInlineNew, Blueprintable, BlueprintType)
class UNDERNEST_API UUNDBodyPartConfiguration : public UPMTGBodyPartConfiguration
{
	GENERATED_BODY()
public:

	UUNDBodyPartConfiguration();

	/** Retrieves the corresponding mesh for the given tag. */
	UFUNCTION(BlueprintPure, Category = "UND|BodyPart")
	TArray<FUNDMeshForBodyPartComponent> GetMeshesByTag(FGameplayTag StateTag) const;

	/** Whether we should toggle ragdoll when the body part is destructed. */
	UFUNCTION(BlueprintPure, Category = "UND|BodyPart")
	bool ShouldRagdollOnDestruction() const { return bRagdollMeshesOnDestruction; }

	/** Retrieves the profile that should be applied when we enter into ragdoll mode. */
	UFUNCTION(BlueprintPure, Category = "UND|BodyPart")
	FCollisionProfileName GetRagdollProfileName() const { return RagdollProfile; }
	
protected:
	
	/** Mesh that we are going to set based on the state of the ASC. */
	UPROPERTY(EditAnywhere)
	TArray<FUNDMeshByHealthState> MeshByState;

	/** Whether we simulate physics for all the meshes when the ability system actor is destroyed. */
	UPROPERTY(EditAnywhere)
	bool bRagdollMeshesOnDestruction = true;

	/** Profile that will be set to start radgolling if enabled. */
	UPROPERTY(EditAnywhere, meta = (EditCondition = "bRagdollMeshesOnDestruction"))
	FCollisionProfileName RagdollProfile;
};

/** Body part phase used for bosses. */
UCLASS(Blueprintable, BlueprintType)
class UNDERNEST_API UUNDBossBodyPartPhase : public UPMTGBodyPartPhase
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "UND|BodyPart")
	const TSoftObjectPtr<UPMTDDialogueGraph>& GetFinishPhaseDialogue() const { return FinishPhaseDialogue; }

	/** Try update all relevant meshes for the given actor and state tag. */
	UFUNCTION(BlueprintCallable, Category = "UND|BodyPart")
	void UpdateMeshesForTag(FGameplayTag StateTag, APMTCAbilitySystemActor* Actor);

	/** Try to apply ragdoll mode on the given body parts. */
	UFUNCTION(BlueprintCallable, Category = "UND|BodyPart")
	void TryApplyRagdollOnBodyPartMeshes(APMTCAbilitySystemActor* Actor);

protected:

	/** Dialogue used when the phase has completed. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSoftObjectPtr<UPMTDDialogueGraph> FinishPhaseDialogue;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class UNDERNEST_API UUNDBodyPartManagerComponent : public UPMTGBodyPartManagerComponent
{
	GENERATED_BODY()

public:

	UUNDBodyPartManagerComponent();
};
