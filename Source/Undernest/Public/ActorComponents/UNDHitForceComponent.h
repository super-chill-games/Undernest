﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// UE Includes
#include "GameplayTagContainer.h"

// PMTC Includes
#include "Utilities/PMTCCommonTypes.h"

#include "UNDHitForceComponent.generated.h"


UCLASS(Abstract, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class UNDERNEST_API UUNDHitForceComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UUNDHitForceComponent();

protected:

	/** Bind to required delegates for our owner. */
	void InitializeComponent() override;

	/** Try to stop any movement and apply corresponding damage. */
	UFUNCTION()
	void OnOwnerHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	/** Store any required data and make the bindings. */
	void OnForceApplyTagChanged(const FGameplayTag Tag, int32 Count);

	/** Clear bindings when stun is removed. */
	void OnFungiStunChanged(const FGameplayTag Tag, int32 Count);

	/** All gameplay effects we should apply to our owner. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TArray<FPMTCSetByCallerGameplayEffectConfig> GameplayEffectsToApply;

	/** Filter that is applied to the actors we collide against.
	* If it passes the filter, then we are going to apply the gameplay effect. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTCObjectFilter ObjectFilter;

	/** Minimum velocity we should have to trigger damage to our owner. */
	UPROPERTY(EditAnywhere, Category = "UND")
	double MinVelocityToTriggerDamage = 200.0;

	/** Whether we stop all movement when we collide against a wall. */
	UPROPERTY(EditAnywhere, Category = "UND")
	bool bStopAllMovementOnCollision = true;

private:

	/** Location our actor had when the force has been applied.
	* This way we can identify how much it traveled to prevent applying any kind of damage
	* if moving a little bit for collision handling. */
	FVector LocationOnForceApplied = FVector::ZeroVector;
};
