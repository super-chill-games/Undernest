// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

// PMTC Includes
#include "Camera/PMTCPlayerCameraManager.h"

#include "UNDBossManagerComponent.generated.h"

class APawn;
class APlayerCameraManager;
class UPMTDDialogueGraph;
class UPMTDDialogueUserComponent;
class UPMTGBodyPartPhase;
class UPMTGBodyPartManagerComponent;

/** Component that takes care of controlling the scripting for a boss
* battle. */
UCLASS(ClassGroup=(UND), meta=(BlueprintSpawnableComponent), Blueprintable)
class UNDERNEST_API UUNDBossManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UUNDBossManagerComponent();

	/** Retrieves the center point of the boss. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|Boss")
	FVector GetBossCenterPoint() const;

	/** Retrieves the boss pawn. */
	UFUNCTION(BlueprintPure, Category = "UND|Boss")
	APawn* GetOwningBossPawn() const;

protected:

	/** Make bindings with the actor to listen for overlap events. */
	void InitializeComponent() override;

	/** Return camera to player and start battle. */
	UFUNCTION()
	void OnWelcomeDialogueFinished(UPMTDDialogueUserComponent* DialogueUserComponent);

	/** Return camera to player and keep fighting. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Boss")
	void OnPhaseDialogueFinished(UPMTDDialogueUserComponent* DialogueUserComponent);

	/** Called when all phases were completed and we are about to start the final fight. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Boss")
	void HandleAllPhasesCompleted(UPMTGBodyPartManagerComponent* BodyPartManager);

	/** Called when a phase for a boss finishes.
	* Move camera to boss and once there it would play the phase dialogue. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Boss")
	void OnBossPhaseFinished(UPMTGBodyPartManagerComponent* BodyPartManager, UPMTGBodyPartPhase* Phase);

	/** Called when all phases for a boss have finished. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Boss")
	void OnAllPhasesFinished(UPMTGBodyPartManagerComponent* BodyPartManager);

	/** Apply or remove the corresponding tags when a phase finishes. */
	UFUNCTION(BlueprintCallable, Category = "UND|Boss")
	void AddOrRemoveBossPhaseFinishTags(bool bAdd = true);

	/** Try focus camera into boss, start dialogue and so on. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Boss")
	void HandlePlayerEnteredIntoBossArea(APawn* PlayerPawn);

	/** Called when the boss battle has begun (camera is returning to player, dialogue has ended, and so on). */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Boss")
	void HandleBossBattleBegin();

	/** Start welcome dialogue. */
	UFUNCTION()
	void OnCameraTransitionedIntoTarget(APlayerCameraManager* CameraManager);

	/** Start phase dialogue. */
	UFUNCTION()
	void OnCameraTransitionedIntoTargetFromPhase(APlayerCameraManager* CameraManager);

	/** If OtherActor is a player, then notify events to start
	* battle. */
	UFUNCTION()
	void OnActorBeginOverlapWithBossArea(AActor* OverlappedActor, AActor* OtherActor);

	/** Area that once overlapped, will start the boss battle.
	* This could imply triggering a gameflow event, start a dialogue and so on. */
	UPROPERTY(EditInstanceOnly, Category = "UND")
	TObjectPtr<AActor> EnterIntoBossArea = nullptr;

	/** Dialogue that will be displayed when the player enters into the boss area.
	* If no dialogue specified, then the battle will begin immediately. */
	UPROPERTY(EditAnywhere, Category = "UND|Dialogue")
	TSoftObjectPtr<UPMTDDialogueGraph> WelcomeDialogue = nullptr;

	/** Parameters used to focus the camera into the boss. */
	UPROPERTY(EditAnywhere, Category = "UND|Camera")
	FPMTCCameraTransitionParams FocusBossCameraParams;

	/** Whether the AI controller is spawned when the battle spawns. */
	UPROPERTY(EditInstanceOnly, Category = "UND")
	bool bSpawnControllerWhenBattleBegins = true;
};
