// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UNDTextPieceComponent.generated.h"

/** Simple component that shows a message on the UI. */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UNDERNEST_API UUNDTextPieceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UUNDTextPieceComponent();

protected:

	/** Bind to actor overlap events. */
	void InitializeComponent() override;

	/** Try to display message. */
	UFUNCTION()
	void OnActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);

	/** Try to hide message. */
	UFUNCTION()
	void OnActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor);

	/** Text that will be displayed. */
	UPROPERTY(EditAnywhere, Category = "UND", meta = (MultiLine = true))
	FText TextPiece;
};
