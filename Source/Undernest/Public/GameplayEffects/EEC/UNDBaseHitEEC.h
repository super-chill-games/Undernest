// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffects/EEC/PMTRTUBaseHitEEC.h"
#include "UNDBaseHitEEC.generated.h"

/**
 * Main EEC for damage.
 */
UCLASS()
class UNDERNEST_API UUNDBaseHitEEC : public UPMTRTUBaseHitEEC
{
	GENERATED_BODY()

public:

	UUNDBaseHitEEC();

	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
		FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

	DECLARE_ATTRIBUTE_CAPTUREDEF(FungiHealth);
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxFungiHealth);
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxFungiDamageAllowedPerStunState);

	DECLARE_ATTRIBUTE_CAPTUREDEF(MetaFungiDamage);
	DECLARE_ATTRIBUTE_CAPTUREDEF(AccumulatedFungiDamage);

protected:

	/** Calculate and grant the fungi energy to the player based on the fungi damage that has done. */
	void GrantFungiEnergy(const FGameplayEffectCustomExecutionParameters& ExecutionParams, float FungiDamageDone) const;

	/** Gameplay effect used to stun a target when its fungi is depleted. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> FungiStunGameplayEffect;
};
