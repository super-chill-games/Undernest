﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "UNDDashCooldownEEC.generated.h"

/**
 * EEC that manages applying a cooldown reduction to the dash ability.
 */
UCLASS()
class UNDERNEST_API UUNDDashCooldownEEC : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:

	/** Try apply dash gameplay effect reductions. */
	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
		FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

protected:

	/** Gameplay effect that will remove/reduce the dash cooldown. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> DashReductionGE;

	/** Gameplay effect that will be applied to the enemies we hit to prevent applying the
	* reduction GE. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> DashMarkGE;

	/** Whether the dash mark is removed from all the other AIs when we hit to a new one. */
	UPROPERTY(EditAnywhere, Category = "UND")
	bool bRemoveDashMarkOnOtherAIs = false;
};
