﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/PMTCAbilitySystemComponent.h"
#include "UNDShieldedEnemyExternalCalculator.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDShieldedEnemyExternalCalculator : public UPMTCAttributeExternalCalculator
{
	GENERATED_BODY()

public:

	/** Negate damage if we are not being hit backwards and our shield is up. */
	float CalculateAttribute_Implementation(UAbilitySystemComponent* TargetASC, const FGameplayEffectSpec& Spec, FGameplayAttribute Attribute, float AttributeValue) const override;

};
