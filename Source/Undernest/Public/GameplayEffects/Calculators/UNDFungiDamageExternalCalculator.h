﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/PMTCAbilitySystemComponent.h"
#include "UNDFungiDamageExternalCalculator.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDFungiDamageExternalCalculator : public UPMTCAttributeExternalCalculator
{
	GENERATED_BODY()

public:

	float CalculateAttribute_Implementation(UAbilitySystemComponent* TargetASC, const FGameplayEffectSpec& Spec, FGameplayAttribute Attribute, float AttributeValue) const override;
};
