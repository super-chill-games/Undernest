// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "UNDGameInstance.generated.h"

class UUNDGameGlobals;
class UUNDPersistentData;

/**
 * Main game instance for undernest.
 */
UCLASS()
class UNDERNEST_API UUNDGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UUNDGameInstance();

	/** Initialize ASC data. */
	void Init() override;

	/** Retrieves game globals. */
	UFUNCTION(BlueprintPure, Category = "UND")
	UUNDGameGlobals* GetGameGlobals() const { return GameGlobals; }

	/** Retrieves persistent data. */
	UFUNCTION(BlueprintPure, Category = "UND")
	UUNDPersistentData* GetPersistentData() const;

protected:

	/** Class of game globals, will be instantiated so it can save state. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TSubclassOf<UUNDGameGlobals> GameGlobalsClass;

	/** Class that holds persistent data across levels or gameplay. */
	UPROPERTY(EditDefaultsOnly, Category = "UND")
	TSubclassOf<UUNDPersistentData> PersistentDataClass;

	/** Game global's instance. */
	UPROPERTY(Transient)
	TObjectPtr<UUNDGameGlobals> GameGlobals = nullptr;

	/** Persistent data. */
	UPROPERTY(Transient)
	TObjectPtr<UUNDPersistentData> PersistentData = nullptr;
};
