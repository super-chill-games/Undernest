// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UNDPersistentData.generated.h"

/**
 * Track all persistent data for our game.
 */
UCLASS(Blueprintable, BlueprintType)
class UNDERNEST_API UUNDPersistentData : public UObject
{
	GENERATED_BODY()

public:

	/** Retrieves persistent data. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent", meta = (DefaultToSelf = "WorldContext"))
	static UUNDPersistentData* GetPersistentData(const UObject* WorldContext);

	/** Mark HUB world as loaded for first time. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent")
	void SetHUBOpenedAtLeastOnce() { bHUBWorldLoadedAtLeastOnce = true; }

	/** Mark tutorial level as played. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent")
	void SetTutorialLevelPlayed() { bTutorialLevelPlayed = true; }

	/** Retrieve the remaining fungi resource and set it to zero. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent")
	int32 TakePersistentFungiResource();

	/** Retrieve the current fungi resource and based on any state, keep the proper value. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent")
	void HandleCharacterDeath(AActor* Character);

	/** Applies any persistent data on the given character when is revived. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent")
	void ApplyPersistentDataOnRevive(AActor* Character);

	/** Increase by one the amount of ants we have rescued so far. */
	UFUNCTION(BlueprintCallable, Category = "UND|Persistent")
	void AddRescuedAnt();

	/** Retrieves how many ants have been rescued. */
	UFUNCTION(BlueprintPure, Category = "UND|Persistent")
	int64 GetRescuedAnts() const;

	/** Verifies if the HUB world was loaded at least once. */
	UFUNCTION(BlueprintPure, Category = "UND|Persistent")
	bool WasHubLoadedAtLeastOnce() const;

	/** Verifies if the tutorial world was played. */
	UFUNCTION(BlueprintPure, Category = "UND|Persistent")
	bool WasTutorialLevelPlayed() const;

	/** Whether the player has died. */
	UFUNCTION(BlueprintPure, Category = "UND|Persistent")
	bool HasPlayerDied() const;

protected:

	/** Fungi resource left after dying. */
	UPROPERTY()
	int32 PersistentFungiResource = 0;

	/** How many ants we have rescued. */
	UPROPERTY()
	uint32 PersistentRescuedAnts = 0;

	/** Whether the hub world has been loaded at least once. */
	UPROPERTY()
	bool bHUBWorldLoadedAtLeastOnce = false;

	/** Whether the tutorial level has been played. */
	UPROPERTY()
	bool bTutorialLevelPlayed = false;

	/** Whether the player has died. */
	UPROPERTY()
	bool bHasPlayerDied = false;
};
