// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "UNDGameGlobals.generated.h"

/**
 * Object that holds global data for Undernest and there isn't a clear place to put them.
 */
UCLASS(Blueprintable, BlueprintType)
class UNDERNEST_API UUNDGameGlobals : public UObject
{
	GENERATED_BODY()

public:

	UUNDGameGlobals();

	/** Retrieves game globals statically. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameGlobals", meta = (WorldContext = "WorldContextObject"))
	static UUNDGameGlobals* GetGameGlobals(UObject* WorldContextObject);

	/** Verifies if we are in a HUB world. */
	UFUNCTION(BlueprintPure, Category = "UND|GameGlobals", meta = (WorldContext = "WorldContextObject"))
	static bool AreWeInHUBWorld(UObject* WorldContextObject);

	/** Verifies if we are in a laboratory world. */
	UFUNCTION(BlueprintPure, Category = "UND|GameGlobals", meta = (WorldContext = "WorldContextObject"))
	static bool AreWeInLaboratoryWorld(UObject* WorldContextObject);

	/** Utility function that disables or enables input for player and change the character's custom time dilation. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameGlobals", meta = (WorldContext = "WorldContextObject"))
	static void EnableDisableInputAndChangeTimeDilationForCharacter(UObject* WorldContextObject,
		bool bEnableInput = true, float TimeDilation = 1.0f, int32 PlayerIndex = 0);

	/** Travel to hub. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameGlobals")
	bool TravelToHUB(bool bAbsolute = true);

	/** Travel to laboratory. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameGlobals")
	bool TravelToLaboratory(bool bAbsolute = true, bool bFromDeath = true);

	/** Travel to tutorial level. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameGlobals")
	bool TravelToTutorial();

	/** Travel to menu level. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameGlobals")
	bool TravelToMenu();

protected:

	/** Level used for the HUB. */
	UPROPERTY(EditDefaultsOnly, Category = "Levels")
	TSoftObjectPtr<UWorld> HubLevel;

	/** Level used for the laboratory. */
	UPROPERTY(EditDefaultsOnly, Category = "Levels")
	TSoftObjectPtr<UWorld> LaboratoryLevel;

	/** Level used for the tutorial. */
	UPROPERTY(EditDefaultsOnly, Category = "Levels")
	TSoftObjectPtr<UWorld> TutorialLevel;

	/** Level used for the menu. */
	UPROPERTY(EditDefaultsOnly, Category = "Levels")
	TSoftObjectPtr<UWorld> MenuLevel;

	/** Whether data reset after we travel to the laboratory using an absolute travel. */
	UPROPERTY(EditDefaultsOnly, Category = "Levels")
	bool bPersistDataOnTravelToLaboratoryAbsolute = false;
};
