// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Pickup/PMTPIBasePickup.h"
#include "UNDBasePickup.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API AUNDBasePickup : public APMTPIBasePickup
{
	GENERATED_BODY()

public:

	AUNDBasePickup(const FObjectInitializer& InObjectInitializer);
	
};
