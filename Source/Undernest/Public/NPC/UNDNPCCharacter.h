﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Characters/PMTNPCCharacter.h"
#include "Interfaces/PMTIInteractionReceiverInterface.h"
#include "UNDNPCCharacter.generated.h"

UCLASS()
class UNDERNEST_API AUNDNPCCharacter : public APMTNPCCharacter, public IPMTIInteractionReceiverInterface
{
	GENERATED_BODY()

public:
};
