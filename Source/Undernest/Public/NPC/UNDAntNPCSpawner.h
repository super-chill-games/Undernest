﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "NPC/UNDNPCSpawner.h"
#include "UNDAntNPCSpawner.generated.h"

class UUNDAntNPCEmitterDefinition;
/** Simple actor used to spawn an unlockable npc. */
UCLASS()
class UNDERNEST_API AUNDAntNPCSpawner : public AUNDNPCSpawner
{
	GENERATED_BODY()

public:
	AUNDAntNPCSpawner();

	/** Try set emitter definition. */
	void HandleNPCSpawned_Implementation(AActor* InActor) override;

	/** Verify conditions. */
	bool CanSpawnNPC_Implementation() const override;

protected:

	/** Emitter of the npc we should spawn. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSoftObjectPtr<UUNDAntNPCEmitterDefinition> NPCEmitterDefinition;
};
