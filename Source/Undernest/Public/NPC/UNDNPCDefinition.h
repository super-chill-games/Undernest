// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "NPC/PMTNPCDefinition.h"
#include "UNDNPCDefinition.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDNPCDefinition : public UPMTNPCDefinition
{
	GENERATED_BODY()
};
