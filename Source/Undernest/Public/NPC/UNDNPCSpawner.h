// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "NPC/PMTNPCSpawner.h"
#include "UNDNPCSpawner.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API AUNDNPCSpawner : public APMTNPCSpawner
{
	GENERATED_BODY()
	
};
