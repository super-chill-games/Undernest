﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Dialogue/PMTDDialogueEmitterDefinition.h"
#include "UNDAntNPCEmitterDefinition.generated.h"

class UPMTDDialogueGraph;
/**
 * Simple emitter definition used for hub ants.
 */
UCLASS(BlueprintType)
class UNDERNEST_API UUNDAntNPCEmitterDefinition : public UPMTDDialogueEmitterDefinition
{
	GENERATED_BODY()

public:

	/** Verifies if this npc has been unlocked. */
	UFUNCTION(BlueprintPure, Category = "UND|NPC|Ant")
	bool IsNPCUnlocked(const UObject* WorldContext) const;

	/** Retrieves the dialogue graph we should use. */
	UFUNCTION(BlueprintPure, Category = "UND|NPC|Ant")
	const TSoftObjectPtr<UPMTDDialogueGraph>& GetDialogueGraph() const { return Dialogue; }

protected:

	/** How many ants do we require to be able to spawn this ant. */
	UPROPERTY(EditAnywhere, Category = "UND", AssetRegistrySearchable)
	int32 RequiredSavedAnts = 0;

	/** Dialogue we should use with this ant npc. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSoftObjectPtr<UPMTDDialogueGraph> Dialogue;
};
