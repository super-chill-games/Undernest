﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/PMTCGameplayAbilityBase.h"

// PMTC Includes
#include "Abilities/Tasks/PMTCInputEventTask.h"
#include "Types/PMTCollisionTypes.h"
#include "UNDChainParryAbility.generated.h"

class AUNDParrySplineActor;
class UPMTCWaitInputEvents;
class UAbilityTask_WaitDelay;

/**
 * Ability that will trigger chains of parries that will insta stun enemies.
 */
UCLASS()
class UNDERNEST_API UUNDChainParryAbility : public UPMTCGameplayAbilityBase
{
	GENERATED_BODY()

public:

	UUNDChainParryAbility();

	/** Prevent activation if no input is specified. */
	bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;

	/** Start listening for input. */
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:

	/** Find any nearby actor that we can trigger a parry. */
	AActor* FindNextAvailableActor();

	/** Request the next input we are going to listen for chaining. */
	TSoftObjectPtr<UInputAction> RequestNextInput() const;

	/** Verify if our target is still valid to parry, otherwise jump to a new one. */
	void TickAbility_Implementation(float DeltaTime) override;

	/** Fly to the given target. */
	void FlyToTarget(AActor* InTarget);

	/** Display the input that must be pressed. */
	void DisplayChainInput();

	/** Chain parry or end ability if input pressed is invalid. */
	UFUNCTION()
	void OnInputPressed(const UInputAction* Input, ETriggerEvent Event, float EventTime, float TimeHeld);

	/** If the previous movement mode was the spline one, then we can display the other prompt. */
	UFUNCTION()
	void OnSplineFlightFinished(ACharacter* Character, EMovementMode PrevMovementMode, uint8 PreviousCustomMode);

	/** Generate next prompt. */
	void HandleFlightFinished();

	/** Kill ability. */
	UFUNCTION()
	void OnInputTimeFinished();

	/** Kill ability. */
	UFUNCTION()
	void OnTimeLimitReached();

	/** How much time do we have to press the corresponding input to trigger another chain. */
	UPROPERTY(EditAnywhere, Category = "UND|Input")
	FPMTCSetByCallerScalable TimeToPressInput;

	/** Max amount of times we can chain a parry. */
	UPROPERTY(EditAnywhere, Category = "UND|Input")
	FPMTCSetByCallerScalable MaxChain;

	/** Inputs we should listen for chaining. */
	UPROPERTY(EditAnywhere, Category = "UND|Input")
	TArray<FPMTCWaitInputParams> InputsToListen;

	/** Effects that are applied while we are flying. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	TArray<FPMTCSetByCallerGameplayEffectConfig> EffectsWhileFlying;

	/** Config used to detect actors that we can chain parries. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	FPMTCTraceConfiguration DetectionConfig;

	/** Filter used to prevent certain actors to be chosen for chaining a parry. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	FPMTCObjectFilter ActorFilter;

	/** If our owner has any of these tags, we are not going to be able to find a target. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	FGameplayTagContainer PreventChainOwnerTags;

	/** Max distance we are allowed to find possible actors. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	FPMTCSetByCallerScalable MaxDistance;

	/** Actor we are going to spawn to make the character fly. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	TSubclassOf<AUNDParrySplineActor> SplineActorClass;

	/** Cue that will take care of displaying the proper input for the player. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain", meta = (Categories = "GameplayCue"))
	FGameplayTag InputCue;

	/** Whether there's a time limit to trigger all chains. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	bool bHasTimeLimit = false;

	/** Time we can trigger all inputs. */
	UPROPERTY(EditAnywhere, Category = "UND|Chain")
	FPMTCSetByCallerScalable TimeLimit;

	/** Task used to listen for input events for chaining parries. */
	UPROPERTY(Transient)
	TObjectPtr<UPMTCWaitInputEvents> WaitInputEvents = nullptr;

	/** Task that will terminate the ability if no input is pressed. */
	UPROPERTY(Transient)
	TObjectPtr<UAbilityTask_WaitDelay> WaitInputPressTask = nullptr;

	/** Task that will wait for certain time to kill the parry chain. */
	UPROPERTY(Transient)
	TObjectPtr<UAbilityTask_WaitDelay> TimeLimitTask = nullptr;

	/** Input we should press to chain a parry. */
	UPROPERTY(Transient)
	TSoftObjectPtr<UInputAction> ChainInput = nullptr;

	/** To which actor should we chain. */
	UPROPERTY(Transient)
	TObjectPtr<AActor> Target = nullptr;

	/** Actor used to make the flight. */
	UPROPERTY(Transient)
	TObjectPtr<AUNDParrySplineActor> SplineActor = nullptr;

	/** Effects we have applied while we were flying. */
	TArray<FActiveGameplayEffectHandle> FlyingEffects;
	
	/** Count how many times we have chained a parry. */
	int32 ChainCount = 0;

	/** Timestamp when the input has been displayed. */
	float InputDisplayTimestamp = 0.0f;

	bool bIgnoreInputs = false;

	bool bIsFlying = false;
};
