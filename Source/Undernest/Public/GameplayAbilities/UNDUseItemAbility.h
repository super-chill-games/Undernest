// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/PMTINUseItemAbility.h"
#include "UNDUseItemAbility.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDUseItemAbility : public UPMTINUseItemAbility
{
	GENERATED_BODY()

public:
	
};
