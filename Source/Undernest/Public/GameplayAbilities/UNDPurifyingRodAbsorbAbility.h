﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilities/PMTCMontageAbility.h"
#include "Abilities/Tasks/PMTCBaseAbilityTask.h"

// PMTC Includes
#include "Types/PMTCAnimTypes.h"
#include "Types/PMTCollisionTypes.h"

// PMTGU Includes
#include "Utilities/PMTGUTypes.h"

#include "UNDPurifyingRodAbsorbAbility.generated.h"

class UAbilityTask_WaitGameplayEvent;
class UAbilityTask_WaitInputRelease;

/** Task that controls the area and which targets can we absorb. */
UCLASS()
class UNDERNEST_API UUNDPurifyingAbsorbAreaTask : public UPMTCBaseAbilityTask
{
	GENERATED_BODY()

public:

	/** Create the task that will take care of controlling the targets that are being hit. */
	static UUNDPurifyingAbsorbAreaTask* StartPurifyTask(
		UGameplayAbility* OwningAbility,
		const FPMTCObjectFilter& InFilter,
		int32 InMaxAllowedTargets,
		const FPMTCTraceConfiguration& InAbsorbTraceConfig,
		FGameplayEffectSpecHandle InAbsorbGE,
		float InMaxRadius,
		float InInitialRadius,
		float InTimeToReachMaxRadius);

	UUNDPurifyingAbsorbAreaTask();

	/** Call start purifying in our rod. */
	void Activate() override;
	
protected:

	/** Increase absorb radius and update the affected targets if any. */
	void TickTask(float DeltaTime) override;

	/** Kill any added cue. */
	void OnDestroy(bool bInOwnerFinished) override;

	/** Track those actors that are being absorbed */
	UPROPERTY(Transient)
	TArray<TObjectPtr<AActor>> AbsorbedActors;

	/** Filter applied to hits. */
	FPMTCObjectFilter Filter;

	/** Configuration used to make traces to detect those enemies we can hit. */
	FPMTCTraceConfiguration AbsorbTraceConfig;

	/** Gameplay effect that will be applied while absorbing. */
	FGameplayEffectSpecHandle AbsorbGameplayEffectSpec;
	
	/** How many targets can we hit at the same time while absorbing. */
	int32 MaxAllowedTargets = 0;

	/** Max absorb radius. */
	float MaxRadius = 0.0f;

	/** Time we take to reach max radius. */
	float TimeToReachMaxRadius = 0.0f;

	/** Initial absorb radius. */
	float InitialRadius = 0.0f;

	/** Time we are absorbing so far. */
	float AccumulatedTime = 0.0f;
};

/**
 * Main ability used by the purifying rod to absorb fungi energy and make
 * a flash move after it's released.
 */
UCLASS()
class UNDERNEST_API UUNDPurifyingRodAbsorbAbility : public UPMTCMontageAbility
{
	GENERATED_BODY()

public:

	UUNDPurifyingRodAbsorbAbility();

	/** Start listening for input. */
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle,
		const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo,
		const FGameplayEventData* TriggerEventData) override;

	/** Kill wait input release task. */
	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo,
		const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:

	/** Jump to flash section name. */
	UFUNCTION()
	virtual void OnInputReleased(float TimeHeld);

	/** Trigger flash and kill ability. */
	UFUNCTION()
	void OnTriggerFlashEvent(FGameplayEventData Payload);

	/** Start purify absorb task. */
	UFUNCTION()
	void OnStartAbsorbingEvent(FGameplayEventData Payload);

	/** Max radius for the absorb area. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTCSetByCallerScalable AbsorbMaxRadius;

	/** Initial radius for the absorb area. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTGUScalableFloat AbsorbInitialRadius;

	/** Time in seconds that we take to reach the max radius. If 0.0f, then it will be instant. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTGUScalableFloat TimeToReachMaxRadius;

	/** How many targets do we allow to absorb while we charge. If 0, then we can absorb as many as we want. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTGUScalableFloat MaxTargetsToAbsorb;

	/** Name of the montage section that we are going to change when we release the input. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FName FlashSectionName;

	/** Filter for actors with specific classes or tags. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTCObjectFilter TargetFilter;

	/** GE that is applied while absorbing to targets. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTCSetByCallerGameplayEffectConfig AbsorbGE;

	/** GE that is applied to all nearby targets when we release the input.
	* Set this to null if you do not want to trigger any flash GE. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UGameplayEffect> FlashGE;

	/** Configuration used to detect those actors we can absorb.
	* Radius/extent will be modified based on the radius of the area that increases
	* over time. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTCTraceConfiguration AbsorbDetectionTrace;

	/** Event we are going to wait in the montage to trigger the flash. If not specified,
	* then it will be called when the input is released. */
	UPROPERTY(EditAnywhere, Category = "UND|Event")
	FGameplayTag EventToTriggerFlash;

	/** Event we are going to wait in the montage to trigger the absorb process. If not specified,
	* then it will be called when the montage starts. */
	UPROPERTY(EditAnywhere, Category = "UND|Event")
	FGameplayTag EventToStartAbsorbing;

	/** Once released, jump to free montage section. */
	UPROPERTY(Transient)
	TObjectPtr<UAbilityTask_WaitInputRelease> WaitInputReleaseTask = nullptr;

	/** Task that takes care of absorbing nearby corruption. */
	UPROPERTY(Transient)
	TObjectPtr<UUNDPurifyingAbsorbAreaTask> PurifyAbsorbTask = nullptr;

	/** Task used to wait for the trigger flash event. */
	UPROPERTY(Transient)
	TObjectPtr<UAbilityTask_WaitGameplayEvent> WaitTriggerFlashEventTask = nullptr;

	/** Task used to wait for start absorbing. */
	UPROPERTY(Transient)
	TObjectPtr<UAbilityTask_WaitGameplayEvent> WaitStartAbsorbTask = nullptr;

	/** All melee weapons that were previously active. */
	TArray<FPMTCSimpleHandle> PreviousMeleeWeapons;

	/** Store the time that passed when we released the button. */
	float StoredTimeHeld = 0.0f;
};
