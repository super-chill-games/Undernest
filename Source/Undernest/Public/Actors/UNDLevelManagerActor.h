// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UNDLevelManagerActor.generated.h"

/** Actor that holds components that controls some of the stuff
* for the level flow. */
UCLASS(Blueprintable, BlueprintType, Abstract)
class UNDERNEST_API AUNDLevelManagerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	AUNDLevelManagerActor();

private:

#if WITH_EDITORONLY_DATA
	/** Billboard Component displayed in editor */
	UPROPERTY(VisibleAnywhere, Category = "UND")
	TObjectPtr<class UBillboardComponent> SpriteComponent;
#endif // WITH_EDITORONLY_DATA
};
