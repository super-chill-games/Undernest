// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Actors/PMTRTUDestructibleActor.h"

// UND Includes
#include "Interfaces/UNDCorruptionInterface.h"

#include "UNDDestructibleActor.generated.h"

/**
 * Base class for destructible actors.
 */
UCLASS()
class UNDERNEST_API AUNDDestructibleActor : public APMTRTUDestructibleActor, public IUNDCorruptionInterface
{
	GENERATED_BODY()

public:

protected:
	
};
