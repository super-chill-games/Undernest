// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

// UND Includes
#include "Interfaces/UNDCorruptionInterface.h"

#include "UNDDoor.generated.h"

class UUNDLevelDefinition;
class UBoxComponent;

/** Defines the state of the door. */
UENUM(BlueprintType)
enum class EUNDDoorState : uint8
{
	Close = 0,
	Open = 1,
	Locked = 2
};

/** Base class that allows the transition to other levels. */
UCLASS(Abstract)
class UNDERNEST_API AUNDDoor : public AActor, public IUNDCorruptionInterface
{
	GENERATED_BODY()
	
public:	

	AUNDDoor();

	/** Retrieves door's state. */
	UFUNCTION(BlueprintPure, Category = "UND|Door")
	EUNDDoorState GetDoorState() const { return CurrentState; }

	/** Set our variables to replicate. */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:

	/** Make our bindings. */
	void PostInitializeComponents() override;

	/** Try open/close door. */
	void BeginPlay() override;

	/** Try to update door's state. */
	void TryChangeDoorState(EUNDDoorState PreviousState, EUNDDoorState NewState);

	/** Verifies if the door can actually change to the given state. For example,
	* we could check if the area was cleared in certain time. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Door")
	bool CanChangeToState(EUNDDoorState DoorState) const;

	/** Called when a player has entered to door. By default
	* we broadcast a gameflow event. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Door")
	void HandlePlayerEnteredToDoor(APawn* PlayerPawn);

	/** Called when a player has tried to enter the door while we are in a corrupted state. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Door")
	void HandlePlayerEnteredToDoorWhileCorrupted(APawn* PlayerPawn);

	/** Called both for client and server when door's state has changed. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Door")
	void HandleDoorChangeState(EUNDDoorState PreviousState, EUNDDoorState NewState, bool bInstantChange);

	/** Try to transition to other level. */
	UFUNCTION()
	void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Nothing for the moment. */
	UFUNCTION()
	void OnComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/** Try to open or close the doors. */
	UFUNCTION()
	void OnRep_CurrentState(EUNDDoorState PreviousState);

	/** Called when we should update the door state based on a gameflow event.
	* Only called on server. We verify if we can jump to the new state. */
	void OnDoorGameFlowEvent(const FGameplayTag& Event, EUNDDoorState NewState);

	/** Component that takes care of the transform. */
	UPROPERTY(VisibleAnywhere, Category = "UND")
	TObjectPtr<USceneComponent> MainComponent;

	/** Component that will be used to start transitioning to another level. */
	UPROPERTY(VisibleAnywhere, Category = "UND")
	TObjectPtr<UBoxComponent> CollisionComponent;

	/** Initial state of the door. This is used to avoid triggering open/close animations
	* while the door has this state at begin play. */
	UPROPERTY(EditAnywhere, Category = "UND")
	EUNDDoorState InitialState = EUNDDoorState::Close;

	/** State that the door should have. Modify this if you want
	* to change the state of the door when on begin play.*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_CurrentState, Category = "UND", meta = (ExposeOnSpawn = true))
	EUNDDoorState CurrentState = EUNDDoorState::Close;

	/** Whether we take some seconds to load a level, to allow effects, camera and such to play. If 0.0f,
	* then it will be instant. */
	UPROPERTY(EditAnywhere, Category = "UND", meta = (UIMin = 0.0, ClampMin = 0.0))
	float DelayToLoadLevel = 3.0f;

	/** If the door's open/close state is set on beginplay or when replicated, whether
	* it should be instant. This can be ignored if doors open in an instant manner
	* already. */
	UPROPERTY(EditAnywhere, Category = "UND")
	bool bInstantCloseOrOpenOnBeginPlayOrCreation = true;

	/** Whether the door is always locked. */
	UPROPERTY(EditAnywhere, Category = "UND")
	bool bAlwaysLocked = false;

	/** To which area should we transition into.
	 * This will override any previous area, so only use it on hub or world selector.
	 */
	UPROPERTY(EditAnywhere, Category = "UND", meta = (Categories = "Area"))
	FGameplayTag AreaToTransition;

	/** Level definition we are going to just go, avoiding level manager.
	* NOTE: This won't even care to which level we go, even if we have played it already. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UUNDLevelDefinition> OverrideLevelDefinition;
};
