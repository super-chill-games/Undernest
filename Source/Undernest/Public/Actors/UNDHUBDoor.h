// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Actors/UNDDoor.h"
#include "UNDHUBDoor.generated.h"

/**
 * Door that is meant to be used for the HUB, so we can transition into
 * different areas.
 */
UCLASS(Abstract)
class UNDERNEST_API AUNDHUBDoor : public AUNDDoor
{
	GENERATED_BODY()

public:

	AUNDHUBDoor();
};
