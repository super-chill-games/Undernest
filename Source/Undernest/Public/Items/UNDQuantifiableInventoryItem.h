// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Items/UNDBaseInventoryItem.h"
#include "UNDQuantifiableInventoryItem.generated.h"

/**
 * Item that supports quantity.
 */
UCLASS()
class UNDERNEST_API UUNDQuantifiableInventoryItem : public UUNDBaseInventoryItem
{
	GENERATED_BODY()

public:

	int32 GetItemQuantity() const override;

	/** Increment the amount of items we can have and return the amount we have. */
	int32 IncrementItemQuantity(int32 InAmount) override;

	/** Decrement the amount of items we have and return the amount we have. */
	int32 DecrementItemQuantity(int32 InAmount) override;

	/** Increment the quantity by decimal, triggering an increase of quantity if the value increases enough. */
	void IncrementItemQuantityByDecimal(float InAmount);

	/** Retrieves item max quantity. */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "UND|Item")
	int32 GetItemMaxQuantity() const;

	/** Set quantity to replicate. */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	bool SupportsQuantity() const { return true; }

protected:

	/** Notify that we got updated. */
	UFUNCTION()
	virtual void OnRep_ItemQuantity(uint16 OldValue);

	/** How much quantity we have for this item. */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_ItemQuantity)
	uint16 ItemQuantity = 1;

	/** Item quantity we accumulate in case we increment this item by float instead. */
	float DecimalItemQuantity = 1.0f;
};
