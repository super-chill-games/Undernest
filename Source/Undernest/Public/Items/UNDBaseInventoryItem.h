// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Inventory/PMTRTUBaseInventoryItem.h"
#include "UNDBaseInventoryItem.generated.h"

/**
 * Base item class for undernest.
 */
UCLASS(Abstract)
class UNDERNEST_API UUNDBaseInventoryItem : public UPMTRTUBaseInventoryItem
{
	GENERATED_BODY()
	
};
