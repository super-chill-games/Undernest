// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Items/UNDBaseInventoryItem.h"

// PMTMW Includes
#include "Interfaces/PMTMWMeleeInstanceInterface.h"

#include "UNDMeleeInventoryItem.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDMeleeInventoryItem : public UUNDBaseInventoryItem, public IPMTMWMeleeInstanceInterface
{
	GENERATED_BODY()

public:

	// ~ Begin IPMTMWMeleeInstanceInterface
	const UPMTMWMeleeData* GetMeleeData() const override final;
	FPMTCSimpleHandle GetMeleeWeaponHandle() const override final;
	// ~ End IPMTMWMeleeInstanceInterface

	/** Try create melee weapon on melee user component. */
	void HandleItemAddedToLoadout_Implementation();

	void HandleItemRemovedFromLoadout_Implementation();

	/** Activate melee weapon on user component. */
	void HandleItemEquipped_Implementation();

	void HandleItemUnequipped_Implementation();

	/** Remove from melee user component if it's there. */
	void HandleItemDestruction() override;

};
