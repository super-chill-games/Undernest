// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Inventory/PMTRTUBaseItemDefinition.h"
#include "UNDBaseItemDefinition.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDBaseItemDefinition : public UPMTRTUBaseItemDefinition
{
	GENERATED_BODY()

public:

	UUNDBaseItemDefinition();
	
};
