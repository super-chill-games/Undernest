// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Items/UNDBaseItemDefinition.h"
#include "UNDQuantifiableItemDefinition.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDQuantifiableItemDefinition : public UUNDBaseItemDefinition
{
	GENERATED_BODY()

public:

	UUNDQuantifiableItemDefinition();

	/** Retrieves item's base max quantity. */
	UFUNCTION(BlueprintPure, Category = "UND|Item")
	int32 GetBaseMaxQuantity() const { return BaseMaxQuantity; } 

protected:

	/** Base max quantity for the item. */
	UPROPERTY(EditAnywhere, Category = "PMT")
	int32 BaseMaxQuantity = 1;
	
};
