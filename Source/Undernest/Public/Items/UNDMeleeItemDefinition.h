// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Items/UNDBaseItemDefinition.h"
#include "UNDMeleeItemDefinition.generated.h"

/**
 * Base item definition used for melee weapons.
 */
UCLASS()
class UNDERNEST_API UUNDMeleeItemDefinition : public UUNDBaseItemDefinition
{
	GENERATED_BODY()

public:

	UUNDMeleeItemDefinition();

	/** Retrieves melee data. */
	UFUNCTION(BlueprintPure, Category = "UND|Item|Melee")
	TSubclassOf<UUNDMeleeData> GetMeleeData() const { return MeleeData; } 

protected:

	/** Static data for the melee weapon. */
	UPROPERTY(EditAnywhere, Category = "UND")
	TSubclassOf<UUNDMeleeData> MeleeData;
};
