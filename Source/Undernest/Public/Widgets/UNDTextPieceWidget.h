// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/PMTWBaseUserWidget.h"

// UND Includes
#include "UI/Messages/Types/UNDTextPieceMessageData.h"

#include "UNDTextPieceWidget.generated.h"

class UPMTWBaseRichTextBlock;

/**
 * Simple widget that is capable to show text pieces on screen.
 */
UCLASS()
class UNDERNEST_API UUNDTextPieceWidget : public UPMTWBaseUserWidget
{
	GENERATED_BODY()

public:

	/** Called when a new text piece should be set. */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UND|Widget|TextPiece")
	void HandleNewTextPiece(const FText& InMessage);

	/** Called when the current text piece has been removed.
	* Calling GetCurrentTextPiece will retrieve the next text, if any. */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "UND|Widget|TextPiece")
	void HandleCurrentTextPieceRemoved();

	/** Verifies if we have any remaining text to display. */
	UFUNCTION(BlueprintPure, Category = "UND|Widget|TextPiece")
	bool HasAnyRemainingText() const { return TextPieces.Num() > 0; }

	/** Retrieves the current text piece. */
	UFUNCTION(BlueprintPure, Category = "UND|Widget|TextPiece")
	FText GetCurrentTextPiece() const;

protected:

	/** Bind to messages. */
	void OnPlayerContextInitialized() override;

	/** Try to display text. */
	void OnTextPieceAddedMessageReceived(const FPMTCCustomData& MessageData);

	/** Try to remove text. */
	void OnTextPieceRemovedMessageReceived(const FPMTCCustomData& MessageData);

	/** Text used to show the text piece. */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidgetOptional))
	TObjectPtr<UPMTWBaseRichTextBlock> RICHTEXT_TextPiece;

	/** All text pieces we have. */
	UPROPERTY(Transient)
	TArray<FUNDTextPieceMessageData> TextPieces;
};
