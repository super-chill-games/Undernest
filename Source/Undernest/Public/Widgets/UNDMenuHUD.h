// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/UNDHUDBase.h"
#include "UNDMenuHUD.generated.h"

class UUNDMenuWidget;

/**
 * HUD used for the main menu.
 */
UCLASS(Abstract)
class UNDERNEST_API AUNDMenuHUD : public AUNDHUDBase
{
	GENERATED_BODY()
};
