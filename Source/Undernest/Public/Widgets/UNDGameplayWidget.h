// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/PMTWBaseUserWidget.h"
#include "UNDGameplayWidget.generated.h"

/**
 * Main widget used for gameplay.
 */
UCLASS()
class UNDERNEST_API UUNDGameplayWidget : public UPMTWBaseUserWidget
{
	GENERATED_BODY()
	
};
