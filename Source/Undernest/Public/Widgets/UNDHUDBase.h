// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/PMTRTUHUDBase.h"
#include "UNDHUDBase.generated.h"

/**
 * Base hud class.
 */
UCLASS(Abstract)
class UNDERNEST_API AUNDHUDBase : public APMTRTUHUDBase
{
	GENERATED_BODY()

public:

	/** If we are in HUBWorld, then notify about it. */
	void BeginPlay() override;
};
