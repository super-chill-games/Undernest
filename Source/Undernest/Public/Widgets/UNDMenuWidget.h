// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/PMTWBaseUserWidget.h"
#include "UNDMenuWidget.generated.h"

/**
 * Widget used for the main menu.
 */
UCLASS(Abstract)
class UNDERNEST_API UUNDMenuWidget : public UPMTWBaseUserWidget
{
	GENERATED_BODY()
	
};
