// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/PMTWBaseUserWidget.h"
#include "UNDLevelNameWidget.generated.h"

class UPMTWBaseRichTextBlock;
class UPMTWBaseImage;
class UUNDLevelDefinition;

/**
 * Widget that displays the name of the current level when it's created.
 */
UCLASS()
class UNDERNEST_API UUNDLevelNameWidget : public UPMTWBaseUserWidget
{
	GENERATED_BODY()

public:

	/** Called when the level is created. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Widget")
	void HandleLevelDisplayed(const UUNDLevelDefinition* LevelDefinition);

protected:

	/** Retrieve player's world and the level definition. */
	void OnPlayerContextInitialized() override;

	/** Name of the level, if applies. */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidgetOptional))
	TObjectPtr<UPMTWBaseRichTextBlock> TEXT_LevelName;

	/** Image of the level, if applies. */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidgetOptional))
	TObjectPtr<UPMTWBaseImage> IMAGE_Level;
	
};
