// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

// UE Includes
#include "GameplayTagContainer.h"

#include "UNDCorruptionInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUNDCorruptionInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface for those objects that desire to listen for corruption events.
 */
class UNDERNEST_API IUNDCorruptionInterface
{
	GENERATED_BODY()

public:

	/** Called each time that the corruption state changes. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Corruption")
	void HandleCorruptionStateChange(const FGameplayTag& OldCorruptionState, const FGameplayTag& NewCorruptionState);

	/** Retrieves the current corruption state. By default
	* we look for the world's state. Can be overriden in case
	* some actors have a custom logic to handle the corruption state. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Corruption")
	FGameplayTag GetCorruptionState() const;
	FGameplayTag GetCorruptionState_Implementation() const;

	/** Verifies if this is corrupted. */
	bool IsCorrupted() const;
};
