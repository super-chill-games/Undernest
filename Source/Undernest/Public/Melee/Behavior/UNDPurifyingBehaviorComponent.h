﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ActorComponents/PMTMWMeleeBehaviorComponent.h"

// UE Includes
#include "NiagaraComponent.h"

// PMTVFX Includes
#include "Types/PMTVFXTypes.h"

#include "UNDPurifyingBehaviorComponent.generated.h"

class UNiagaraComponent;

USTRUCT(BlueprintType)
struct FUNDAbsorbAffectedActor
{
	GENERATED_BODY()
public:

	FUNDAbsorbAffectedActor() {}

	FUNDAbsorbAffectedActor(AActor* InActor) : Actor(InActor) {}

	FUNDAbsorbAffectedActor(AActor* InActor, UNiagaraComponent* InNiagaraComponent) : Actor(InActor), NiagaraComponent(InNiagaraComponent) {}

	UPROPERTY(BlueprintReadWrite, Transient)
	TObjectPtr<AActor> Actor = nullptr;

	UPROPERTY(BlueprintReadWrite, Transient)
	TObjectPtr<UNiagaraComponent> NiagaraComponent = nullptr;
	
};

/** Component that controls the effects to display for each actor we are absorbing. */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class UNDERNEST_API UUNDPurifyingBehaviorComponent : public UPMTMWMeleeBehaviorComponent
{
	GENERATED_BODY()

public:

	UUNDPurifyingBehaviorComponent();

	/** Add the given actor as affected. If it exists, then this doesn't do anything. */
	void AddAffectedActor(AActor* InActor);

	/** Remove the given actor as affected. If it doesn't exists, then this doesn't do anything. */
	void RemoveAffectedActor(AActor* InActor);

	/** Called when this starts the purifying process. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Purifying")
	void HandleStartPurifying();

	/** Called when this stops the purifying process. */
	UFUNCTION(BlueprintNativeEvent, Category = "UND|Purifying")
	void HandleStopPurifying();

	/** Clear all affected actors and VFX. */
	UFUNCTION(BlueprintCallable, Category = "UND|Purifying")
	void StopPurify();

	/** Retrieves all affected actors. */
	UFUNCTION(BlueprintPure, Category = "UND|Purifying")
	const TArray<FUNDAbsorbAffectedActor>& GetAffectedActors() const { return AffectedActors; }

protected:
	/** Modify spawned effect if needed. */
	UFUNCTION(BlueprintNativeEvent, BlueprintCosmetic, Category = "UND")
	void HandleVFXSpawned(AActor* AffectedActor, UNiagaraComponent* NiagaraComponent);
	
	/** VFX that will be spawned for the affected actor. */
	UPROPERTY(EditAnywhere, Category = "UND")
	FPMTVFXInstantiationParameters AbsorbVFX;
	
	/** All actors we are currently affecting. */
	UPROPERTY(Transient)
	TArray<FUNDAbsorbAffectedActor> AffectedActors;
};
