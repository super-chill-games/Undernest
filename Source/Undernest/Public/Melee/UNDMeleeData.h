// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Melee/PMTMWMeleeData.h"
#include "UNDMeleeData.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDMeleeData : public UPMTMWMeleeData
{
	GENERATED_BODY()
	
};
