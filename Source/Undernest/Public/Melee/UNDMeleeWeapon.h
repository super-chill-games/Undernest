// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Melee/PMTRTUMeleeWeapon.h"
#include "UNDMeleeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API AUNDMeleeWeapon : public APMTRTUMeleeWeapon
{
	GENERATED_BODY()
	
};
