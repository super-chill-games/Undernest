// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Loot/PMTLOLootTable.h"
#include "UNDLootTable.generated.h"

/**
 * Main loot table for undernest.
 */
UCLASS()
class UNDERNEST_API UUNDLootTable : public UPMTLOLootTable
{
	GENERATED_BODY()
	
};
