// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"

// UE Includes
#include "GameplayTagContainer.h"

#include "UNDGameFlowWorldSubsystem.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(UNDGameFlowLog, Log, All);

DECLARE_DELEGATE_OneParam(FUNDGameFlowEvent, const FGameplayTag&);
DECLARE_DYNAMIC_DELEGATE_OneParam(FUNDGameFlowDynamicEvent, const FGameplayTag&, Event);

DECLARE_MULTICAST_DELEGATE_OneParam(FUNDGameFlowMultiEvent, const FGameplayTag&);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUNDGameFlowMultiDynamicEvent, const FGameplayTag&, Event);

USTRUCT()
struct UNDERNEST_API FUNDGameFlowEventDelegates
{
	GENERATED_BODY()

public:

	FUNDGameFlowEventDelegates() {}

	FUNDGameFlowEventDelegates(const FGameplayTag& Event) : EventTag(Event) {}

	/** Event identifier. */
	FGameplayTag EventTag;

	/** Blueprint event called when the event has been notified. */
	UPROPERTY(Transient)
	FUNDGameFlowMultiDynamicEvent OnEvent;

	/** Native event called when the event has been notified. */
	FUNDGameFlowMultiEvent OnEventNative;

	/** Whether the event has been notified. */
	bool bHasEventBeingNotified = false;
};

/**
 * World subsystem that has control over some events.
 */
UCLASS()
class UNDERNEST_API UUNDGameFlowWorldSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()

public:

	/** Retrieves the game flow world subsystem from the given context object. */
	UFUNCTION(BlueprintPure, Category = "UND|GameFlow")
	static UUNDGameFlowWorldSubsystem* GetGameFlowWorldSSFromContext(UObject* WorldContext);

	/** Notifies to everyone listening for the given event.
	* @param bForceNotify - If true, it will notify the event even if it was fired before. If false,
	* and the event was fired, it will be ignored.
	* @param Caller - Optional parameter used to display on logs who sent the event.
	* Can be ignored on blueprint since it will default to self, which should be the standard parameter. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameFlow", meta = (DefaultToSelf = "Caller"))
	void NotifyGameFlowEvent(const FGameplayTag& EventTag, bool bForceNotify = true, UObject* Caller = nullptr);

	/** Bind the given event when the menu loads, or if it's already loaded, then broadcast the given event.
	* @param bBroadcastIfAlreadyNotified - If true, then if the event has been notified already, the given event will
	* be called. If false, we are not going to do anything if the event was sent already.
	* No binding will be done if the gameplay tag is empty. */
	UFUNCTION(BlueprintCallable, Category = "UND|GameFlow")
	void BindOrCallForEvent(const FGameplayTag& EventTag, FUNDGameFlowDynamicEvent Event, bool bBroadcastIfAlreadyNotified = true);

	/** Native version to bind for event. */
	void BindOrCallForEvent(const FGameplayTag& EventTag, FUNDGameFlowEvent Event, bool bBroadcastIfAlreadyNotified = true);

protected:

	/** Retrieves the delegates for the given event tag. */
	FUNDGameFlowEventDelegates& GetOrCreateEventForTag(const FGameplayTag& EventTag);

	/** Blueprint event called when the menu level has loaded. */
	UPROPERTY(Transient)
	TArray<FUNDGameFlowEventDelegates> Events;
};
