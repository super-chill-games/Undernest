// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "UNDCorruptionWorldSubsystem.generated.h"

class UUNDCorruptionManager;

/**
 * World subsystem that takes care of handling everything related to corruption.
 */
UCLASS(Config = Game)
class UNDERNEST_API UUNDCorruptionWorldSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()

public:

	UUNDCorruptionWorldSubsystem();
	
	/** Create corruption manager. */
	void Initialize(FSubsystemCollectionBase& Collection) override;

	/** Call initialize on corruption manager. */
	void PostInitialize() override;

	/** Retrieves corruption world subsystem from the given context. */
	UFUNCTION(BlueprintCallable, Category = "UND|Corruption")
	static UUNDCorruptionWorldSubsystem* GetCorruptionWorldSSFromContext(UObject* WorldContext);

	/** Updates corruption's radius. */
	UFUNCTION(BlueprintCallable, Category = "UND|Corruption", meta = (WorldContext = "WorldContextObject"))
	static void UpdateCorruptionRadius(UObject* WorldContextObject, float Radius);

	/** Updates corruption's center. */
	UFUNCTION(BlueprintCallable, Category = "UND|Corruption", meta = (WorldContext = "WorldContextObject"))
	static void UpdateCorruptionCenter(UObject* WorldContextObject, const FVector& Center);

	/** Retrieves corruption's manager. */
	UFUNCTION(BlueprintPure, Category = "UND|Corruption")
	UUNDCorruptionManager* GetCorruptionManager() const;

protected:

	/** Class that holds references to required stuff to handle corruption. */
	UPROPERTY(globalconfig, EditAnywhere, Category = "UND", meta = (MetaClass = "/Script/Undernest.UNDCorruptionManager", DisplayName = "Corruption Manager Class"))
	FSoftClassPath CorruptionManagerClass;

	/** Corruption's manager. Hold required data for some stuff related to corruption. */
	UPROPERTY(Transient)
	TObjectPtr<UUNDCorruptionManager> CorruptionManager = nullptr;
};
