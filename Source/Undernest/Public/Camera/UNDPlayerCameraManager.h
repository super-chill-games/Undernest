// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Actors/PMTRTUPlayerCameraManager.h"
#include "UNDPlayerCameraManager.generated.h"

/**
 * Base camera manager for players, that handles transitions and fades.
 */
UCLASS()
class UNDERNEST_API AUNDPlayerCameraManager : public APMTRTUPlayerCameraManager
{
	GENERATED_BODY()

public:

	AUNDPlayerCameraManager();

	/** Update fade transition parameter. */
	UFUNCTION(BlueprintCallable, Category = "UND|Camera")
	void UpdateFadeTransition(float Value);

protected:

	/** Parameter used to use a transition between levels. */
	UPROPERTY(EditDefaultsOnly, Category = "UND|Transition")
	FName FadeTransitionParameterName;
	
};
