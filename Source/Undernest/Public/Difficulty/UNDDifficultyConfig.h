// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Difficulty/PMTCDifficultyConfig.h"
#include "UNDDifficultyConfig.generated.h"

/**
 * Difficulty config that is based on the amount of non rest areas we have completed so far.
 */
UCLASS()
class UNDERNEST_API UUNDDifficultyConfig : public UPMTCDifficultyConfig
{
	GENERATED_BODY()

public:

	UUNDDifficultyConfig();

	/** Difficulty level will be measured by how many areas we have cleared so far. */
	float GetDifficultyAsNumber_Implementation(const UObject* WorldContext) const override;
	
};
