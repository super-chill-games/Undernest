// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Animation/PMTRTUBaseAnimInstance.h"
#include "UNDBaseAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDBaseAnimInstance : public UPMTRTUBaseAnimInstance
{
	GENERATED_BODY()
	
};
