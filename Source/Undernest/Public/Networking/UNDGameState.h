// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Networking/PMTRTUBaseGameState.h"
#include "UNDGameState.generated.h"

/**
 * Base game state for undernest.
 */
UCLASS()
class UNDERNEST_API AUNDGameState : public APMTRTUBaseGameState
{
	GENERATED_BODY()

public:

	AUNDGameState();
	
};
