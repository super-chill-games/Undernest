// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Networking/PMTRTUBaseGameMode.h"
#include "UNDGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API AUNDGameMode : public APMTRTUBaseGameMode
{
	GENERATED_BODY()
	
};
