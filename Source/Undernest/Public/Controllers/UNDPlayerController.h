// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Controllers/Player/PMTRTUBasePlayerController.h"
#include "UNDPlayerController.generated.h"

/**
 * Base player controller class for undernest.
 */
UCLASS()
class UNDERNEST_API AUNDPlayerController : public APMTRTUBasePlayerController
{
	GENERATED_BODY()

public:

	/** Enable input for us and the character. */
	void PostSeamlessTravel() override;
	
};
