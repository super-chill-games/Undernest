﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CameraBlockingVolume.h"
#include "UNDCameraBlockingVolume.generated.h"

/** Ready to use blocking volume to use with our cameras. */
UCLASS()
class UNDERNEST_API AUNDCameraBlockingVolume : public ACameraBlockingVolume
{
	GENERATED_BODY()

public:

	AUNDCameraBlockingVolume();
};
