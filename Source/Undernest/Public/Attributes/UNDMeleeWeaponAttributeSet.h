﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Attributes/PMTMWMeleeWeaponAttributeSet.h"
#include "UNDMeleeWeaponAttributeSet.generated.h"

/**
 * 
 */
UCLASS()
class UNDERNEST_API UUNDMeleeWeaponAttributeSet : public UPMTMWMeleeWeaponAttributeSet
{
	GENERATED_BODY()

public:

	/** Damage done to fungi life. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayAttributeData FungiDamage;
	PMTMW_ATTRIBUTE_ACCESSORS(UUNDMeleeWeaponAttributeSet, FungiDamage);

protected:
};
