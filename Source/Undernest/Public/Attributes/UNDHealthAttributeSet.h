﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Attributes/PMTRTURepHealthAttributeSet.h"
#include "UNDHealthAttributeSet.generated.h"

/**
 * Attribute set that has fungi health too.
 */
UCLASS()
class UNDERNEST_API UUNDHealthAttributeSet : public UPMTRTURepHealthAttributeSet
{
	GENERATED_BODY()
public:

	UUNDHealthAttributeSet();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/** Clamp fungi health to 0.0. */
	void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;

	/** Clamp fungi health to 0.0. */
	void PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const override;

	PMTC_ATTRIBUTE_ACCESSORS(UUNDHealthAttributeSet, FungiHealth);
	PMTC_ATTRIBUTE_ACCESSORS(UUNDHealthAttributeSet, MaxFungiHealth);
	PMTC_ATTRIBUTE_ACCESSORS(UUNDHealthAttributeSet, FungiRecoveryPerSecond);
	PMTC_ATTRIBUTE_ACCESSORS(UUNDHealthAttributeSet, HealthStunRecoverInSeconds);
	PMTC_ATTRIBUTE_ACCESSORS(UUNDHealthAttributeSet, HealthRecoverAfterStun);
	PMTC_ATTRIBUTE_ACCESSORS(UUNDHealthAttributeSet, MaxFungiDamageAllowedPerStunState);

	UPROPERTY(ReplicatedUsing = OnRep_FungiHealth)
	FPMTCHitGameplayAttributeData FungiHealth;

	UPROPERTY(ReplicatedUsing = OnRep_MaxFungiHealth)
	FPMTCGameplayAttributeData MaxFungiHealth;

	UPROPERTY(ReplicatedUsing = OnRep_FungiRecoveryPerSecond)
	FPMTCGameplayAttributeData FungiRecoveryPerSecond;

	/** How much time does a character take on recover its health after being stunned. */
	UPROPERTY()
	FPMTCGameplayAttributeData HealthStunRecoverInSeconds;

	/** How much health do we recover after being stunned. */
	UPROPERTY()
	FPMTCGameplayAttributeData HealthRecoverAfterStun;

	/** When stunned, what's the max allowed damage we are permitted to do to the fungi health. */
	UPROPERTY()
	FPMTCGameplayAttributeData MaxFungiDamageAllowedPerStunState;

	
protected:
	
	UFUNCTION()
	void OnRep_FungiHealth(const FPMTCHitGameplayAttributeData& OldValue);

	UFUNCTION()
	void OnRep_MaxFungiHealth(const FPMTCGameplayAttributeData& OldValue);

	UFUNCTION()
	void OnRep_FungiRecoveryPerSecond(const FPMTCGameplayAttributeData& OldValue);
};
