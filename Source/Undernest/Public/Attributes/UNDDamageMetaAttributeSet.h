﻿// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Attributes/PMTRTUDamageMetaAttributeSet.h"
#include "UNDDamageMetaAttributeSet.generated.h"

/**
 * Damage meta attribute set that also has fungi damage.
 */
UCLASS()
class UNDERNEST_API UUNDDamageMetaAttributeSet : public UPMTRTUDamageMetaAttributeSet
{
	GENERATED_BODY()

public:

	UUNDDamageMetaAttributeSet();
	
	PMTC_ATTRIBUTE_ACCESSORS(UUNDDamageMetaAttributeSet, MetaFungiDamage);
	PMTC_ATTRIBUTE_ACCESSORS(UUNDDamageMetaAttributeSet, AccumulatedFungiDamage);
	
	UPROPERTY()
	FGameplayAttributeData MetaFungiDamage;

	/** Do not modify ever this variable, this will be used to store how much fungi damage we have done
	* to the target so we can avoid doing more damage than the one we specified. */
	UPROPERTY()
	FGameplayAttributeData AccumulatedFungiDamage;
};
