// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Characters/PMTRTUBaseCharacter.h"
#include "UNDCharacter.generated.h"

/**
 * Base character for undernest.
 */
UCLASS(Abstract)
class UNDERNEST_API AUNDCharacter : public APMTRTUBaseCharacter
{
	GENERATED_BODY()

public:
	
	AUNDCharacter(const FObjectInitializer& InObjectInitializer);

	/** Make native bindings based on default input config. */
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Whether we have our movement blocked. */
	UFUNCTION(BlueprintPure, Category = "UND|Character")
	bool HasMovementBlocked() const;
	
protected:

	/** Move our character. */
	void Input_Move(const FInputActionValue& InputActionValue);

	/** Try open pause menu. */
	void Input_Pause(const FInputActionValue& InputActionValue);
};
