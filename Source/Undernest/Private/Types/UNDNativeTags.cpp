// Copyright Super Chill Games. All Rights Reserved.


#include "Types/UNDNativeTags.h"

namespace UNDNativeTags
{

UE_DEFINE_GAMEPLAY_TAG(AbilityInputAttackA, FName(TEXT("Ability.Input.AttackA")));
UE_DEFINE_GAMEPLAY_TAG(AbilityInputAttackB, FName(TEXT("Ability.Input.AttackB")));
UE_DEFINE_GAMEPLAY_TAG(AbilityInputDash, FName(TEXT("Ability.Input.Dash")));
UE_DEFINE_GAMEPLAY_TAG(AbilityInputSpecialAttack, FName(TEXT("Ability.Input.SpecialAttack")));
UE_DEFINE_GAMEPLAY_TAG(AbilityInputSpecialAttackHold, FName(TEXT("Ability.Input.SpecialAttackHold")));
	
UE_DEFINE_GAMEPLAY_TAG(AbilityParryChain, FName(TEXT("Ability.ParryChain")));
UE_DEFINE_GAMEPLAY_TAG(AbilityProjectile, FName(TEXT("Ability.Projectile")));

UE_DEFINE_GAMEPLAY_TAG(Area, FName(TEXT("Area")));
UE_DEFINE_GAMEPLAY_TAG(AreaDesert, FName(TEXT("Area.Desert")));
UE_DEFINE_GAMEPLAY_TAG(AreaExterior, FName(TEXT("Area.Exterior")));
UE_DEFINE_GAMEPLAY_TAG(AreaForest, FName(TEXT("Area.Forest")));

UE_DEFINE_GAMEPLAY_TAG(CharacterDescriptorAnt, FName(TEXT("Character.Descriptor.Ant")));
UE_DEFINE_GAMEPLAY_TAG(CharacterType, FName(TEXT("Character.Type")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeDestructor, FName(TEXT("Character.Type.Destructor")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeDigger, FName(TEXT("Character.Type.Digger")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeFungiMage, FName(TEXT("Character.Type.FungiMage")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeHeavy, FName(TEXT("Character.Type.Heavy")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeLancer, FName(TEXT("Character.Type.Lancer")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeMage, FName(TEXT("Character.Type.Mage")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeShielded, FName(TEXT("Character.Type.Shielded")));
UE_DEFINE_GAMEPLAY_TAG(CharacterTypeShootingPlant, FName(TEXT("Character.Type.ShootingPlant")));

UE_DEFINE_GAMEPLAY_TAG(CooldownAbility, FName(TEXT("Cooldown.Ability")));
UE_DEFINE_GAMEPLAY_TAG(CooldownAbilityDash, FName(TEXT("Cooldown.Ability.Dash")));
UE_DEFINE_GAMEPLAY_TAG(CooldownConsumable, FName(TEXT("Cooldown.Consumable")));
UE_DEFINE_GAMEPLAY_TAG(CooldownConsumableHoney, FName(TEXT("Cooldown.Consumable.Honey")));

UE_DEFINE_GAMEPLAY_TAG(CorruptionSource, FName(TEXT("Corruption.Source")));
UE_DEFINE_GAMEPLAY_TAG(CorruptionState, FName(TEXT("Corruption.State")));
UE_DEFINE_GAMEPLAY_TAG(CorruptionStateClear, FName(TEXT("Corruption.State.Clear")));
UE_DEFINE_GAMEPLAY_TAG(CorruptionStateCorrupted, FName(TEXT("Corruption.State.Corrupted")));
UE_DEFINE_GAMEPLAY_TAG(CorruptionStateVulnerable, FName(TEXT("Corruption.State.Vulnerable")));

UE_DEFINE_GAMEPLAY_TAG(DialogueTypeIdentifierFreedom, FName(TEXT("Dialogue.Type.Identifier.Freedom")));
	
UE_DEFINE_GAMEPLAY_TAG(GameFlowBossBattleBegin, FName(TEXT("GameFlow.BossBattle.Begin")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowBossBattleEnd, FName(TEXT("GameFlow.BossBattle.End")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowCorruptionSourcesDestroyed, FName(TEXT("GameFlow.CorruptionSources.Destroyed")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowDoorsClose, FName(TEXT("GameFlow.Doors.Close")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowDoorsOpen, FName(TEXT("GameFlow.Doors.Open")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowEndGame, FName(TEXT("GameFlow.EndGame")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowIntroVideoFinished, FName(TEXT("GameFlow.IntroVideoFinished")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowIntroVideoStarted, FName(TEXT("GameFlow.IntroVideoStarted")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowLevelSequenceStarted, FName(TEXT("GameFlow.Level.Sequence.Started")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowLevelSequenceFinished, FName(TEXT("GameFlow.Level.Sequence.Finished")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowLevelLoaded, FName(TEXT("GameFlow.LevelLoaded")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowLevelLoadedMenu, FName(TEXT("GameFlow.LevelLoaded.Menu")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowLevelMissionCompleted, FName(TEXT("GameFlow.LevelMissionCompleted")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowPlayerDied, FName(TEXT("GameFlow.Player.Died")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowPlayerEnteredToBossArea, FName(TEXT("GameFlow.Player.EnteredToBossArea")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowPlayerEnteredToDoor, FName(TEXT("GameFlow.Player.EnteredToDoor")));
UE_DEFINE_GAMEPLAY_TAG(GameFlowPlayerStart, FName(TEXT("GameFlow.PlayerStart")));

UE_DEFINE_GAMEPLAY_TAG(GameplayCueAbilitySelfDestruct, FName(TEXT("GameplayCue.Ability.SelfDestruct")));
UE_DEFINE_GAMEPLAY_TAG(GameplayCueAbsorbAreaFungiRod, FName(TEXT("GameplayCue.Absorb.Area.Fungi.Rod")));
UE_DEFINE_GAMEPLAY_TAG(GameplayCueAbsorbFungiRod, FName(TEXT("GameplayCue.Absorb.Fungi.Rod")));
UE_DEFINE_GAMEPLAY_TAG(GameplayCueHitAgainstWall, FName(TEXT("GameplayCue.HitAgainstWall")));
UE_DEFINE_GAMEPLAY_TAG(GameplayCueMageTeleportLocation, FName(TEXT("GameplayCue.Mage.TeleportLocation")));
UE_DEFINE_GAMEPLAY_TAG(GameplayCueParryChainInput, FName(TEXT("GameplayCue.ParryChainInput")));
	
UE_DEFINE_GAMEPLAY_TAG(GameplayEffectAbsorbFungi, FName(TEXT("GameplayEffect.AbsorbFungi")));
UE_DEFINE_GAMEPLAY_TAG(GameplayEffectBossImmunity, FName(TEXT("GameplayEffect.Boss.Immunity")));
UE_DEFINE_GAMEPLAY_TAG(GameplayEffectFungiStun, FName(TEXT("GameplayEffect.Fungi.Stun")));
UE_DEFINE_GAMEPLAY_TAG(GameplayEffectIgnoreHit, FName(TEXT("GameplayEffect.Ignore.Hit")));
UE_DEFINE_GAMEPLAY_TAG(GameplayEffectNonRescueAnt, FName(TEXT("GameplayEffect.NonRescueAnt")));
UE_DEFINE_GAMEPLAY_TAG(GameplayEffectNormalStun, FName(TEXT("GameplayEffect.NormalStun")));

UE_DEFINE_GAMEPLAY_TAG(EventPurifyRodFlash, FName(TEXT("Event.Purify.RodFlash")));
UE_DEFINE_GAMEPLAY_TAG(EventStartPurifyAbsorb, FName(TEXT("Event.StartPurify.Absorb")));
UE_DEFINE_GAMEPLAY_TAG(EventSpawnProjectile, FName(TEXT("Event.Spawn.Projectile")));

UE_DEFINE_GAMEPLAY_TAG(EventUIGenericKill, FName(TEXT("Event.UI.Generic.Kill")));
UE_DEFINE_GAMEPLAY_TAG(EventUIGenericVisibility, FName(TEXT("Event.UI.Generic.Visibility")));
UE_DEFINE_GAMEPLAY_TAG(EventUIHUBWorld, FName(TEXT("Event.UI.HUBWorld")));
UE_DEFINE_GAMEPLAY_TAG(EventUILaboratory, FName(TEXT("Event.UI.Laboratory")));
UE_DEFINE_GAMEPLAY_TAG(EventUIRescuedAnts, FName(TEXT("Event.UI.RescuedAnts")));
UE_DEFINE_GAMEPLAY_TAG(EventUITextPieceAdded, FName(TEXT("Event.UI.TextPiece.Added")));
UE_DEFINE_GAMEPLAY_TAG(EventUITextPieceRemoved, FName(TEXT("Event.UI.TextPiece.Removed")));

UE_DEFINE_GAMEPLAY_TAG(ImmunityAI, FName(TEXT("Immunity.AI")));
UE_DEFINE_GAMEPLAY_TAG(ImmunityFungi, FName(TEXT("Immunity.Fungi")));

UE_DEFINE_GAMEPLAY_TAG(InputTagAttackA, FName(TEXT("InputTag.Attack.A")));
UE_DEFINE_GAMEPLAY_TAG(InputTagAttackB, FName(TEXT("InputTag.Attack.B")));
UE_DEFINE_GAMEPLAY_TAG(InputTagDash, FName(TEXT("InputTag.Dash")));
UE_DEFINE_GAMEPLAY_TAG(InputTagInteract, FName(TEXT("InputTag.Interact")));
UE_DEFINE_GAMEPLAY_TAG(InputTagSpecialAttack, FName(TEXT("InputTag.SpecialAttack")));
UE_DEFINE_GAMEPLAY_TAG(InputTagSpecialAttackHold, FName(TEXT("InputTag.SpecialAttackHold")));
UE_DEFINE_GAMEPLAY_TAG(InputTagUseItem, FName(TEXT("InputTag.UseItem")));
UE_DEFINE_GAMEPLAY_TAG(InputTagUseItemHoney, FName(TEXT("InputTag.UseItem.Honey")));

UE_DEFINE_GAMEPLAY_TAG(InteractionDialogue, FName(TEXT("Interaction.Dialogue")));

UE_DEFINE_GAMEPLAY_TAG(InventoryItemMelee, FName(TEXT("Inventory.Item.Melee")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemMeleeHammer, FName(TEXT("Inventory.Item.Melee.Hammer")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemMeleeRod, FName(TEXT("Inventory.Item.Melee.Rod")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemMeleeSpear, FName(TEXT("Inventory.Item.Melee.Spear")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemMeleeStaff, FName(TEXT("Inventory.Item.Melee.Staff")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemMeleeSword, FName(TEXT("Inventory.Item.Melee.Sword")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemMeleeSwordFernit, FName(TEXT("Inventory.Item.Melee.Sword.Fernit")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemQuantifiable, FName(TEXT("Inventory.Item.Quantifiable")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemQuantifiableFungiEnergy, FName(TEXT("Inventory.Item.Quantifiable.FungiEnergy")));
UE_DEFINE_GAMEPLAY_TAG(InventoryItemQuantifiableHoney, FName(TEXT("Inventory.Item.Quantifiable.Honey")));

UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorBoss, FName(TEXT("Level.Descriptor.Boss")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorCombat, FName(TEXT("Level.Descriptor.Combat")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorDontSave, FName(TEXT("Level.Descriptor.DontSave")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorExterior, FName(TEXT("Level.Descriptor.Exterior")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorFinalArea, FName(TEXT("Level.Descriptor.FinalArea")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorHub, FName(TEXT("Level.Descriptor.Hub")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorLaboratory, FName(TEXT("Level.Descriptor.Laboratory")));
UE_DEFINE_GAMEPLAY_TAG(LevelDescriptorRest, FName(TEXT("Level.Descriptor.Rest")));

UE_DEFINE_GAMEPLAY_TAG(MeleeHammer, FName(TEXT("Melee.Hammer")));
UE_DEFINE_GAMEPLAY_TAG(MeleeHammerBasic, FName(TEXT("Melee.Hammer.Basic")));
UE_DEFINE_GAMEPLAY_TAG(MeleeRod, FName(TEXT("Melee.Rod")));
UE_DEFINE_GAMEPLAY_TAG(MeleeRodPurifying, FName(TEXT("Melee.Rod.Purifying")));
UE_DEFINE_GAMEPLAY_TAG(MeleeSpear, FName(TEXT("Melee.Spear")));
UE_DEFINE_GAMEPLAY_TAG(MeleeSpearBasic, FName(TEXT("Melee.Spear.Basic")));
UE_DEFINE_GAMEPLAY_TAG(MeleeStaff, FName(TEXT("Melee.Staff")));
UE_DEFINE_GAMEPLAY_TAG(MeleeStaffBasic, FName(TEXT("Melee.Staff.Basic")));
UE_DEFINE_GAMEPLAY_TAG(MeleeSword, FName(TEXT("Melee.Sword")));
UE_DEFINE_GAMEPLAY_TAG(MeleeSwordBasic, FName(TEXT("Melee.Sword.Basic")));
UE_DEFINE_GAMEPLAY_TAG(MeleeSwordFernit, FName(TEXT("Melee.Sword.Fernit")));

UE_DEFINE_GAMEPLAY_TAG(MissionDescriptorFreeNPC, FName(TEXT("Mission.Descriptor.FreeNPC")));
UE_DEFINE_GAMEPLAY_TAG(MissionTargetBossObjective, FName(TEXT("Mission.Target.BossObjective")));

UE_DEFINE_GAMEPLAY_TAG(NPCIdentifierConie, FName(TEXT("NPC.Identifier.Conie")));
UE_DEFINE_GAMEPLAY_TAG(NPCIdentifierCornelio, FName(TEXT("NPC.Identifier.Cornelio")));
UE_DEFINE_GAMEPLAY_TAG(NPCIdentifierMortis, FName(TEXT("NPC.Identifier.Mortis")));
UE_DEFINE_GAMEPLAY_TAG(NPCIdentifierVanis, FName(TEXT("NPC.Identifier.Vanis")));

UE_DEFINE_GAMEPLAY_TAG(PerkDescriptorHideFromList, FName(TEXT("Perk.Descriptor.HideFromList")));
	
UE_DEFINE_GAMEPLAY_TAG(PerkIdentifierAbsorbImprovement, FName(TEXT("Perk.Identifier.AbsorbImprovement")));
UE_DEFINE_GAMEPLAY_TAG(PerkIdentifierAbsorbRangeImprovement, FName(TEXT("Perk.Identifier.AbsorbRangeImprovement")));
UE_DEFINE_GAMEPLAY_TAG(PerkIdentifierFungiLostPerDeath, FName(TEXT("Perk.Identifier.FungiLostPerDeath")));
UE_DEFINE_GAMEPLAY_TAG(PerkIdentifierMaxHealth, FName(TEXT("Perk.Identifier.MaxHealth")));
UE_DEFINE_GAMEPLAY_TAG(PerkIdentifierMovementSpeed, FName(TEXT("Perk.Identifier.MovementSpeed")));

UE_DEFINE_GAMEPLAY_TAG(PerkReceiverMelee, FName(TEXT("Perk.Receiver.Melee")));
UE_DEFINE_GAMEPLAY_TAG(PerkReceiverMeleeFernitSword, FName(TEXT("Perk.Receiver.Melee.FernitSword")));

UE_DEFINE_GAMEPLAY_TAG(PlayerStartRevive, FName(TEXT("PlayerStart.Revive")));
	
UE_DEFINE_GAMEPLAY_TAG(SetByCallerGenericFungiDamage, FName(TEXT("SetByCaller.Generic.FungiDamage")));
UE_DEFINE_GAMEPLAY_TAG(SetByCallerGenericFungiHealth, FName(TEXT("SetByCaller.Generic.FungiHealth")));
UE_DEFINE_GAMEPLAY_TAG(SetByCallerGenericFungiMaxHealth, FName(TEXT("SetByCaller.Generic.FungiMaxHealth")));
UE_DEFINE_GAMEPLAY_TAG(SetByCallerLaunchDuration, FName(TEXT("SetByCaller.Launch.Duration")));
UE_DEFINE_GAMEPLAY_TAG(SetByCallerMaxWalkSpeed, FName(TEXT("SetByCaller.MaxWalkSpeed")));
	
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeDestructor, FName(TEXT("Skeleton.Type.Destructor")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeDigger, FName(TEXT("Skeleton.Type.Digger")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeFernit, FName(TEXT("Skeleton.Type.Fernit")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeFungiMage, FName(TEXT("Skeleton.Type.FungiMage")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeHeavy, FName(TEXT("Skeleton.Type.Heavy")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeLancer, FName(TEXT("Skeleton.Type.Lancer")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeMage, FName(TEXT("Skeleton.Type.Mage")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeShielded, FName(TEXT("Skeleton.Type.Shielded")));
UE_DEFINE_GAMEPLAY_TAG(SkeletonTypeShootingPlant, FName(TEXT("Skeleton.Type.ShootingPlant")));

UE_DEFINE_GAMEPLAY_TAG(StateAbsorbing, FName(TEXT("State.Absorbing")));
UE_DEFINE_GAMEPLAY_TAG(StateAttacking, FName(TEXT("State.Attacking.Melee")));
UE_DEFINE_GAMEPLAY_TAG(StateAttackingMelee, FName(TEXT("State.Attacking.Melee")));
UE_DEFINE_GAMEPLAY_TAG(StateBeingPushed, FName(TEXT("State.BeingPushed")));
UE_DEFINE_GAMEPLAY_TAG(StateBlockAttack, FName(TEXT("State.Block.Attack")));
UE_DEFINE_GAMEPLAY_TAG(StateBlockConsumable, FName(TEXT("State.Block.Consumable")));
UE_DEFINE_GAMEPLAY_TAG(StateChargingForward, FName(TEXT("State.ChargingForward")));
UE_DEFINE_GAMEPLAY_TAG(StateDashMark, FName(TEXT("State.DashMark")));
UE_DEFINE_GAMEPLAY_TAG(StateDashing, FName(TEXT("State.Dashing")));
UE_DEFINE_GAMEPLAY_TAG(StateDenyFungiEnergyAbsorb, FName(TEXT("State.DenyFungiEnergyAbsorb")));
UE_DEFINE_GAMEPLAY_TAG(StateFlyZone, FName(TEXT("State.FlyZone")));
UE_DEFINE_GAMEPLAY_TAG(StateFungiStun, FName(TEXT("State.Fungi.Stun")));
UE_DEFINE_GAMEPLAY_TAG(StateHealthFull, FName(TEXT("State.Health.Full")));
UE_DEFINE_GAMEPLAY_TAG(StateHealthHalf, FName(TEXT("State.Health.Half")));
UE_DEFINE_GAMEPLAY_TAG(StateHealthLow, FName(TEXT("State.Health.Low")));
UE_DEFINE_GAMEPLAY_TAG(StateLaunched, FName(TEXT("State.Launched")));
UE_DEFINE_GAMEPLAY_TAG(StateNormalStun, FName(TEXT("State.NormalStun")));
UE_DEFINE_GAMEPLAY_TAG(StateParryChain, FName(TEXT("State.ParryChain")));
UE_DEFINE_GAMEPLAY_TAG(StateRage, FName(TEXT("State.Rage")));
UE_DEFINE_GAMEPLAY_TAG(StateRescued, FName(TEXT("State.Rescued")));
UE_DEFINE_GAMEPLAY_TAG(StateSafeZone, FName(TEXT("State.SafeZone")));
UE_DEFINE_GAMEPLAY_TAG(StateShieldActive, FName(TEXT("State.Shield.Active")));
UE_DEFINE_GAMEPLAY_TAG(StateUsingConsumable, FName(TEXT("State.Using.Consumable")));
UE_DEFINE_GAMEPLAY_TAG(StateUsingConsumableHoney, FName(TEXT("State.Using.Consumable.Honey")));
UE_DEFINE_GAMEPLAY_TAG(StateVulnerable, FName(TEXT("State.Vulnerable")));
UE_DEFINE_GAMEPLAY_TAG(StateWaitBossDialogue, FName(TEXT("State.WaitBossDialogue")));
	
UE_DEFINE_GAMEPLAY_TAG(TriggerAbilityParryChain, FName(TEXT("Trigger.Ability.ParryChain")));

}
