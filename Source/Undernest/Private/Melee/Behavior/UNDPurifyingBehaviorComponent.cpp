﻿// Copyright Super Chill Games. All Rights Reserved.


#include "Melee/Behavior/UNDPurifyingBehaviorComponent.h"

#include "FunctionLibraries/PMTVFXHelpersFunctionLibrary.h"
#include "Melee/PMTMWMeleeWeapon.h"

UUNDPurifyingBehaviorComponent::UUNDPurifyingBehaviorComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UUNDPurifyingBehaviorComponent::AddAffectedActor(AActor* InActor)
{
	FUNDAbsorbAffectedActor* AffectedActor = AffectedActors.FindByPredicate([InActor](const FUNDAbsorbAffectedActor& Element)
	{
		return Element.Actor == InActor;
	});
	if (!AffectedActor)
	{
		FUNDAbsorbAffectedActor& NewAffectedActor = AffectedActors.Emplace_GetRef(InActor);
		FPMTVFXSpawnParameters SpawnParameters;
		SpawnParameters.InstantiationParameters = AbsorbVFX;
		SpawnParameters.ComponentToAttach = GetMeleeWeapon()->GetMeleeMeshComponent();
		NewAffectedActor.NiagaraComponent = UPMTVFXHelpersFunctionLibrary::SpawnNiagaraVFXSynchronous(SpawnParameters);
		HandleVFXSpawned(InActor, NewAffectedActor.NiagaraComponent);
		SetComponentTickEnabled(true);
	}
}

void UUNDPurifyingBehaviorComponent::RemoveAffectedActor(AActor* InActor)
{
	const int32 Index = AffectedActors.IndexOfByPredicate([InActor](const FUNDAbsorbAffectedActor& Element)
	{
		return Element.Actor == InActor;
	});
	if (AffectedActors.IsValidIndex(Index))
	{
		if (AffectedActors[Index].NiagaraComponent)
		{
			AffectedActors[Index].NiagaraComponent->Deactivate();
		}
		AffectedActors.RemoveAtSwap(Index);
		// We don't disable ticking because we want to display some effects that depend on ticking.
	}
}

void UUNDPurifyingBehaviorComponent::HandleStartPurifying_Implementation()
{
}

void UUNDPurifyingBehaviorComponent::HandleStopPurifying_Implementation()
{
}

void UUNDPurifyingBehaviorComponent::StopPurify()
{
	for (const FUNDAbsorbAffectedActor& AffectedActor : AffectedActors)
	{
		if (AffectedActor.NiagaraComponent)
		{
			AffectedActor.NiagaraComponent->Deactivate();
		}
	}
	AffectedActors.Reset();
	HandleStopPurifying();
}

void UUNDPurifyingBehaviorComponent::HandleVFXSpawned_Implementation(AActor* AffectedActor,
                                                                     UNiagaraComponent* NiagaraComponent)
{
}
