// Copyright Super Chill Games. All Rights Reserved.


#include "Actors/UNDDoor.h"

// PMTC Includes
#include "FunctionLibraries/PMTCCommonHelperFunctionLibrary.h"

// UND Includes
#include "Globals/UNDGameGlobals.h"
#include "Levels/UNDLevelDefinition.h"
#include "Levels/UNDLevelGenerator.h"
#include "Subsystems/UNDGameFlowWorldSubsystem.h"
#include "Types/UNDNativeTags.h"

// UE Includes
#include "Components/BoxComponent.h"
#include "Net/Core/PushModel/PushModel.h"
#include "Net/UnrealNetwork.h"

AUNDDoor::AUNDDoor()
{
	PrimaryActorTick.bCanEverTick = false;

	MainComponent = CreateDefaultSubobject<USceneComponent>(TEXT("MainComponent"));
	RootComponent = MainComponent;
	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComponent"));
	CollisionComponent->SetupAttachment(GetRootComponent());

	NetDormancy = ENetDormancy::DORM_DormantAll;
	bReplicates = true;
}

void AUNDDoor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(AUNDDoor, CurrentState, Params);
}

void AUNDDoor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

#if !UE_BUILD_SHIPPING
	if (!GetWorld() || !GetWorld()->IsGameWorld())
	{
		return;
	}
#endif // !UE_BUILD_SHIPPING

	if (HasAuthority())
	{
		CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AUNDDoor::OnComponentBeginOverlap);
		CollisionComponent->OnComponentEndOverlap.AddDynamic(this, &AUNDDoor::OnComponentEndOverlap);

		UUNDGameFlowWorldSubsystem* GameFlowWorldSS = GetWorld()->GetSubsystem<UUNDGameFlowWorldSubsystem>();
		GameFlowWorldSS->BindOrCallForEvent(UNDNativeTags::GameFlowDoorsClose,
			FUNDGameFlowEvent::CreateUObject(this, &AUNDDoor::OnDoorGameFlowEvent, EUNDDoorState::Close));
		GameFlowWorldSS->BindOrCallForEvent(UNDNativeTags::GameFlowDoorsOpen,
			FUNDGameFlowEvent::CreateUObject(this, &AUNDDoor::OnDoorGameFlowEvent, EUNDDoorState::Open));
	}
}

void AUNDDoor::BeginPlay()
{
	Super::BeginPlay();

	if (InitialState != CurrentState)
	{
		TryChangeDoorState(InitialState, CurrentState);
	}
}

void AUNDDoor::TryChangeDoorState(EUNDDoorState PreviousState, EUNDDoorState NewState)
{
	const bool bJustCreated = CreationTime == GetWorld()->GetTimeSeconds();
	const bool bInstantCloseOrOpen = bInstantCloseOrOpenOnBeginPlayOrCreation && bJustCreated;
	if (HasAuthority())
	{
		CurrentState = NewState;
		MARK_PROPERTY_DIRTY_FROM_NAME(AUNDDoor, CurrentState, this);
	}
	HandleDoorChangeState(PreviousState, NewState, bInstantCloseOrOpen);
}

bool AUNDDoor::CanChangeToState_Implementation(EUNDDoorState DoorState) const
{
	return CurrentState != DoorState && !bAlwaysLocked;
}

void AUNDDoor::HandlePlayerEnteredToDoor_Implementation(APawn* PlayerPawn)
{
	if (const UUNDLevelGenerationGISubsystem* LevelGeneratorSS = GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>())
	{
		if (AreaToTransition.IsValid())
		{
			UUNDLevelGenerator* LevelGenerator = LevelGeneratorSS->GetLevelGenerator();
			LevelGenerator->SetCurrentArea(AreaToTransition);
		}
		const TSubclassOf<UUNDLevelDefinition> NextLevelToPlay = !OverrideLevelDefinition ?
			LevelGeneratorSS->GetLevelGenerator()->TryGenerateNextAreaToPlay(FRandomStream()) : OverrideLevelDefinition;
		const bool bShouldRepeatLevel = OverrideLevelDefinition != nullptr;
		if (NextLevelToPlay)
		{
			GetWorld()->GetSubsystem<UUNDGameFlowWorldSubsystem>()->NotifyGameFlowEvent(
				UNDNativeTags::GameFlowPlayerEnteredToDoor, false, this);
			constexpr bool bEnableInput = false;
			UUNDGameGlobals::EnableDisableInputAndChangeTimeDilationForCharacter(this, bEnableInput, 1.0f, 0);
			LevelGeneratorSS->GetLevelGenerator()->TravelToLevel(this, NextLevelToPlay, DelayToLoadLevel, bShouldRepeatLevel);
		}
		else
		{
#if !UE_BUILD_SHIPPING
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Emerald, TEXT("There are no more levels to play!"));
			}
#endif // !UE_BUILD_SHIPPING
		}
	}
}

void AUNDDoor::HandlePlayerEnteredToDoorWhileCorrupted_Implementation(APawn* PlayerPawn)
{

}

void AUNDDoor::HandleDoorChangeState_Implementation(EUNDDoorState PreviousState, EUNDDoorState NewState, bool bInstantChange)
{

}

void AUNDDoor::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (APawn* Pawn = Cast<APawn>(OtherActor))
	{
		const TArray<APawn*> PlayerPawns = UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this);
		if (PlayerPawns.Contains(Pawn))
		{
			if (IsCorrupted())
			{
				HandlePlayerEnteredToDoorWhileCorrupted(Pawn);
			}
			else if (GetDoorState() == EUNDDoorState::Open)
			{
				HandlePlayerEnteredToDoor(Pawn);
			}
		}
	}
}

void AUNDDoor::OnComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AUNDDoor::OnRep_CurrentState(EUNDDoorState PreviousState)
{
	TryChangeDoorState(PreviousState, CurrentState);
}

void AUNDDoor::OnDoorGameFlowEvent(const FGameplayTag& Event, EUNDDoorState NewState)
{
	if (!CanChangeToState(NewState))
	{
		return;
	}
	TryChangeDoorState(CurrentState, NewState);
}
