// Copyright Super Chill Games. All Rights Reserved.


#include "Actors/UNDHUBDoor.h"

// UND Includes
#include "Levels/UNDLevelGenerator.h"

AUNDHUBDoor::AUNDHUBDoor()
{
	InitialState = EUNDDoorState::Open;
	CurrentState = EUNDDoorState::Open;
}
