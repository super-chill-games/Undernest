// Copyright Super Chill Games. All Rights Reserved.


#include "Actors/UNDLevelManagerActor.h"

// UE Includes
#include "Components/BillboardComponent.h"

AUNDLevelManagerActor::AUNDLevelManagerActor()
{
	PrimaryActorTick.bCanEverTick = false;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
#if WITH_EDITORONLY_DATA
	SpriteComponent = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("SpriteComponent"));
	if (!IsRunningCommandlet() && (SpriteComponent != nullptr))
	{
		SpriteComponent->SetupAttachment(GetRootComponent());
	}
#endif // WITH_EDITORONLY_DATA
}
