// Copyright Super Chill Games. All Rights Reserved.


#include "Debug/UNDCheatManager.h"

// PMTC Includes
#include "Interfaces/PMTCAbilitySystemInterface.h"
#include "Types/PMTCNativeTags.h"

// PMTGU Includes
#include "Types/PMTGUNativeTags.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AIController.h"
#include "EngineUtils.h"
#include "GameplayEffect.h"

void UUNDCheatManager::KillEveryEnemy()
{
	if (!ExecuteCheatOnServer(FString(TEXT("KillEveryEnemy"))) && InstantKillGE)
	{
		if (UAbilitySystemComponent* OwnerASC = GetOwnerASC())
		{
			for (TActorIterator<AAIController> It(OwnerASC->GetWorld()); It; ++It)
			{
				if (AAIController* AIController = *It)
				{
					if (APawn* Pawn = AIController->GetPawn())
					{
						if (UAbilitySystemComponent* TargetASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Pawn))
						{
							OwnerASC->ApplyGameplayEffectToTarget(InstantKillGE.GetDefaultObject(), TargetASC, 1.0f);
						}
					}
				}
			}
		}
	}
}

void UUNDCheatManager::MakeDamage(float Damage)
{
	if (SelfDamageGE && !ExecuteCheatOnServer(FString(TEXT("MakeDamage"))))
	{
		if (UAbilitySystemComponent* OwnerASC = GetOwnerASC())
		{
			FHitResult FakeHit;
			FakeHit.Component = Cast<UPrimitiveComponent>(OwnerASC->GetAvatarActor()->GetRootComponent());
			FakeHit.HitObjectHandle = OwnerASC->GetAvatarActor();
			FakeHit.ImpactPoint = OwnerASC->GetAvatarActor()->GetActorLocation();
			FakeHit.Location = FakeHit.ImpactPoint;
			FakeHit.bBlockingHit = true;
			
			FGameplayEffectSpecHandle Spec = OwnerASC->MakeOutgoingSpec(SelfDamageGE, 1.0f, OwnerASC->MakeEffectContext());
			Spec.Data->GetContext().AddHitResult(FakeHit);
			Spec.Data->SetSetByCallerMagnitude(PMTCNativeTags::SetByCallerFlatDamage, Damage);
			Spec.Data->AddDynamicAssetTag(PMTGUNativeTags::GameplayEffectHitTeammates);
			OwnerASC->ApplyGameplayEffectSpecToSelf(*Spec.Data);
		}
	}
}

void UUNDCheatManager::ToggleGodMode()
{
	if (GodModeGE && !ExecuteCheatOnServer(FString(TEXT("ToggleGodMode"))))
	{
		if (UAbilitySystemComponent* OwnerASC = GetOwnerASC())
		{
			const int32 Count = OwnerASC->GetGameplayEffectCount(GodModeGE, nullptr, true);
			if (Count > 0)
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, TEXT("GodMode OFF"));
				}
				OwnerASC->RemoveActiveGameplayEffectBySourceEffect(GodModeGE, nullptr);
			}
			else
			{
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, TEXT("GodMode ON"));
				}
				OwnerASC->ApplyGameplayEffectToSelf(GodModeGE.GetDefaultObject(), 1.0f, FGameplayEffectContextHandle());
			}
		}
	}
}
