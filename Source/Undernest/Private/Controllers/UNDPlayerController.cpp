// Copyright Super Chill Games. All Rights Reserved.


#include "Controllers/UNDPlayerController.h"

void AUNDPlayerController::PostSeamlessTravel()
{
	Super::PostSeamlessTravel();
	EnableInput(this);
	if (APawn* LocalPawn = GetPawn())
	{
		LocalPawn->EnableInput(this);
	}
}
