// Copyright Super Chill Games. All Rights Reserved.


#include "Pickup/UNDBasePickup.h"

// UE Includes
#include "Components/SphereComponent.h"

AUNDBasePickup::AUNDBasePickup(const FObjectInitializer& InObjectInitializer) :
	Super(InObjectInitializer
		.SetDefaultSubobjectClass(APMTPIBasePickup::CollisionComponentName, USphereComponent::StaticClass()))
{

}
