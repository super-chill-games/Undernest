// Copyright Super Chill Games. All Rights Reserved.


#include "Difficulty/UNDDifficultyConfig.h"

// PMTC Includes
#include "Types/PMTCNativeTags.h"

// UND Includes
#include "Levels/UNDLevelGenerator.h"
#include "Types/UNDNativeTags.h"

UUNDDifficultyConfig::UUNDDifficultyConfig()
{
	Difficulty = PMTCNativeTags::DifficultyCustom;
}

float UUNDDifficultyConfig::GetDifficultyAsNumber_Implementation(const UObject* WorldContext) const
{
	if (WorldContext)
	{
		if (UWorld* World = WorldContext->GetWorld())
		{
			FGameplayTagContainer AreaTags;
			AreaTags.AddTagFast(UNDNativeTags::LevelDescriptorCombat);
			AreaTags.AddTagFast(UNDNativeTags::LevelDescriptorFinalArea);
			AreaTags.AddTagFast(UNDNativeTags::LevelDescriptorBoss);

			UUNDLevelGenerator* LevelGenerator = World->GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>()->GetLevelGenerator();
			return static_cast<float>(LevelGenerator->GetPlayedLevelsWithAnyTags(AreaTags).Num());
		}
	}
	return Super::GetDifficultyAsNumber_Implementation(WorldContext);
}
