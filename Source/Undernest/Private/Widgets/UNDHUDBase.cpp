// Copyright Super Chill Games. All Rights Reserved.


#include "Widgets/UNDHUDBase.h"

// PMTUI Includes
#include "UI/PMTUIMessageIntermediary.h"

// UND Includes
#include "Globals/UNDGameGlobals.h"
#include "Types/UNDNativeTags.h"

void AUNDHUDBase::BeginPlay()
{
	Super::BeginPlay();

	if (UUNDGameGlobals::AreWeInHUBWorld(this))
	{
		UPMTUIMessageIntermediary::SendMessage<FPMTCCustomData>(
			GetOwningPlayerController(), UNDNativeTags::EventUIHUBWorld, FPMTCCustomData());
	}
	else if (UUNDGameGlobals::AreWeInLaboratoryWorld(this))
	{
		UPMTUIMessageIntermediary::SendMessage<FPMTCCustomData>(
			GetOwningPlayerController(), UNDNativeTags::EventUILaboratory, FPMTCCustomData());
	}
}
