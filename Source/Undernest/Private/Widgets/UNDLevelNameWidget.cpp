// Copyright Super Chill Games. All Rights Reserved.


#include "Widgets/UNDLevelNameWidget.h"

// PMTW Includes
#include "Widgets/Extended/PMTWBaseImage.h"
#include "Widgets/Extended/PMTWBaseRichTextBlock.h"

// UND Includes
#include "Levels/UNDLevelDefinition.h"
#include "Levels/UNDLevelGenerator.h"

void UUNDLevelNameWidget::HandleLevelDisplayed_Implementation(const UUNDLevelDefinition* LevelDefinition)
{
	if (LevelDefinition)
	{
		if (TEXT_LevelName)
		{
			TEXT_LevelName->SetText(LevelDefinition->GetLevelName());
		}

		if (IMAGE_Level)
		{
			IMAGE_Level->SetBrushFromSoftSlateBrush(LevelDefinition->GetLevelImageName());
		}
	}
}

void UUNDLevelNameWidget::OnPlayerContextInitialized()
{
	Super::OnPlayerContextInitialized();

	HandleLevelDisplayed(UUNDLevelGenerator::GetCurrentLevelDefinition(GetOwningPlayer()).GetDefaultObject());
}
