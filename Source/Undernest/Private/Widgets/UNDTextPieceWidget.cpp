// Copyright Super Chill Games. All Rights Reserved.


#include "Widgets/UNDTextPieceWidget.h"

// PMTUI Includes
#include "UI/PMTUIMessageIntermediary.h"

// PMTW Includes
#include "Widgets/Extended/PMTWBaseRichTextBlock.h"

// UND Includes
#include "Types/UNDNativeTags.h"

void UUNDTextPieceWidget::HandleNewTextPiece_Implementation(const FText& InMessage)
{

}

void UUNDTextPieceWidget::HandleCurrentTextPieceRemoved_Implementation()
{

}

FText UUNDTextPieceWidget::GetCurrentTextPiece() const
{
	return HasAnyRemainingText() ? TextPieces[0].Message : FText();
}

void UUNDTextPieceWidget::OnPlayerContextInitialized()
{
	Super::OnPlayerContextInitialized();

	PMTUI_USERWIDGET_REGISTER_FOR_EVENT(UNDNativeTags::EventUITextPieceAdded, &UUNDTextPieceWidget::OnTextPieceAddedMessageReceived);
	PMTUI_USERWIDGET_REGISTER_FOR_EVENT(UNDNativeTags::EventUITextPieceRemoved, &UUNDTextPieceWidget::OnTextPieceRemovedMessageReceived);
}

void UUNDTextPieceWidget::OnTextPieceAddedMessageReceived(const FPMTCCustomData& MessageData)
{
	TextPieces.Add(UPMTUIMessageIntermediary::GetMessageChecked<FUNDTextPieceMessageData>(MessageData));
	if (TextPieces.Num() == 1)
	{
		HandleNewTextPiece(TextPieces[0].Message);
	}
}

void UUNDTextPieceWidget::OnTextPieceRemovedMessageReceived(const FPMTCCustomData& MessageData)
{
	const FUNDTextPieceMessageData& Message = UPMTUIMessageIntermediary::GetMessageChecked<FUNDTextPieceMessageData>(MessageData);
	if (TextPieces.Num() > 0)
	{
		if (TextPieces[0].Owner == Message.Owner)
		{
			TextPieces.RemoveAt(0);
			HandleCurrentTextPieceRemoved();
		}
	}
}
