﻿// Copyright Super Chill Games. All Rights Reserved.


#include "ActorComponents/UNDHitForceComponent.h"

// PMTC Includes
#include "Interfaces/PMTCAbilitySystemInterface.h"
#include "Types/PMTCNativeTags.h"

// UND Includes
#include "Types/UNDNativeTags.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"

UUNDHitForceComponent::UUNDHitForceComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	ObjectFilter.BlacklistActorClasses.Add(APawn::StaticClass());
	ObjectFilter.ExclusionTags.AddTag(PMTCNativeTags::StateDead);
	bWantsInitializeComponent = true;
}

void UUNDHitForceComponent::InitializeComponent()
{
	Super::InitializeComponent();

#if !UE_BUILD_SHIPPING
	if (!GetWorld() || !GetWorld()->IsGameWorld())
	{
		return;
	}
#endif // !UE_BUILD_SHIPPING

	if (UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(GetOwner()))
	{
		ASC->RegisterGameplayTagEvent(UNDNativeTags::StateBeingPushed, EGameplayTagEventType::NewOrRemoved).AddUObject(
			this, &UUNDHitForceComponent::OnForceApplyTagChanged);
		ASC->RegisterGameplayTagEvent(UNDNativeTags::StateFungiStun, EGameplayTagEventType::NewOrRemoved).AddUObject(
			this, &UUNDHitForceComponent::OnFungiStunChanged);
	}
}

void UUNDHitForceComponent::OnOwnerHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse,
                                       const FHitResult& Hit)
{
	const double CurrentVelocitySquared = GetOwner()->GetVelocity().SizeSquared();
	if (CurrentVelocitySquared >= FMath::Square(MinVelocityToTriggerDamage))
	{
		if (ObjectFilter.PassesFilter(*OtherActor))
		{
			UCharacterMovementComponent* CharacterMovementComponent = SelfActor->FindComponentByClass<UCharacterMovementComponent>();
			if (CharacterMovementComponent)
			{
				// If the character is playing a root motion animation or does not have any root motion applied,
				// then do not allow this to be applied.
				// A force is given by the dash, therefore we only want to apply this on those cases.
				if (CharacterMovementComponent->HasAnimRootMotion() || CharacterMovementComponent->CurrentRootMotion.RootMotionSources.Num() == 0)
				{
					return;
				}

				// We hit a surface that is perpendicular to us, therefore this doesn't make sense, return.
				const float Dot = Hit.ImpactNormal.Dot(CharacterMovementComponent->GetLastUpdateVelocity().GetSafeNormal());
				if (FMath::Abs(Dot) <= 0.4f)
				{
					return;
				}
				if (bStopAllMovementOnCollision)
				{
					CharacterMovementComponent->StopMovementImmediately();
					CharacterMovementComponent->CurrentRootMotion.Clear();
				}
			}

			if (UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(SelfActor))
			{
				FGameplayEffectContextHandle ContextHandle = ASC->MakeEffectContext();
				ContextHandle.AddHitResult(Hit);
				for (const FPMTCSetByCallerGameplayEffectConfig& Effect : GameplayEffectsToApply)
				{
					FGameplayEffectSpecHandle SpecHandle = ASC->MakeOutgoingSpec(
						Effect.GetGameplayEffectClass(), 1.0f, ContextHandle);
					Effect.SetSetByCallerValues(*SpecHandle.Data);
					ASC->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);
				}

				ASC->ExecuteGameplayCue(UNDNativeTags::GameplayCueHitAgainstWall, ContextHandle);
			}
		}
	}
}

void UUNDHitForceComponent::OnForceApplyTagChanged(const FGameplayTag Tag, int32 Count)
{
	if (Count > 0)
	{
		LocationOnForceApplied = GetOwner()->GetActorLocation();
		GetOwner()->OnActorHit.AddDynamic(this, &UUNDHitForceComponent::OnOwnerHit);
	}
	else
	{
		if (GetOwner()->OnActorHit.IsAlreadyBound(this, &UUNDHitForceComponent::OnOwnerHit))
		{
			GetOwner()->OnActorHit.RemoveDynamic(this, &UUNDHitForceComponent::OnOwnerHit);
		}
	}
}

void UUNDHitForceComponent::OnFungiStunChanged(const FGameplayTag Tag, int32 Count)
{
	if (Count == 0)
	{
		if (GetOwner()->OnActorHit.IsAlreadyBound(this, &UUNDHitForceComponent::OnOwnerHit))
		{
			GetOwner()->OnActorHit.RemoveDynamic(this, &UUNDHitForceComponent::OnOwnerHit);
		}
	}
}
