// Copyright Super Chill Games. All Rights Reserved.


#include "ActorComponents/UNDBossManagerComponent.h"

// PMTC Includes
#include "FunctionLibraries/PMTCCommonHelperFunctionLibrary.h"
#include "Interfaces/PMTCAbilitySystemInterface.h"

// PMTD Includes
#include "ActorComponents/PMTDDialogueUserComponent.h"
#include "Interfaces/PMTDDialogueUserInterface.h"

// PMTG Includes
#include "ActorComponents/PMTGBodyPartManagerComponent.h"

// UND Includes
#include "Subsystems/UNDGameFlowWorldSubsystem.h"
#include "Types/UNDNativeTags.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "AIController.h"
#include "BrainComponent.h"
#include "Abilities/PMTCAbilitySystemComponent.h"
#include "ActorComponents/UNDBodyPartManagerComponent.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"
#include "Types/PMTCNativeTags.h"

UUNDBossManagerComponent::UUNDBossManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;

	FocusBossCameraParams.bDisableInputWhileReturning = false;
	FocusBossCameraParams.bDisableInputWhileTransitioning = true;
	FocusBossCameraParams.bDisableInputWhileOnViewTarget = true;
	FocusBossCameraParams.TransitionDuration = EPMTCCameraTransitionDuration::Event;

	FocusBossCameraParams.EventOnTransition.BindDynamic(this, &UUNDBossManagerComponent::OnCameraTransitionedIntoTarget);
}

FVector UUNDBossManagerComponent::GetBossCenterPoint_Implementation() const
{
	const FName CenterPointTag(TEXT("BossCenterPoint"));
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClassWithTag(this, ATargetPoint::StaticClass(), CenterPointTag, Actors);
	return Actors.Num() > 0 ? Actors[0]->GetActorLocation() : FVector::ZeroVector;
}

APawn* UUNDBossManagerComponent::GetOwningBossPawn() const
{
	return Cast<APawn>(GetOwner());
}

void UUNDBossManagerComponent::InitializeComponent()
{
	Super::InitializeComponent();
#if !UE_BUILD_SHIPPING
	if (!GetWorld() || !GetWorld()->IsGameWorld())
	{
		return;
	}
#endif // !UE_BUILD_SHIPPING

	if (EnterIntoBossArea)
	{
		EnterIntoBossArea->OnActorBeginOverlap.AddDynamic(this, &UUNDBossManagerComponent::OnActorBeginOverlapWithBossArea);
		TArray<AActor*> OverlappingActors;
		EnterIntoBossArea->GetOverlappingActors(OverlappingActors, APawn::StaticClass());
		for (AActor* OverlappingPawn : OverlappingActors)
		{
			OnActorBeginOverlapWithBossArea(EnterIntoBossArea, OverlappingPawn);
			// In case previous function detected a player, then we don't need to iterate anymore.
			if (!EnterIntoBossArea->OnActorBeginOverlap.IsAlreadyBound(this, &UUNDBossManagerComponent::OnActorBeginOverlapWithBossArea))
			{
				break;
			}
		}
	}

	if (UPMTGBodyPartManagerComponent* BodyPartManager = GetOwner()->FindComponentByClass<UPMTGBodyPartManagerComponent>())
	{
		BodyPartManager->OnPhaseFinished.AddDynamic(this, &UUNDBossManagerComponent::OnBossPhaseFinished);
		BodyPartManager->OnAllPhasesCompleted.AddDynamic(this, &UUNDBossManagerComponent::OnAllPhasesFinished);
		//constexpr float BodyPartLevel = 1.0f;
		//BodyPartManager->InitializePhase(BodyPartLevel);
	}
}

void UUNDBossManagerComponent::OnWelcomeDialogueFinished(UPMTDDialogueUserComponent* DialogueUserComponent)
{
	if (DialogueUserComponent)
	{
		DialogueUserComponent->OnDialogueFinished.RemoveDynamic(this, &UUNDBossManagerComponent::OnWelcomeDialogueFinished);
	}

	const TArray<APawn*> PlayerPawns = UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this);
	const APlayerController* PC = UPMTCCommonHelperFunctionLibrary::TryGetPlayerControllerFromActor(PlayerPawns[0]);
	if (APMTCPlayerCameraManager* CameraManager = Cast<APMTCPlayerCameraManager>(PC->PlayerCameraManager))
	{
		CameraManager->RequestReturnToPreviousViewTarget();
	}

	HandleBossBattleBegin();
	UPMTGBodyPartManagerComponent* BodyPartManager = GetOwner()->FindComponentByClass<UPMTGBodyPartManagerComponent>();
	BodyPartManager->TryStartNextPhase();
}

void UUNDBossManagerComponent::HandleAllPhasesCompleted_Implementation(UPMTGBodyPartManagerComponent* BodyPartManager)
{
}

void UUNDBossManagerComponent::OnPhaseDialogueFinished_Implementation(UPMTDDialogueUserComponent* DialogueUserComponent)
{
	if (DialogueUserComponent)
	{
		DialogueUserComponent->OnDialogueFinished.RemoveDynamic(this, &UUNDBossManagerComponent::OnPhaseDialogueFinished);
	}

	const TArray<APawn*> PlayerPawns = UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this);
	const APlayerController* PC = UPMTCCommonHelperFunctionLibrary::TryGetPlayerControllerFromActor(PlayerPawns[0]);
	if (APMTCPlayerCameraManager* CameraManager = Cast<APMTCPlayerCameraManager>(PC->PlayerCameraManager))
	{
		CameraManager->RequestReturnToPreviousViewTarget();
	}

	AddOrRemoveBossPhaseFinishTags(false);

	UPMTGBodyPartManagerComponent* BodyPartManager = GetOwner()->FindComponentByClass<UPMTGBodyPartManagerComponent>();
	const bool bStartedPhase = BodyPartManager->TryStartNextPhase();
	if (!bStartedPhase && BodyPartManager->HasFinishedAllPhases())
	{
		if (const APawn* BossPawn = GetOwningBossPawn())
		{
			if (const AAIController* Controller = Cast<AAIController>(BossPawn->GetController()))
			{
				// When all phases are completed, it means that we now we have to fight
				// the boss until its fungi health is depleted, therefore we need to start the logic here.
				// See UPMTGBodyPartManagerComponent::OnBodyPartPhaseStateChanged.
				Controller->GetBrainComponent()->StartLogic();
			}
		}
		HandleAllPhasesCompleted(BodyPartManager);
	}
}

void UUNDBossManagerComponent::OnAllPhasesFinished_Implementation(UPMTGBodyPartManagerComponent* BodyPartManager)
{
}

void UUNDBossManagerComponent::AddOrRemoveBossPhaseFinishTags(bool bAdd)
{
	// Make all players invulnerable until next phase starts.
	for (const APawn* Pawn : UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this))
	{
		UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Pawn);
		UPMTCAbilitySystemComponent::AddOrRemoveLooseGameplayTag(ASC, PMTCNativeTags::ImmunityAll, bAdd);
		UPMTCAbilitySystemComponent::AddOrRemoveLooseGameplayTag(ASC, UNDNativeTags::StateWaitBossDialogue, bAdd);
	}
	
	if (const APawn* BossPawn = GetOwningBossPawn())
	{
		UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(BossPawn);
		UPMTCAbilitySystemComponent::AddOrRemoveLooseGameplayTag(ASC, UNDNativeTags::StateWaitBossDialogue, bAdd);
	}
}

void UUNDBossManagerComponent::OnBossPhaseFinished_Implementation(UPMTGBodyPartManagerComponent* BodyPartManager,
	UPMTGBodyPartPhase* Phase)
{
	bool bSwitchToNextPhase = true;
	if (const UUNDBossBodyPartPhase* BossPhase = Cast<UUNDBossBodyPartPhase>(Phase))
	{
		if (!BossPhase->GetFinishPhaseDialogue().IsNull())
		{
			FocusBossCameraParams.EventOnTransition.BindDynamic(this, &UUNDBossManagerComponent::OnCameraTransitionedIntoTargetFromPhase);
			const TArray<APawn*> PlayerPawns = UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this);
			// This can be zero when we are tearing down the game.
			if (PlayerPawns.Num() > 0)
			{
				if (const APlayerController* PC = UPMTCCommonHelperFunctionLibrary::TryGetPlayerControllerFromActor(PlayerPawns[0]))
				{
					if (APMTCPlayerCameraManager* CameraManager = Cast<APMTCPlayerCameraManager>(PC->PlayerCameraManager))
					{
						bSwitchToNextPhase = false;
						FocusBossCameraParams.NewViewTarget = GetOwningBossPawn();
						CameraManager->SetViewTargetWithTransition(FocusBossCameraParams);
					}
				}	
			}
		}
	}

	AddOrRemoveBossPhaseFinishTags(true);
	
	if (bSwitchToNextPhase)
	{
		OnPhaseDialogueFinished(nullptr);
	}
}

void UUNDBossManagerComponent::HandlePlayerEnteredIntoBossArea_Implementation(APawn* PlayerPawn)
{
	UUNDGameFlowWorldSubsystem::GetGameFlowWorldSSFromContext(this)->NotifyGameFlowEvent(UNDNativeTags::GameFlowPlayerEnteredToBossArea);
	// This will never be null, since we already verified that the pawn is player controlled before.
	const APlayerController* PC = UPMTCCommonHelperFunctionLibrary::TryGetPlayerControllerFromActor(PlayerPawn);
	if (APMTCPlayerCameraManager* CameraManager = Cast<APMTCPlayerCameraManager>(PC->PlayerCameraManager))
	{
		FocusBossCameraParams.NewViewTarget = GetOwningBossPawn();
		CameraManager->SetViewTargetWithTransition(FocusBossCameraParams);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("'%s': No camera manager found for player '%s'. Will start dialogue."), ANSI_TO_TCHAR(__FUNCTION__), *PlayerPawn->GetName());
		OnCameraTransitionedIntoTarget(nullptr);
	}
}

void UUNDBossManagerComponent::HandleBossBattleBegin_Implementation()
{
	APawn* BossOwner = GetOwningBossPawn();
	UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(BossOwner);
	// Required for any boss mission.
	ASC->AddLooseGameplayTag(UNDNativeTags::MissionTargetBossObjective);
	if (bSpawnControllerWhenBattleBegins)
	{
		BossOwner->SpawnDefaultController();
	}
	UUNDGameFlowWorldSubsystem::GetGameFlowWorldSSFromContext(this)->NotifyGameFlowEvent(UNDNativeTags::GameFlowBossBattleBegin);
}

void UUNDBossManagerComponent::OnCameraTransitionedIntoTarget(APlayerCameraManager* CameraManager)
{
	if (!WelcomeDialogue.IsNull())
	{
		// At least one must exist, since someone entered into the boss area.
		const TArray<APawn*> PlayerPawns = UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this);
		check(PlayerPawns.Num() > 0);
		// This should always be valid.
		UPMTDDialogueUserComponent* DialogueUserComponent = IPMTDDialogueUserInterface::Execute_GetDialogueUserComponent(PlayerPawns[0]);
		check(DialogueUserComponent);

		DialogueUserComponent->OnDialogueFinished.AddDynamic(this, &UUNDBossManagerComponent::OnWelcomeDialogueFinished);
		DialogueUserComponent->TryStartDialogue(WelcomeDialogue, TArray<AActor*>({GetOwningBossPawn()}));
	}
	else
	{
		OnWelcomeDialogueFinished(nullptr);
	}
}

void UUNDBossManagerComponent::OnCameraTransitionedIntoTargetFromPhase(APlayerCameraManager* CameraManager)
{
	UPMTGBodyPartManagerComponent* BodyPartManagerComponent = GetOwner()->FindComponentByClass<UPMTGBodyPartManagerComponent>();
	if (UUNDBossBodyPartPhase* BossBodyPart = Cast<UUNDBossBodyPartPhase>(BodyPartManagerComponent->GetLastCompletedPhase()))
	{
		if (!BossBodyPart->GetFinishPhaseDialogue().IsNull())
		{
			const TArray<APawn*> PlayerPawns = UPMTCCommonHelperFunctionLibrary::GetPlayersPawn(this);
			check(PlayerPawns.Num() > 0);
			// This should always be valid.
			UPMTDDialogueUserComponent* DialogueUserComponent = IPMTDDialogueUserInterface::Execute_GetDialogueUserComponent(PlayerPawns[0]);
			check(DialogueUserComponent);

			DialogueUserComponent->OnDialogueFinished.AddDynamic(this, &UUNDBossManagerComponent::OnPhaseDialogueFinished);
			const bool bDialogueStarted = DialogueUserComponent->TryStartDialogue(BossBodyPart->GetFinishPhaseDialogue(),
				TArray<AActor*>({GetOwningBossPawn()}));
			if (!bDialogueStarted)
			{
				OnPhaseDialogueFinished(DialogueUserComponent);
			}
		}
		else
		{
			OnPhaseDialogueFinished(nullptr);
		}
	}
	else
	{
		OnPhaseDialogueFinished(nullptr);
	}
}

void UUNDBossManagerComponent::OnActorBeginOverlapWithBossArea(AActor* OverlappedActor, AActor* OtherActor)
{
	if (APawn* Pawn = Cast<APawn>(OtherActor))
	{
		if (Pawn->IsPlayerControlled())
		{
			EnterIntoBossArea->OnActorEndOverlap.RemoveDynamic(this, &UUNDBossManagerComponent::OnActorBeginOverlapWithBossArea);
			OverlappedActor->SetActorEnableCollision(false);
			HandlePlayerEnteredIntoBossArea(Pawn);
		}
	}
}
