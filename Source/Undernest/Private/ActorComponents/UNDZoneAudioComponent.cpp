// Copyright Super Chill Games. All Rights Reserved.


#include "ActorComponents/UNDZoneAudioComponent.h"

// PMTAU Includes
#include "Globals/PMTAUAudioGlobals.h"
#include "Sound/PMTAUUESoundGlobals.h"

// UND Includes
#include "Subsystems/UNDGameFlowWorldSubsystem.h"

// UE Includes
#include "Components/AudioComponent.h"
#include "Sound/SoundBase.h"

UUNDZoneAudioComponent::UUNDZoneAudioComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

UAudioComponent* UUNDZoneAudioComponent::PlayCombatSound(float FadeInDuration, float FadeOutDuration)
{
	return TryPlaySound(CombatAudioComponent, CombatSound, FadeInDuration, FadeOutDuration);
}

UAudioComponent* UUNDZoneAudioComponent::PlayCalmSound(float FadeInDuration, float FadeOutDuration)
{
	return TryPlaySound(CalmAudioComponent, CalmSound, FadeInDuration, FadeOutDuration);
}

UAudioComponent* UUNDZoneAudioComponent::PlayRestSound(float FadeInDuration, float FadeOutDuration)
{
	return TryPlaySound(RestAudioComponent, RestSound, FadeInDuration, FadeOutDuration);
}

void UUNDZoneAudioComponent::StopEverySound(float FadeOutDuration)
{
	if (UAudioComponent* CurrentAudio = GetCurrentAudioBeingPlayed())
	{
		CurrentAudio->FadeOut(FadeOutDuration, 0.0f);
	}
}

UAudioComponent* UUNDZoneAudioComponent::GetCurrentAudioBeingPlayed() const
{
	if (IsPlayingAudioComponent(CombatAudioComponent))
	{
		return CombatAudioComponent;
	}
	else if (IsPlayingAudioComponent(CalmAudioComponent))
	{
		return CalmAudioComponent;
	}
	return IsPlayingAudioComponent(RestAudioComponent) ? RestAudioComponent : nullptr;
}

void UUNDZoneAudioComponent::InitializeComponent()
{
	Super::InitializeComponent();

#if !UE_BUILD_SHIPPING
	if (!GetWorld() || !GetWorld()->IsGameWorld())
	{
		return;
	}
#endif // UE_BUILD_SHIPPING

	UUNDGameFlowWorldSubsystem* GameFlowSubsystem = GetWorld()->GetSubsystem<UUNDGameFlowWorldSubsystem>();
	GameFlowSubsystem->BindOrCallForEvent(CombatSoundOnGameflowEvent,
		FUNDGameFlowEvent::CreateUObject(this, &UUNDZoneAudioComponent::OnGameFlowEvent), true);
	GameFlowSubsystem->BindOrCallForEvent(RestSoundOnGameflowEvent,
		FUNDGameFlowEvent::CreateUObject(this, &UUNDZoneAudioComponent::OnGameFlowEvent), true);
	GameFlowSubsystem->BindOrCallForEvent(CalmSoundOnGameflowEvent,
		FUNDGameFlowEvent::CreateUObject(this, &UUNDZoneAudioComponent::OnGameFlowEvent), true);
	GameFlowSubsystem->BindOrCallForEvent(StopAudioGameFlowEvent,
		FUNDGameFlowEvent::CreateUObject(this, &UUNDZoneAudioComponent::OnGameFlowEvent), true);
}

UAudioComponent* UUNDZoneAudioComponent::TryPlaySound(TObjectPtr<UAudioComponent>& Component,
	const FPMTAUSoundCustomData& Audio, float FadeInDuration, float FadeOutDuration)
{
	UAudioComponent* CurrentAudioComponent = GetCurrentAudioBeingPlayed();
	if (CurrentAudioComponent)
	{
		if (CurrentAudioComponent == Component)
		{
			return Component;
		}
		CurrentAudioComponent->FadeOut(FadeOutDuration, 0.0f);
	}

	if (!Component)
	{
		Component = SpawnAudioComponent(Audio);
	}

	if (Component)
	{
		Component->FadeIn(FadeInDuration);
	}
	return Component;
}

bool UUNDZoneAudioComponent::IsPlayingAudioComponent(const TObjectPtr<UAudioComponent>& AudioComponent) const
{
	if (AudioComponent)
	{
		const EAudioComponentPlayState PlayState = AudioComponent->GetPlayState();
		return PlayState == EAudioComponentPlayState::FadingIn || PlayState == EAudioComponentPlayState::Playing;
	}
	return false;
}

UAudioComponent* UUNDZoneAudioComponent::SpawnAudioComponent(const FPMTAUSoundCustomData& SoundData) const
{
	UPMTAUSoundGlobalsBase* SoundGlobals = UPMTAUAudioGlobals::GetSoundGlobals();
	FPMTCCustomDataHandle DataHandle = CreateCustomData<FPMTAUSoundCustomData>(SoundData);
	return Cast<UAudioComponent>(SoundGlobals->PlayAU2DSound(GetOwner(), DataHandle, FHitResult(), GetOwner()));
}

void UUNDZoneAudioComponent::OnGameFlowEvent(const FGameplayTag& Event)
{
	if (CombatSoundOnGameflowEvent.IsValid() && Event.MatchesTag(CombatSoundOnGameflowEvent))
	{
		PlayCombatSound(CombatSound.FadeInDuration, CombatSound.FadeOutDuration);
	}
	else if (RestSoundOnGameflowEvent.IsValid() && Event.MatchesTag(RestSoundOnGameflowEvent))
	{
		PlayRestSound(RestSound.FadeInDuration, RestSound.FadeOutDuration);
	}
	else if (CalmSoundOnGameflowEvent.IsValid() && Event.MatchesTag(CalmSoundOnGameflowEvent))
	{
		PlayCalmSound(CalmSound.FadeInDuration, CalmSound.FadeOutDuration);
	}
	else if (StopAudioGameFlowEvent.IsValid() && Event.MatchesTag(StopAudioGameFlowEvent))
	{
		StopEverySound(2.0f);
	}
}
