// Copyright Super Chill Games. All Rights Reserved.


#include "ActorComponents/UNDTextPieceComponent.h"

// PMTUI Includes
#include "UI/PMTUIMessageIntermediary.h"

// UND Includes
#include "Types/UNDNativeTags.h"
#include "UI/Messages/Types/UNDTextPieceMessageData.h"

// UE Includes
#include "GameFramework/Pawn.h"

UUNDTextPieceComponent::UUNDTextPieceComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void UUNDTextPieceComponent::InitializeComponent()
{
	Super::InitializeComponent();

#if !UE_BUILD_SHIPPING
	if (!GetWorld() || !GetWorld()->IsGameWorld())
	{
		return;
	}
#endif // !UE_BUILD_SHIPPING

	GetOwner()->OnActorBeginOverlap.AddDynamic(this, &UUNDTextPieceComponent::OnActorBeginOverlap);
	GetOwner()->OnActorEndOverlap.AddDynamic(this, &UUNDTextPieceComponent::OnActorEndOverlap);
}

void UUNDTextPieceComponent::OnActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (APawn* Pawn = Cast<APawn>(OtherActor))
	{
		if (Pawn->IsLocallyControlled())
		{
			UPMTUIMessageIntermediary::SendMessage<FUNDTextPieceMessageData>(
				Cast<APlayerController>(Pawn->GetController()), UNDNativeTags::EventUITextPieceAdded,
				FUNDTextPieceMessageData(this, TextPiece));
		}
	}
}

void UUNDTextPieceComponent::OnActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (APawn* Pawn = Cast<APawn>(OtherActor))
	{
		if (Pawn->IsLocallyControlled())
		{
			UPMTUIMessageIntermediary::SendMessage<FUNDTextPieceMessageData>(
				Cast<APlayerController>(Pawn->GetController()), UNDNativeTags::EventUITextPieceRemoved,
				FUNDTextPieceMessageData(this, TextPiece));
		}
	}
}
