// Copyright Super Chill Games. All Rights Reserved.


#include "ActorComponents/UNDBodyPartManagerComponent.h"

#include "ActorComponents/PMTCSkeletalMeshComponent.h"
#include "ActorComponents/PMTCStaticMeshComponent.h"
#include "FunctionLibraries/PMTCCommonHelperFunctionLibrary.h"

UUNDBodyPartConfiguration::UUNDBodyPartConfiguration()
{
	RagdollProfile.Name = TEXT("Ragdoll");
}

TArray<FUNDMeshForBodyPartComponent> UUNDBodyPartConfiguration::GetMeshesByTag(FGameplayTag StateTag) const
{
	const FUNDMeshByHealthState* State = MeshByState.FindByPredicate([&StateTag](const FUNDMeshByHealthState& Element)
	{
		return Element.StateTag == StateTag;
	});
	return State ? State->MeshesPerComponent : TArray<FUNDMeshForBodyPartComponent>();
}

void UUNDBossBodyPartPhase::UpdateMeshesForTag(FGameplayTag StateTag, APMTCAbilitySystemActor* Actor)
{
	if (const UUNDBodyPartConfiguration* BodyPart = GetBodyPartConfigurationForActor<UUNDBodyPartConfiguration>(Actor))
	{
		TArray<FUNDMeshForBodyPartComponent> Meshes = BodyPart->GetMeshesByTag(StateTag);
		for (const FUNDMeshForBodyPartComponent& Mesh : Meshes)
		{
			TArray<UActorComponent*> Components;
			if (!Mesh.SkeletalMesh.IsNull())
			{
				Components =
					Actor->GetComponentsByTag(UPMTCSkeletalMeshComponent::StaticClass(), Mesh.ComponentName);
				if (Components.Num() == 0)
				{
					Components =
						GetBodyPartManager()->GetOwner()->GetComponentsByTag(UPMTCSkeletalMeshComponent::StaticClass(), Mesh.ComponentName);
				}

				if (Components.Num() > 0)
				{
					static_cast<UPMTCSkeletalMeshComponent*>(Components[0])->SetMeshFromSoftMesh(Mesh.SkeletalMesh);
				}
			}
			else if (!Mesh.StaticMesh.IsNull())
			{
				Components =
					Actor->GetComponentsByTag(UPMTCSkeletalMeshComponent::StaticClass(), Mesh.ComponentName);
				if (Components.Num() == 0)
				{
					Components =
						GetBodyPartManager()->GetOwner()->GetComponentsByTag(UPMTCSkeletalMeshComponent::StaticClass(), Mesh.ComponentName);
				}

				if (Components.Num() > 0)
				{
					static_cast<UPMTCStaticMeshComponent*>(Components[0])->SetMeshFromSoftMesh(Mesh.StaticMesh);
				}
			}
		}
	}
}

void UUNDBossBodyPartPhase::TryApplyRagdollOnBodyPartMeshes(APMTCAbilitySystemActor* Actor)
{
	AActor* BodyPartActor = GetBodyPartManager()->GetOwner();
	if (const UUNDBodyPartConfiguration* BodyPart = GetBodyPartConfigurationForActor<UUNDBodyPartConfiguration>(Actor))
	{
		if (BodyPart->ShouldRagdollOnDestruction())
		{
			TArray<UActorComponent*> Components = GetComponentsForActor(Actor);
			for (UActorComponent* Component : Components)
			{
				if (USkeletalMeshComponent* SkeletalMesh = Cast<USkeletalMeshComponent>(Component))
				{
					// We need this to make sure the physics bodies are created.
					SkeletalMesh->SetLeaderPoseComponent(nullptr);
				}
				if (UPrimitiveComponent* PrimitiveComponent = Cast<UPrimitiveComponent>(Component))
				{
					PrimitiveComponent->SetCollisionProfileName(BodyPart->GetRagdollProfileName().Name);
					PrimitiveComponent->IgnoreActorWhenMoving(BodyPartActor, true);
					PrimitiveComponent->SetSimulatePhysics(true);
				}
			}
		}
		else
		{
			TArray<UActorComponent*> Components = GetComponentsForActor(Actor);
			for (UActorComponent* Component : Components)
			{
				if (Component && (!Component->GetIsReplicated() || BodyPartActor->HasAuthority()))
				{
					Component->DestroyComponent();
				}
			}
		}
	}
}

UUNDBodyPartManagerComponent::UUNDBodyPartManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bSwitchToNewPhaseAutomatically = true;
}
