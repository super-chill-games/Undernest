﻿// Copyright Super Chill Games. All Rights Reserved.


#include "GameplayEffects/EEC/UNDDashCooldownEEC.h"

// PMTC Includes
#include "Interfaces/PMTCAbilitySystemInterface.h"

// UND Includes
#include "Types/UNDNativeTags.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "GameFramework/Pawn.h"
#include "EngineUtils.h"


void UUNDDashCooldownEEC::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
                                                 FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	if (const FHitResult* HitResult = ExecutionParams.GetOwningSpec().GetContext().GetHitResult())
	{
		// Do not reset cooldown when hitting non pawns.
		const AActor* HitActor = HitResult->GetActor();
		if (!HitActor || HitActor->IsActorBeingDestroyed())
		{
			return;
		}
		if (!HitActor->GetClass()->IsChildOf(APawn::StaticClass()))
		{
			return;
		}
		if (UAbilitySystemComponent* HitASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(HitActor))
		{
			if (!HitASC->HasMatchingGameplayTag(UNDNativeTags::StateDashMark))
			{
				// TODO(Brian): Complete me.
				if (bRemoveDashMarkOnOtherAIs)
				{
					for (TActorIterator<AActor> It(HitASC->GetWorld()); It; ++It)
					{
						if (UAbilitySystemComponent* OtherASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponentDontCreate(*It))
						{
							if (OtherASC->HasMatchingGameplayTag(UNDNativeTags::StateDashMark))
							{
								OtherASC->RemoveActiveGameplayEffectBySourceEffect(DashMarkGE, ExecutionParams.GetTargetAbilitySystemComponent(), 1);
							}
						}
					}
				}

				if (DashMarkGE)
				{
					ExecutionParams.GetTargetAbilitySystemComponent()->ApplyGameplayEffectToTarget(
						DashMarkGE.GetDefaultObject(), HitASC, ExecutionParams.GetOwningSpec().GetLevel(),
						ExecutionParams.GetOwningSpec().GetContext());
				}
				

				if (DashReductionGE)
				{
					ExecutionParams.GetTargetAbilitySystemComponent()->ApplyGameplayEffectToSelf(
						DashReductionGE.GetDefaultObject(), ExecutionParams.GetOwningSpec().GetLevel(),
						ExecutionParams.GetOwningSpec().GetContext());
				}
			}
		}
	}
}
