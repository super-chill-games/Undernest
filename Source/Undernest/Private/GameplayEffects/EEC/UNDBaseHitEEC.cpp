// Copyright Super Chill Games. All Rights Reserved.


#include "GameplayEffects/EEC/UNDBaseHitEEC.h"

// PMTC Includes
#include "Types/PMTCNativeTags.h"

// UND Includes
#include "Attributes/UNDDamageMetaAttributeSet.h"
#include "Attributes/UNDHealthAttributeSet.h"
#include "Settings/UNDGameSettings.h"
#include "Types/UNDNativeTags.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "Globals/UNDPersistentData.h"
#include "Interfaces/PMTINInventoryTrackerInterface.h"
#include "Inventory/PMTINBaseInventoryItem.h"
#include "Items/UNDQuantifiableInventoryItem.h"
#include "Types/PMTMWNativeTags.h"

UUNDBaseHitEEC::UUNDBaseHitEEC()
{
	DEFINE_ATTRIBUTE_CAPTUREDEF(UUNDHealthAttributeSet, FungiHealth, Target, false);
	DEFINE_ATTRIBUTE_CAPTUREDEF(UUNDHealthAttributeSet, MaxFungiHealth, Target, false);
	DEFINE_ATTRIBUTE_CAPTUREDEF(UUNDHealthAttributeSet, MaxFungiDamageAllowedPerStunState, Target, false);
	DEFINE_ATTRIBUTE_CAPTUREDEF(UUNDDamageMetaAttributeSet, MetaFungiDamage, Target, false);
	DEFINE_ATTRIBUTE_CAPTUREDEF(UUNDDamageMetaAttributeSet, AccumulatedFungiDamage, Target, false);

	RelevantAttributesToCapture.Add(FungiHealthDef);
	RelevantAttributesToCapture.Add(MaxFungiHealthDef);
	RelevantAttributesToCapture.Add(MaxFungiDamageAllowedPerStunStateDef);
	RelevantAttributesToCapture.Add(MetaFungiDamageDef);
	RelevantAttributesToCapture.Add(AccumulatedFungiDamageDef);
}

void UUNDBaseHitEEC::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
                                            FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	UAbilitySystemComponent* TargetASC =
		ExecutionParams.GetTargetAbilitySystemComponent();
	// Ignore everything if we are immune to all kind of damage.
	if (TargetASC->HasMatchingGameplayTag(PMTCNativeTags::ImmunityAll))
	{
		return;
	}

	AActor* SourceAvatar = GetInstigatorFromContext(ExecutionParams, OutExecutionOutput);

	if (IsActorAI(SourceAvatar))
	{
		// Ignore.
		if (TargetASC->HasMatchingGameplayTag(UNDNativeTags::ImmunityAI))
		{
			return;
		}
	}

	// Can't hit teammate, don't do anything then.
	if (!IsAllowedToHitTargetIfTeammate(ExecutionParams, OutExecutionOutput))
	{
		return;
	}

	FGameplayTagContainer SpecAssetTags;
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	Spec.GetAllAssetTags(SpecAssetTags);

	// Gather the tags from the source and target as that can affect which buffs should be used
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	const float FungiHealthRemaining = CalculateAttributeValue(ExecutionParams, FungiHealthDef, EvaluationParameters, 0.0f);
	const float HealthRemaining = CalculateAttributeValue(ExecutionParams, HealthDef, EvaluationParameters, 0.0f);
	// If the target is dead we don't want to generate any feedback
	if (TargetTags->HasTag(PMTCNativeTags::StateDead) || HealthRemaining == 0.0f)
	{
		return;
	}

	OutExecutionOutput.MarkConditionalGameplayEffectsToTrigger();

	// 1. Try process fungi damage first.
	const bool bHasFungiSupport = CalculateAttributeValue(ExecutionParams, MaxFungiHealthDef, EvaluationParameters, 0.0f) > 0.0f;
	float FungiDamageDone = 0.0f;
	if (!TargetTags->HasTag(UNDNativeTags::ImmunityFungi) && bHasFungiSupport)
	{
		FungiDamageDone = CalculateAttributeValue(ExecutionParams, MetaFungiDamageDef, EvaluationParameters, 0.0f);
	}
	const float CurrentAccumulatedFungiDamage = CalculateAttributeValue(ExecutionParams, AccumulatedFungiDamageDef, EvaluationParameters, 0.0f);
	const float MaxAllowedFungiDamage = CalculateAttributeValue(ExecutionParams, MaxFungiDamageAllowedPerStunStateDef, EvaluationParameters, 0.0f);
	bool bKillStunGameplayEffect = false;
	if (MaxAllowedFungiDamage > 0.0f && FungiDamageDone > 0.0f)
	{
		if (CurrentAccumulatedFungiDamage + FungiDamageDone >= MaxAllowedFungiDamage)
		{
			FungiDamageDone = MaxAllowedFungiDamage - CurrentAccumulatedFungiDamage;
			bKillStunGameplayEffect = true;
			// Make sure to reset accumulated fungi damage for the next time.
			OutExecutionOutput.AddOutputModifier(
				FGameplayModifierEvaluatedData(AccumulatedFungiDamageProperty, EGameplayModOp::Override, 0.0f));
		}
		else
		{
			OutExecutionOutput.AddOutputModifier(
				FGameplayModifierEvaluatedData(AccumulatedFungiDamageProperty, EGameplayModOp::Additive, FungiDamageDone));
		}
	}

	if (FungiDamageDone != 0.0f)
	{
		if (FungiDamageDone > 0.0f && FungiDamageDone >= FungiHealthRemaining)
		{
			FungiDamageDone = FungiHealthRemaining;
		}

		if (APawn* SourcePawn = Cast<APawn>(SourceAvatar))
		{
			if (SourcePawn->IsPlayerControlled())
			{
				// TODO(Brian): Calculate currency count based on fungi damage absorbed.
			}
		}
		OutExecutionOutput.AddOutputModifier(
			FGameplayModifierEvaluatedData(FungiHealthProperty, EGameplayModOp::Additive, -1.0f * FungiDamageDone));
	}

	// 2. Process health damage.
	float DamageDone = 0.0f;
	if (!TargetASC->HasMatchingGameplayTag(PMTCNativeTags::ImmunityDamage))
	{
		const bool bCanParryHit = TargetASC->HasMatchingGameplayTag(PMTMWNativeTags::StateParry) && !SpecAssetTags.HasTag(PMTMWNativeTags::HitDescriptorCantParry);
		if (!bCanParryHit)
		{
			DamageDone = CalculateAttributeValue(ExecutionParams, MetaDamageDef, EvaluationParameters, 0.0f);
		}
	}

	// 3. Kill target if fungi health is depleted and we support fungi health.
	const bool bHasDepletedFungiEnergy = (FungiHealthRemaining - FungiDamageDone <= 0.0f);
	if (bHasDepletedFungiEnergy && bHasFungiSupport)
	{
		// Immediately kill it, since the real health is fungi.
		DamageDone = HealthRemaining;
	}

	// 4. If no damage is done, make sure to add very tiny value so any hit event is received properly.
	if (DamageDone == 0.0f)
	{
		if (!TargetASC->HasMatchingGameplayTag(PMTCNativeTags::ImmunityHit) && !SpecAssetTags.HasTag(UNDNativeTags::GameplayEffectIgnoreHit))
		{
			DamageDone = KINDA_SMALL_NUMBER;
		}
	}

	// 5. By verifying that the value is zero, we avoid calling dirtiness on the attribute.
	if (DamageDone != 0.0f)
	{
		// Insta kill if there is no more fungi energy.
		if (bHasDepletedFungiEnergy && bHasFungiSupport)
		{
			DamageDone = HealthRemaining;
		}
		// Deny killing if there is still fungi health remaining.
		else if (bHasFungiSupport && (HealthRemaining - DamageDone <= 1.0f))
		{
			if (HealthRemaining > 1.0f)
			{
				DamageDone = HealthRemaining - 1.0f;
				// 6. Stun it, since we reached the health required for it.
				FGameplayEffectSpecHandle StunSpecHandle = TargetASC->MakeOutgoingSpec(FungiStunGameplayEffect,
					1.0f, Spec.GetEffectContext());
				TargetASC->ApplyGameplayEffectSpecToSelf(*StunSpecHandle.Data);
			}
			else
			{
				DamageDone = KINDA_SMALL_NUMBER;
			}
		}
		OutExecutionOutput.AddOutputModifier(
			FGameplayModifierEvaluatedData(HealthProperty, EGameplayModOp::Additive, -1.0f * DamageDone));
	}

	// Ensure value is reset before applying any other gameplay effect.
	OutExecutionOutput.AddOutputModifier(
		FGameplayModifierEvaluatedData(MetaDamageProperty, EGameplayModOp::Override, 0.0f));
	
	const FGameplayTagContainer HitTags = SpecAssetTags.Filter(FGameplayTagContainer(PMTCNativeTags::Hit));
	GrantFungiEnergy(ExecutionParams, FungiDamageDone);
	if (DamageDone >= HealthRemaining)
	{
		if (!SpecAssetTags.HasTag(UNDNativeTags::GameplayEffectNonRescueAnt) && bHasFungiSupport)
        {
			if (TargetASC->HasMatchingGameplayTag(UNDNativeTags::CharacterDescriptorAnt))
			{
				UUNDPersistentData* PersistentData = UUNDPersistentData::GetPersistentData(TargetASC);
				PersistentData->AddRescuedAnt();
			}
        }
		GenerateDeadFeedbackData(ExecutionParams, OutExecutionOutput, DamageDone, HitTags, SourceAvatar);
	}
	else
	{
		TryApplyForceToTarget(ExecutionParams);
	}

	GenerateHitFeedbackData(ExecutionParams, OutExecutionOutput, DamageDone, HitTags, SourceAvatar);
	ApplyAdditionalGEs(ExecutionParams, OutExecutionOutput);

	if (bKillStunGameplayEffect)
	{
		TargetASC->RemoveActiveEffectsWithTags(FGameplayTagContainer(UNDNativeTags::GameplayEffectFungiStun));
	}
}

void UUNDBaseHitEEC::GrantFungiEnergy(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
	float FungiDamageDone) const
{
	if (ExecutionParams.GetTargetAbilitySystemComponent()->HasMatchingGameplayTag(UNDNativeTags::StateDenyFungiEnergyAbsorb))
	{
		return;
	}
	FGameplayTagContainer SpecAssetTags;
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	Spec.GetAllAssetTags(SpecAssetTags);

	if (SpecAssetTags.HasTag(UNDNativeTags::GameplayEffectAbsorbFungi))
	{
		UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();
		if (SourceASC)
		{
			if (IPMTINInventoryGetterInterface* InventoryGetterInterface = Cast<IPMTINInventoryGetterInterface>(SourceASC->GetAvatarActor()))
			{
				const TArray<UPMTINBaseInventoryItem*> FungiEnergyItem = InventoryGetterInterface->GetInventoryTrackerInterface(FGameplayTag())->GetAllItemsOfUniqueType(UNDNativeTags::InventoryItemQuantifiableFungiEnergy);
				if (FungiEnergyItem.Num() > 0)
				{
					if (UUNDQuantifiableInventoryItem* QuantifiableInventoryItem = Cast<UUNDQuantifiableInventoryItem>(FungiEnergyItem[0]))
					{
						QuantifiableInventoryItem->IncrementItemQuantityByDecimal(FMath::Abs(FungiDamageDone) * GetDefault<UUNDGameSettings>()->GetFungiResourceMultiplier());
					}
				}
			}
		}
	}
}
