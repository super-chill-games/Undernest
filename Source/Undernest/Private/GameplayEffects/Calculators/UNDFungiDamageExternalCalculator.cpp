﻿// Copyright Super Chill Games. All Rights Reserved.


#include "GameplayEffects/Calculators/UNDFungiDamageExternalCalculator.h"

// UND Includes
#include "Settings/UNDGameSettings.h"
#include "Types/UNDNativeTags.h"

float UUNDFungiDamageExternalCalculator::CalculateAttribute_Implementation(UAbilitySystemComponent* TargetASC,
	const FGameplayEffectSpec& Spec, FGameplayAttribute Attribute, float AttributeValue) const
{
	if (TargetASC->HasMatchingGameplayTag(UNDNativeTags::StateFungiStun))
	{
		return AttributeValue;
	}
	return GetDefault<UUNDGameSettings>()->GetNonFungiStunDamageMultiplier() * AttributeValue;
}
