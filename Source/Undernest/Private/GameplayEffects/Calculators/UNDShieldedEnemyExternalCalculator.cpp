﻿// Copyright Super Chill Games. All Rights Reserved.


#include "GameplayEffects/Calculators/UNDShieldedEnemyExternalCalculator.h"

// PMTC Includes
#include "Actors/PMTCAbilitySystemProxy.h"
#include "Interfaces/PMTCAbilitySystemInterface.h"

// UND Includes
#include "Types/UNDNativeTags.h"

float UUNDShieldedEnemyExternalCalculator::CalculateAttribute_Implementation(UAbilitySystemComponent* TargetASC,
	const FGameplayEffectSpec& Spec, FGameplayAttribute Attribute, float AttributeValue) const
{
	if (!TargetASC->HasMatchingGameplayTag(UNDNativeTags::StateShieldActive))
	{
		return AttributeValue;
	}
	UAbilitySystemComponent* SourceASC = Spec.GetContext().GetInstigatorAbilitySystemComponent();
	if (SourceASC)
	{
		if (APMTCAbilitySystemProxy* Proxy = Cast<APMTCAbilitySystemProxy>(SourceASC->GetAvatarActor()))
		{
			const AActor* Actor = nullptr;
			if (const AController* Controller = Proxy->GetOwner<AController>())
			{
				Actor = Controller->GetPawn();
			}
			else
			{
				Actor = Proxy->GetOwner();
			}
			SourceASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Actor);
		}
		const FVector OriginForward = SourceASC->GetAvatarActor()->GetActorForwardVector();
		const FVector OurForward = TargetASC->GetAvatarActor()->GetActorForwardVector();
		const float Dot = FVector::DotProduct(OriginForward, OurForward);
		// If we have the following:
		//
		// Source Forward ----> <----- Target Forward
		//
		// then it means the source is hitting in front of us, therefore since we have a shield, we negate
		// the value.
		if (Dot <= 0.0f)
		{
			return 0.0f;
		}

		// Now verify that the location is also backwards, this way we ensure that if the character hits the target while on its
		// back, it won't hit the enemy.

		const FVector OriginLocation = SourceASC->GetAvatarActor()->GetActorLocation();
		const FVector OurLocation = TargetASC->GetAvatarActor()->GetActorLocation();
		const FVector ToOurLocationDir = (OriginLocation - OurLocation).GetSafeNormal();
		const FVector OurBackDirection = OurForward * -1.0;
		const float BackDot = FVector::DotProduct(ToOurLocationDir, OurBackDirection);
		if (BackDot <= 0.0f)
		{
			return 0.0f;
		}
	}
	return Super::CalculateAttribute_Implementation(TargetASC, Spec, Attribute, AttributeValue);
}
