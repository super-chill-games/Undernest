// Copyright Super Chill Games. All Rights Reserved.


#include "Items/UNDQuantifiableInventoryItem.h"

// PMTIN Includes
#include "ActorComponents/PMTINInventoryTrackerComponent.h"

// UND Includes
#include "Items/UNDQuantifiableItemDefinition.h"

// UE Includes
#include "Net/Core/PushModel/PushModel.h"
#include "Net/UnrealNetwork.h"

int32 UUNDQuantifiableInventoryItem::GetItemQuantity() const
{
	return ItemQuantity;
}

int32 UUNDQuantifiableInventoryItem::IncrementItemQuantity(int32 InAmount)
{
	int64 NewQuantity = ItemQuantity;
	NewQuantity += InAmount;
	NewQuantity = FMath::Clamp(NewQuantity, 0, static_cast<int64>(GetItemMaxQuantity()));
	if (NewQuantity != ItemQuantity)
	{
		const uint16 OldValue = ItemQuantity;
		ItemQuantity = NewQuantity;
		MARK_PROPERTY_DIRTY_FROM_NAME(UUNDQuantifiableInventoryItem, ItemQuantity, this);
		OnRep_ItemQuantity(OldValue);
	}
	return ItemQuantity;
}

int32 UUNDQuantifiableInventoryItem::DecrementItemQuantity(int32 InAmount)
{
	int64 NewQuantity = ItemQuantity;
	NewQuantity -= InAmount;
	NewQuantity = FMath::Clamp(NewQuantity, 0, static_cast<int64>(GetItemMaxQuantity()));
	if (NewQuantity != ItemQuantity)
	{
		const uint16 OldValue = ItemQuantity;
		ItemQuantity = NewQuantity;
		MARK_PROPERTY_DIRTY_FROM_NAME(UUNDQuantifiableInventoryItem, ItemQuantity, this);
		OnRep_ItemQuantity(OldValue);
	}
	return ItemQuantity;
}

void UUNDQuantifiableInventoryItem::IncrementItemQuantityByDecimal(float InAmount)
{
	DecimalItemQuantity += InAmount;
	DecimalItemQuantity = FMath::Clamp(DecimalItemQuantity, 0.0f, static_cast<float>(GetItemMaxQuantity()));
	const int32 IntegerPart = FMath::Floor(DecimalItemQuantity);
	if (GetItemQuantity() != IntegerPart)
	{
		const int32 Difference = IntegerPart - GetItemQuantity();
		if (Difference > 0)
		{
			IncrementItemQuantity(Difference);
		}
		else
		{
			DecrementItemQuantity(Difference * -1);
		}
	}
}

int32 UUNDQuantifiableInventoryItem::GetItemMaxQuantity_Implementation() const
{
	return FMath::Clamp(GetItemDefinition<UUNDQuantifiableItemDefinition>()->GetBaseMaxQuantity(), 0, static_cast<int32>(TNumericLimits<uint16>::Max()));
}

void UUNDQuantifiableInventoryItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	Params.Condition = COND_OwnerOnly;

	DOREPLIFETIME_WITH_PARAMS_FAST(UUNDQuantifiableInventoryItem, ItemQuantity, Params);
}

void UUNDQuantifiableInventoryItem::OnRep_ItemQuantity(uint16 OldValue)
{
	if (OldValue != ItemQuantity)
	{
		if (UPMTINInventoryTrackerComponent* Inventory = GetItemInventoryOwner())
		{
			Inventory->HandleItemUpdated(this);
		}
	}
}
