// Copyright Super Chill Games. All Rights Reserved.


#include "Items/UNDMeleeItemDefinition.h"

// UND Includes
#include "Items/UNDMeleeInventoryItem.h"

UUNDMeleeItemDefinition::UUNDMeleeItemDefinition()
{
	ItemInstanceClass = UUNDMeleeInventoryItem::StaticClass();
}