// Copyright Super Chill Games. All Rights Reserved.


#include "Items/UNDBaseItemDefinition.h"

// UND Includes
#include "Items/UNDBaseInventoryItem.h"

UUNDBaseItemDefinition::UUNDBaseItemDefinition()
{
	ItemInstanceClass = UUNDBaseInventoryItem::StaticClass();
}
