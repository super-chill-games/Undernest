// Copyright Super Chill Games. All Rights Reserved.


#include "Items/UNDQuantifiableItemDefinition.h"

// UND Includes
#include "Items/UNDQuantifiableInventoryItem.h"

UUNDQuantifiableItemDefinition::UUNDQuantifiableItemDefinition()
{
	ItemInstanceClass = UUNDQuantifiableInventoryItem::StaticClass();
}
