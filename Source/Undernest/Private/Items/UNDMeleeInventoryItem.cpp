// Copyright Super Chill Games. All Rights Reserved.


#include "Items/UNDMeleeInventoryItem.h"

// PMTC Includes
#include "Interfaces/PMTCAbilitySystemInterface.h"

// PMTIN Includes
#include "ActorComponents/PMTINInventoryTrackerComponent.h"

// PMTMW Includes
#include "ActorComponents/PMTMWMeleeUserComponent.h"
#include "Actors/PMTMWMeleeAbilitySystemProxy.h"
#include "Melee/PMTMWMeleeWeapon.h"

// UND Includes
#include "Items/UNDMeleeItemDefinition.h"
#include "Melee/UNDMeleeData.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/Pawn.h"
#include "Types/PMTINNativeTags.h"

const UPMTMWMeleeData* UUNDMeleeInventoryItem::GetMeleeData() const
{
	if (UUNDMeleeItemDefinition* LocalItemDefinition = GetItemDefinition<UUNDMeleeItemDefinition>())
	{
		return LocalItemDefinition->GetMeleeData().GetDefaultObject();
	}
	return nullptr;
}

FPMTCSimpleHandle UUNDMeleeInventoryItem::GetMeleeWeaponHandle() const
{
	return GetItemHandle();
}

void UUNDMeleeInventoryItem::HandleItemAddedToLoadout_Implementation()
{
	Super::HandleItemAddedToLoadout_Implementation();

	AActor* LocalInventoryOwner = GetItemInventoryOwner()->GetOwner();

	if (UPMTMWMeleeUserComponent* MeleeUserComponent = LocalInventoryOwner->FindComponentByClass<UPMTMWMeleeUserComponent>())
	{
		if (MeleeUserComponent->GetAllMeleeWeapons().Num() == 0)
		{
			APawn* PawnOwner = Cast<APawn>(LocalInventoryOwner);
			AActor* ProxyOwner = PawnOwner;
			if (PawnOwner->GetController())
			{
				ProxyOwner = PawnOwner->GetController();
			}
			MeleeUserComponent->InitializeProxies(GetLoadoutComponent()->GetSlotCountForTag(
				FGameplayTagContainer(PMTINNativeTags::LoadoutRestrictionMelee)), ProxyOwner,
				TSubclassOf<APMTMWMeleeAbilitySystemProxy>(), TSubclassOf<APMTMWMeleeWeapon>(), false);
		}

		UAbilitySystemComponent* OwnerASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(LocalInventoryOwner);
		MeleeUserComponent->SetupMeleeWeapon(
			OwnerASC,
			*this,
			OwnerASC->MakeEffectContext());
	}
}

void UUNDMeleeInventoryItem::HandleItemRemovedFromLoadout_Implementation()
{
	Super::HandleItemRemovedFromLoadout_Implementation();
}

void UUNDMeleeInventoryItem::HandleItemEquipped_Implementation()
{
	Super::HandleItemEquipped_Implementation();

	AActor* LocalOwner = GetItemInventoryOwner()->GetOwner();

	if (UPMTMWMeleeUserComponent* MeleeUserComponent = LocalOwner->FindComponentByClass<UPMTMWMeleeUserComponent>())
	{
		UAbilitySystemComponent* OwnerASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(GetItemInventoryOwner()->GetOwner());
		MeleeUserComponent->ActivateMeleeWeapon(
			OwnerASC,
			*this,
			OwnerASC->MakeEffectContext());
	}
}

void UUNDMeleeInventoryItem::HandleItemUnequipped_Implementation()
{
	Super::HandleItemUnequipped_Implementation();

	AActor* LocalOwner = GetItemInventoryOwner()->GetOwner();

	if (UPMTMWMeleeUserComponent* MeleeUserComponent = LocalOwner->FindComponentByClass<UPMTMWMeleeUserComponent>())
	{
		UAbilitySystemComponent* OwnerASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(GetItemInventoryOwner()->GetOwner());
		MeleeUserComponent->DeactivateMeleeWeapon(
			OwnerASC,
			*this);
	}
}

void UUNDMeleeInventoryItem::HandleItemDestruction()
{
	const AActor* LocalOwner = GetItemInventoryOwner()->GetOwner();
	if (UPMTMWMeleeUserComponent* MeleeUserComponent = LocalOwner->FindComponentByClass<UPMTMWMeleeUserComponent>())
	{
		MeleeUserComponent->FreeMeleeWeapon(
			UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(LocalOwner), GetItemHandle());
	}
	Super::HandleItemDestruction();
}
