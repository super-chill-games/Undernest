// Copyright Super Chill Games. All Rights Reserved.


#include "Subsystems/UNDCorruptionWorldSubsystem.h"

// UND Includes
#include "Corruption/UNDCorruptionManager.h"

UUNDCorruptionWorldSubsystem::UUNDCorruptionWorldSubsystem()
{
	CorruptionManagerClass = UUNDCorruptionManager::StaticClass();
}

void UUNDCorruptionWorldSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	TSubclassOf<UUNDCorruptionManager> ManagerClass = CorruptionManagerClass.IsValid() ?
		LoadClass<UUNDCorruptionManager>(NULL, *CorruptionManagerClass.ToString(), NULL, LOAD_None, NULL) : UUNDCorruptionManager::StaticClass();
	CorruptionManager = NewObject<UUNDCorruptionManager>(this, ManagerClass);
}

void UUNDCorruptionWorldSubsystem::PostInitialize()
{
	Super::PostInitialize();
	CorruptionManager->InitializeCorruptionManager();
}

UUNDCorruptionWorldSubsystem* UUNDCorruptionWorldSubsystem::GetCorruptionWorldSSFromContext(UObject* WorldContext)
{
	if (WorldContext)
	{
		if (UWorld* World = WorldContext->GetWorld())
		{
			return World->GetSubsystem<UUNDCorruptionWorldSubsystem>();
		}
	}
	return nullptr;
}

void UUNDCorruptionWorldSubsystem::UpdateCorruptionRadius(UObject* WorldContextObject, float Radius)
{
	if (WorldContextObject)
	{
		if (UWorld* World = WorldContextObject->GetWorld())
		{
			World->GetSubsystem<UUNDCorruptionWorldSubsystem>()->GetCorruptionManager()->UpdateCorruptionRadius(Radius);
		}
	}
}

void UUNDCorruptionWorldSubsystem::UpdateCorruptionCenter(UObject* WorldContextObject, const FVector& Center)
{
	if (WorldContextObject)
	{
		if (UWorld* World = WorldContextObject->GetWorld())
		{
			World->GetSubsystem<UUNDCorruptionWorldSubsystem>()->GetCorruptionManager()->UpdateCorruptionCenter(Center);
		}
	}
}

UUNDCorruptionManager* UUNDCorruptionWorldSubsystem::GetCorruptionManager() const
{
	return CorruptionManager;
}
