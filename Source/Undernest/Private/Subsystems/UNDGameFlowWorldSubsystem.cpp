// Copyright Super Chill Games. All Rights Reserved.


#include "Subsystems/UNDGameFlowWorldSubsystem.h"

DEFINE_LOG_CATEGORY(UNDGameFlowLog);

UUNDGameFlowWorldSubsystem* UUNDGameFlowWorldSubsystem::GetGameFlowWorldSSFromContext(UObject* WorldContext)
{
	if (WorldContext)
	{
		if (UWorld* World = WorldContext->GetWorld())
		{
			return World->GetSubsystem<UUNDGameFlowWorldSubsystem>();
		}
	}
	return nullptr;
}

void UUNDGameFlowWorldSubsystem::NotifyGameFlowEvent(const FGameplayTag& EventTag, bool bForceNotify, UObject* Caller)
{
	FUNDGameFlowEventDelegates& Delegates = GetOrCreateEventForTag(EventTag);
	if (!Delegates.bHasEventBeingNotified || bForceNotify)
	{
		Delegates.bHasEventBeingNotified = true;
		Delegates.OnEvent.Broadcast(EventTag);
		Delegates.OnEventNative.Broadcast(EventTag);

#if !UE_BUILD_SHIPPING
		if (Caller)
		{
			UE_LOG(UNDGameFlowLog, Log, TEXT("'%s': GameFlow Event '%s' notified by '%s'"), ANSI_TO_TCHAR(__FUNCTION__), *EventTag.ToString(), *Caller->GetName());
		}
#endif // !UE_BUILD_SHIPPING
	}
#if !UE_BUILD_SHIPPING
	else if (Caller)
	{
		UE_LOG(UNDGameFlowLog, Warning, TEXT("'%s': '%s' tried to notify GameFlow Event '%s' but was already notified."), ANSI_TO_TCHAR(__FUNCTION__), *Caller->GetName(), *EventTag.ToString());
	}
#endif // !UE_BUILD_SHIPPING
}

void UUNDGameFlowWorldSubsystem::BindOrCallForEvent(const FGameplayTag& EventTag, FUNDGameFlowDynamicEvent Event, bool bBroadcastIfAlreadyNotified)
{
	if (!EventTag.IsValid())
	{
		return;
	}
	FUNDGameFlowEventDelegates& Delegates = GetOrCreateEventForTag(EventTag);
	if (!Delegates.bHasEventBeingNotified)
	{
		Delegates.OnEvent.AddUnique(Event);
	}
	else if (bBroadcastIfAlreadyNotified)
	{
		Event.ExecuteIfBound(EventTag);
	}
}

void UUNDGameFlowWorldSubsystem::BindOrCallForEvent(const FGameplayTag& EventTag, FUNDGameFlowEvent Event, bool bBroadcastIfAlreadyNotified)
{
	if (!EventTag.IsValid())
	{
		return;
	}
	FUNDGameFlowEventDelegates& Delegates = GetOrCreateEventForTag(EventTag);
	if (!Delegates.bHasEventBeingNotified)
	{
		Delegates.OnEventNative.Add(Event);
	}
	else if (bBroadcastIfAlreadyNotified)
	{
		Event.ExecuteIfBound(EventTag);
	}
}

FUNDGameFlowEventDelegates& UUNDGameFlowWorldSubsystem::GetOrCreateEventForTag(const FGameplayTag& EventTag)
{
	FUNDGameFlowEventDelegates* Delegates = Events.FindByPredicate([&EventTag](const FUNDGameFlowEventDelegates& Element)
	{
		return Element.EventTag == EventTag;
	});
	return Delegates ? *Delegates : Events.Emplace_GetRef(EventTag);
}
