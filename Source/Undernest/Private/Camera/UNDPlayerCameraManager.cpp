// Copyright Super Chill Games. All Rights Reserved.


#include "Camera/UNDPlayerCameraManager.h"

AUNDPlayerCameraManager::AUNDPlayerCameraManager()
{
	FadeTransitionParameterName = FName(TEXT("FadeTransition"));
}

void AUNDPlayerCameraManager::UpdateFadeTransition(float Value)
{
	UpdateCameraScalarMaterialParameter(FadeTransitionParameterName, Value);
}
