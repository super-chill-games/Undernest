// Copyright Super Chill Games. All Rights Reserved.


#include "Corruption/UNDCorruptionManager.h"

// UND Includes
#include "Interfaces/UNDCorruptionInterface.h"
#include "Types/UNDNativeTags.h"

// UE Includes
#include "EngineUtils.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Materials/MaterialParameterCollection.h"

UUNDCorruptionManager::UUNDCorruptionManager()
{
	CurrentCorruptionState = UNDNativeTags::CorruptionStateCorrupted;
	CorruptionRadiusParameterName = TEXT("CorruptionRadius");
	CorruptionCenterParameterName = TEXT("CorruptionCenter");
}

void UUNDCorruptionManager::SetCorruptionState(const FGameplayTag& NewCorruptionState)
{
	if (CurrentCorruptionState == NewCorruptionState)
	{
		return;
	}
	const FGameplayTag OldCorruptionState = CurrentCorruptionState;
	CurrentCorruptionState = NewCorruptionState;
	for (AActor* Actor : CorruptionActors)
	{
		IUNDCorruptionInterface::Execute_HandleCorruptionStateChange(Actor, OldCorruptionState, CurrentCorruptionState);
	}
}

void UUNDCorruptionManager::UpdateCorruptionRadius(float NewRadius)
{
	if (CorruptionParameterCollection)
	{
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), CorruptionParameterCollection,
			CorruptionRadiusParameterName, NewRadius);
	}
}

void UUNDCorruptionManager::UpdateCorruptionCenter(const FVector& Center)
{
	if (CorruptionParameterCollection)
	{
		UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), CorruptionParameterCollection,
			CorruptionCenterParameterName, FLinearColor(Center));
	}
}

void UUNDCorruptionManager::InitializeCorruptionManager_Implementation()
{
	GetWorld()->OnActorsInitialized.AddUObject(this, &UUNDCorruptionManager::OnActorsInitialized);
}

void UUNDCorruptionManager::HandleCorruptedActorSpawned_Implementation(AActor* InActor)
{

}

void UUNDCorruptionManager::HandleCorruptedActorDestroyed_Implementation(AActor* InActor)
{

}

void UUNDCorruptionManager::OnActorSpawned(AActor* InActor)
{
	if (InActor->Implements<UUNDCorruptionInterface>())
	{
		InActor->OnEndPlay.AddDynamic(this, &UUNDCorruptionManager::OnCorruptionActorEndPlay);
		CorruptionActors.Add(InActor);
		HandleCorruptedActorSpawned(InActor);
		IUNDCorruptionInterface::Execute_HandleCorruptionStateChange(InActor, FGameplayTag(), CurrentCorruptionState);
	}
}

void UUNDCorruptionManager::OnActorsInitialized(const UWorld::FActorsInitializedParams& InitializedParams)
{
	GetWorld()->AddOnActorSpawnedHandler(FOnActorSpawned::FDelegate::CreateUObject(this, &UUNDCorruptionManager::OnActorSpawned));
	for (TActorIterator<AActor> It(GetWorld()); It; ++It)
	{
		OnActorSpawned(*It);
	}
}

void UUNDCorruptionManager::OnCorruptionActorEndPlay(AActor* Actor, EEndPlayReason::Type EndPlayReason)
{
	CorruptionActors.RemoveSingleSwap(Actor);
	HandleCorruptedActorDestroyed(Actor);
}
