﻿// Copyright Super Chill Games. All Rights Reserved.


#include "Gameplay/UNDParrySplineActor.h"

// UE Includes
#include "Components/CapsuleComponent.h"
#include "Components/SplineComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"

AUNDParrySplineActor::AUNDParrySplineActor()
{
	PrimaryActorTick.bCanEverTick = false;
	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	RootComponent = SplineComponent;
	SplineComponent->ClearSplinePoints();
	FloorTrace.TraceType = EPMTCTraceType::ObjectTypes;
	FloorTrace.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
	FloorTrace.bMultiTrace = false;
	FloorTrace.bReturnPhysicalMaterial = false;
	FloorTrace.TraceDistance = 200.0;
}

USplineComponent* AUNDParrySplineActor::GenerateFlyingPath(const AActor* InOwner, const AActor* Target)
{
	SplineComponent->ClearSplinePoints(true);
	if (!InOwner || !Target)
	{
		return nullptr;
	}
	const FVector OwnerLocation = InOwner->GetActorLocation();
	const FVector TargetLocation = Target->GetActorLocation();
	double OwnerSize;
	double TargetSize;
	if (const ACharacter* TargetCharacter = Cast<ACharacter>(Target))
	{
		// We divide by two because the location is from the origin of the capsule.
		TargetSize = (TargetCharacter->GetCapsuleComponent()->GetScaledCapsuleRadius());
	}
	else
	{
		FVector Origin;
		FVector BoxExtent;
		Target->GetActorBounds(true, Origin, BoxExtent);
		TargetSize = BoxExtent.GetMax();
	}

	if (const ACharacter* CharacterOwner = Cast<ACharacter>(InOwner))
	{
		OwnerSize = (CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleRadius());
	}
	else
	{
		FVector Origin;
		FVector BoxExtent;
		InOwner->GetActorBounds(true, Origin, BoxExtent);
		OwnerSize = BoxExtent.GetMax();
	}

	const FVector TargetDirection = (TargetLocation - OwnerLocation).GetSafeNormal();
	const FVector BackLocation = ProjectBackLocationToFloor(InOwner, Target, TargetLocation + (TargetDirection * (TargetSize + OwnerSize)));
	// This location is used so we can make the character face the back of the target parried.
	const FVector ExtendedBackLocation = BackLocation + (TargetDirection * 100.0);
	const FVector MiddlePoint = (OwnerLocation + BackLocation) / 2.0;

	SplineComponent->AddSplineWorldPoint(OwnerLocation); // Index 0
	SplineComponent->AddSplineWorldPoint(MiddlePoint); // Index 1
	SplineComponent->AddSplineWorldPoint(ExtendedBackLocation); // Index 2
	SplineComponent->AddSplineWorldPoint(BackLocation); // Index 3
	SplineComponent->UpdateSpline();
	SplineComponent->SetRotationAtSplinePoint(3, UKismetMathLibrary::FindLookAtRotation(BackLocation, TargetLocation), ESplineCoordinateSpace::World, true);
	// SplineComponent->SetSplinePointType(2, ESplinePointType::Curve, false);
	// SplineComponent->SetSplinePointType(3, ESplinePointType::Constant, true);

	const FVector MiddlePointAdjusted = SplineComponent->GetLocationAtSplinePoint(1, ESplineCoordinateSpace::World);
	const FVector WorldUp = SplineComponent->GetUpVectorAtSplinePoint(1, ESplineCoordinateSpace::World);
	const FVector WorldRight = SplineComponent->GetRightVectorAtSplinePoint(1, ESplineCoordinateSpace::World);
	const float GroundDir = FMath::FRandRange(0.0f, 1.0f) <= 0.5f ? 1.0f : -1.0f;
	const FVector SideDirection = WorldRight * GroundDir;
	const float ArcAlpha = FMath::RandRange(0.0f, 1.0f);
	const FVector DesiredUpLocation = MiddlePointAdjusted + (WorldUp * CurvatureDistance);
	const FVector DesiredSideLocation = MiddlePointAdjusted + (SideDirection * CurvatureDistance);

	const FVector NewMiddleLocation = FMath::Lerp(DesiredSideLocation, DesiredUpLocation, ArcAlpha);
	SplineComponent->SetLocationAtSplinePoint(1, NewMiddleLocation, ESplineCoordinateSpace::World);

	// Once the spline is updated, we want to move the "MiddlePoint" in a random arc of 180 degrees.
	const FVector TangentDir = (SplineComponent->GetTangentAtSplinePoint(1, ESplineCoordinateSpace::Local)).GetSafeNormal();
	const FVector Curvature = TangentDir * MaxCurvature;
	SplineComponent->SetTangentAtSplinePoint(1, Curvature, ESplineCoordinateSpace::Local, true);

	// TODO(Brian): Make path validations.
	
	return SplineComponent;
}

USplineComponent* AUNDParrySplineActor::GetSplineComponent() const
{
	return SplineComponent;
}

FVector AUNDParrySplineActor::ProjectBackLocationToFloor(const AActor* InOwner, const AActor* Target,
	const FVector& BackLocation)
{
	FloorTrace.TraceStart = BackLocation;
	FloorTrace.TraceEnd = BackLocation + (Target->GetActorUpVector() * -1.0 * FloorTrace.TraceDistance);
	FloorTrace.ActorsToIgnore.Reset();
	// Horrible but safe.
	FloorTrace.ActorsToIgnore.Add(const_cast<AActor*>(InOwner));
	FloorTrace.ActorsToIgnore.Add(const_cast<AActor*>(Target));
	const TArray<FHitResult> Hits = FloorTrace.MakeTrace(this);
	if (Hits.Num() == 0)
	{
		return BackLocation;
	}

	float OwnerHalfSize;
	if (const ACharacter* Character = Cast<ACharacter>(InOwner))
	{
		OwnerHalfSize = Character->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	}
	else
	{
		FVector Origin;
		FVector BoxExtent;
		InOwner->GetActorBounds(true, Origin, BoxExtent);
		OwnerHalfSize = BoxExtent.Z;
	}
	return Hits[0].ImpactPoint + FVector(0.0, 0.0, OwnerHalfSize);
}
