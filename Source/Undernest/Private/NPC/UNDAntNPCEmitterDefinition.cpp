﻿// Copyright Super Chill Games. All Rights Reserved.


#include "NPC/UNDAntNPCEmitterDefinition.h"

#include "Globals/UNDPersistentData.h"

bool UUNDAntNPCEmitterDefinition::IsNPCUnlocked(const UObject* WorldContext) const
{
	if (const UUNDPersistentData* PersistentData = UUNDPersistentData::GetPersistentData(WorldContext))
	{
		return PersistentData->GetRescuedAnts() >= RequiredSavedAnts;
	}
	return false;
}
