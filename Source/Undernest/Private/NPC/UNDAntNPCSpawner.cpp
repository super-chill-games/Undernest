﻿// Copyright Super Chill Games. All Rights Reserved.


#include "NPC/UNDAntNPCSpawner.h"

#include "ActorComponents/PMTDDialogueUserComponent.h"
#include "FunctionLibraries/PMTCCommonHelperFunctionLibrary.h"
#include "NPC/UNDAntNPCEmitterDefinition.h"

AUNDAntNPCSpawner::AUNDAntNPCSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
	bAllowToSpawnLockedNPCs = true;
}

void AUNDAntNPCSpawner::HandleNPCSpawned_Implementation(AActor* InActor)
{
	Super::HandleNPCSpawned_Implementation(InActor);
	UPMTDDialogueUserComponent* DialogueUserComponent = InActor->FindComponentByClass<UPMTDDialogueUserComponent>();
	DialogueUserComponent->SetDialogueEmitterDefinition(NPCEmitterDefinition.LoadSynchronous());
}

bool AUNDAntNPCSpawner::CanSpawnNPC_Implementation() const
{
	return !NPCEmitterDefinition.IsNull() && NPCEmitterDefinition.LoadSynchronous()->IsNPCUnlocked(this) && Super::CanSpawnNPC_Implementation();
}

