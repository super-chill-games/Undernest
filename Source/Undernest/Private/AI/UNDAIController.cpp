// Copyright Super Chill Games. All Rights Reserved.


#include "AI/UNDAIController.h"

// PMTAI Includes
#include "AI/PMTAIPerceptionComponent.h"

AUNDAIController::AUNDAIController(const FObjectInitializer& InObjectInitializer) :
	Super(InObjectInitializer)
{
	PerceptionComponent = CreateDefaultSubobject<UPMTAIPerceptionComponent>(TEXT("PerceptionComponent"));
}
