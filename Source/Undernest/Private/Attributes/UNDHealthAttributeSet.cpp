﻿// Copyright Super Chill Games. All Rights Reserved.


#include "Attributes/UNDHealthAttributeSet.h"

// UE Includes
#include "Net/UnrealNetwork.h"

UUNDHealthAttributeSet::UUNDHealthAttributeSet()
{
	InitFungiHealth(0.0f);
	InitMaxFungiHealth(0.0f);
	InitFungiRecoveryPerSecond(0.0f);
	InitHealthStunRecoverInSeconds(0.0f);
	InitHealthRecoverAfterStun(0.0f);
	InitMaxFungiDamageAllowedPerStunState(0.0f);

	FungiHealth.MaxAttribute = GetMaxFungiHealthAttribute();
	FungiHealth.ValueAttribute = GetFungiHealthAttribute();

	MaxFungiHealth.ValueAttribute = FungiHealth.ValueAttribute;
	MaxFungiHealth.MaxAttribute = FungiHealth.MaxAttribute;
}

void UUNDHealthAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(UUNDHealthAttributeSet, FungiHealth, Params);
	DOREPLIFETIME_WITH_PARAMS_FAST(UUNDHealthAttributeSet, MaxFungiHealth, Params);
	DOREPLIFETIME_WITH_PARAMS_FAST(UUNDHealthAttributeSet, FungiRecoveryPerSecond, Params);
}

void UUNDHealthAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);
	if (Attribute == GetFungiHealthAttribute())
	{
		NewValue = FMath::Clamp(NewValue, 0.0f, GetMaxFungiHealth());
	}
}

void UUNDHealthAttributeSet::PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const
{
	Super::PreAttributeBaseChange(Attribute, NewValue);
	if (Attribute == GetFungiHealthAttribute())
	{
		NewValue = FMath::Clamp(NewValue, 0.0f, GetMaxFungiHealth());
	}
}

void UUNDHealthAttributeSet::OnRep_FungiHealth(const FPMTCHitGameplayAttributeData& OldValue)
{
	PMTC_GAMEPLAYATTRIBUTE_NOTIFY(UUNDHealthAttributeSet, FungiHealth, OldValue);
}

void UUNDHealthAttributeSet::OnRep_MaxFungiHealth(const FPMTCGameplayAttributeData& OldValue)
{
	PMTC_GAMEPLAYATTRIBUTE_NOTIFY(UUNDHealthAttributeSet, MaxFungiHealth, OldValue);
}

void UUNDHealthAttributeSet::OnRep_FungiRecoveryPerSecond(const FPMTCGameplayAttributeData& OldValue)
{
	PMTC_GAMEPLAYATTRIBUTE_NOTIFY(UUNDHealthAttributeSet, FungiRecoveryPerSecond, OldValue);
}
