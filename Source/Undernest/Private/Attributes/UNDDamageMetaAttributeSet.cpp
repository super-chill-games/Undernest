﻿// Copyright Super Chill Games. All Rights Reserved.


#include "Attributes/UNDDamageMetaAttributeSet.h"

UUNDDamageMetaAttributeSet::UUNDDamageMetaAttributeSet()
{
	InitMetaFungiDamage(0.0f);
	InitAccumulatedFungiDamage(0.0f);
}
