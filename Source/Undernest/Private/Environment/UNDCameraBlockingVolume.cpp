﻿// Copyright Super Chill Games. All Rights Reserved.


#include "Environment/UNDCameraBlockingVolume.h"

#include "Components/BrushComponent.h"

AUNDCameraBlockingVolume::AUNDCameraBlockingVolume()
{
	PrimaryActorTick.bCanEverTick = false;
	GetBrushComponent()->SetCollisionProfileName(FName(TEXT("CameraBlock")));
	GetBrushComponent()->SetGenerateOverlapEvents(false);
}
