// Copyright Super Chill Games. All Rights Reserved.


#include "Levels/UNDLevelDefinition.h"

FText UUNDLevelDefinition::GetLevelName_Implementation() const
{
	return LevelName;
}

FText UUNDLevelDefinition::GetLevelDescription_Implementation() const
{
	return LevelDescription;
}
