// Copyright Super Chill Games. All Rights Reserved.


#include "Levels/UNDLevelGenerator.h"

// UND Includes
#include "Abilities/PMTCAbilitySystemComponent.h"
#include "Attributes/UNDHealthAttributeSet.h"
#include "FunctionLibraries/PMTCCommonHelperFunctionLibrary.h"
#include "Interfaces/PMTCAbilitySystemInterface.h"
#include "Levels/UNDLevelDefinition.h"
#include "Telemetry/PMTCTelemetryTypes.h"
#include "Telemetry/PMTCTelemetryWorldSubsystem.h"
#include "Types/UNDNativeTags.h"

DEFINE_LOG_CATEGORY(UNDLevelGeneratorLog);

UUNDLevelGenerator::UUNDLevelGenerator()
{
	RestAreaPerPlayedLevel = FInt32Range::Inclusive(3, 3);
	BossAreaPerPlayedLevel = FInt32Range::Inclusive(7, 7);
	FinalAreaPerPlayedLevel = FInt32Range::Inclusive(7, 7);
	ExteriorLevels = FInt32Range::Inclusive(2, 2);

	CurrentArea = UNDNativeTags::AreaForest;
}

FGameplayTag UUNDLevelGenerator::GetCurrentLevelAreaType(const UObject* WorldContextObject)
{
	if (WorldContextObject)
	{
		if (UWorld* World = WorldContextObject->GetWorld())
		{
			return World->GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>()->GetLevelGenerator()->GetCurrentArea();
		}
	}
	return FGameplayTag();
}

TSubclassOf<UUNDLevelDefinition> UUNDLevelGenerator::GetCurrentLevelDefinition(const UObject* WorldContextObject)
{
	if (WorldContextObject)
	{
		if (UWorld* World = WorldContextObject->GetWorld())
		{
			UUNDLevelGenerator* LevelGenerator = World->GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>()->GetLevelGenerator();
			TSoftObjectPtr<UWorld> SoftWorld(World);
			for (TSubclassOf<UUNDLevelDefinition> LevelDef : LevelGenerator->Levels)
			{
				if (LevelDef.GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
				{
					return LevelDef;
				}
			}

			if (LevelGenerator->GetHubLevel() && LevelGenerator->GetHubLevel().GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
			{
				return LevelGenerator->GetHubLevel();
			}

			if (LevelGenerator->GetTutorialLevel() && LevelGenerator->GetTutorialLevel().GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
			{
				return LevelGenerator->GetTutorialLevel();
			}

			if (LevelGenerator->GetWorldSelectorLevel() && LevelGenerator->GetWorldSelectorLevel().GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
			{
				return LevelGenerator->GetWorldSelectorLevel();
			}

			if (LevelGenerator->GetLaboratoryLevel() && LevelGenerator->GetLaboratoryLevel().GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
			{
				return LevelGenerator->GetLaboratoryLevel();
			}
		}
	}
	return TSubclassOf<UUNDLevelDefinition>();
}

void UUNDLevelGenerator::ResetLevelGenerator_Implementation()
{
	PlayedLevels.Reset();
	CurrentArea = FGameplayTag();
	ResetExteriorLevels();
}

void UUNDLevelGenerator::InitializeLevelGenerator_Implementation()
{

}

void UUNDLevelGenerator::ResetExteriorLevels()
{
	PlayedLevels.RemoveAllSwap([](TSubclassOf<UUNDLevelDefinition> Element)
	{
		return Element.GetDefaultObject()->GetDescriptorTags().HasTag(UNDNativeTags::LevelDescriptorExterior);
	});
}

void UUNDLevelGenerator::SetCurrentArea(const FGameplayTag& InArea)
{
	CurrentArea = InArea;
}

void UUNDLevelGenerator::MarkLevelAsPlayed(UWorld& World)
{
#if WITH_EDITOR
	// Every other level is added automatically while traveling.
	if (World.IsPlayInEditor() && PlayedLevels.Num() == 0)
	{
		TSoftObjectPtr<UWorld> SoftWorld(&World);
		for (TSubclassOf<UUNDLevelDefinition> LevelDef : Levels)
		{
			if (LevelDef.GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
			{
				if (!LevelDef.GetDefaultObject()->GetDescriptorTags().HasTag(UNDNativeTags::LevelDescriptorDontSave))
				{
					CurrentArea = UPMTCCommonHelperFunctionLibrary::GetTagFromContainer(LevelDef.GetDefaultObject()->GetDescriptorTags(), UNDNativeTags::Area);
					PlayedLevels.Add(LevelDef);
				}
				break;
			}
		}
	}
#endif // WITH_EDITOR
}

bool UUNDLevelGenerator::TravelToLevel(UObject* WorldContextObject, TSubclassOf<UUNDLevelDefinition> InLevelDefinition, float Delay, bool bTravelAnywayIfRepeated)
{
	if (!WorldContextObject || !InLevelDefinition || !WorldContextObject->GetWorld())
	{
		return false;
	}

	if (PlayedLevels.Contains(InLevelDefinition) && !bTravelAnywayIfRepeated)
	{
		return false;
	}

	if (Delay > 0.0f)
	{
		TWeakObjectPtr<UObject> WeakContext(WorldContextObject);
		FTimerHandle TimerHandle;
		FTimerDelegate Delegate = FTimerDelegate::CreateWeakLambda(this, [this, InLevelDefinition, WeakContext]()
		{
			TravelToLevel(WeakContext.Get(), InLevelDefinition, 0.0f);
		});
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, Delegate, Delay, false);
		return true;
	}

	const FGameplayTagContainer& LevelTags = InLevelDefinition.GetDefaultObject()->GetDescriptorTags();
	if (!LevelTags.HasTag(UNDNativeTags::LevelDescriptorDontSave))
	{
		// We use AddUnique in case bTravelAnywayIfRepeated is true.
		PlayedLevels.AddUnique(InLevelDefinition);
	}

#if PMT_ENABLE_TELEMETRY
	if (const APawn* Pawn = WorldContextObject->GetWorld()->GetFirstPlayerController()->GetPawn())
	{
		if (Pawn->GetClass()->ImplementsInterface(UPMTCTelemetryInterface::StaticClass()))
		{
			if (UPMTCTelemetryWorldSubsystem* TelemetryWorldSubsystem = WorldContextObject->GetWorld()->GetSubsystem<UPMTCTelemetryWorldSubsystem>())
			{
				FPMTCTelemetryEvent TelemetryEvent;
				TelemetryEvent.EventCategory = TEXT("LevelTravel");
				TelemetryEvent.EventName = TEXT("AreaEnd");
				TelemetryEvent.EventOwner = TEXT("Player");
				UPMTCTelemetryWorldSubsystem::AddStringValue(TelemetryEvent.Values, TEXT("destination"), InLevelDefinition->GetName());
				IPMTCTelemetryInterface::Execute_DescribeTelemetryValues(Pawn, TelemetryEvent);
				TelemetryWorldSubsystem->RecordTelemetryEvent(TelemetryEvent);
			}
		}
	}
#endif // PMT_ENABLE_TELEMETRY
	return WorldContextObject->GetWorld()->ServerTravel(InLevelDefinition.GetDefaultObject()->GetLevel().GetLongPackageName(), false);
}

TArray<TSubclassOf<UUNDLevelDefinition>> UUNDLevelGenerator::GetPlayedLevelsWithAnyTags(const FGameplayTagContainer& Tags) const
{
	return FilterLevelsWithAnyTags(PlayedLevels, Tags);
}

TSubclassOf<UUNDLevelDefinition> UUNDLevelGenerator::TryGenerateNextAreaToPlay(FRandomStream Stream) const
{
	if (UWorld* World = GetWorld())
	{
		TSoftObjectPtr<UWorld> SoftWorld(World);
		for (TSubclassOf<UUNDLevelDefinition> LevelDef : Levels)
		{
			if (LevelDef.GetDefaultObject()->GetLevel().GetAssetName() == SoftWorld.GetAssetName())
			{
				if (LevelDef.GetDefaultObject()->GetDescriptorTags().HasTag(UNDNativeTags::AreaExterior))
				{
					return TryGenerateNextExteriorAreaToPlay(Stream);
				}
				break;
			}
		}
	}
	TArray<TSubclassOf<UUNDLevelDefinition>> AvailableAreas =
		FilterLevelsWithTags(Levels, FGameplayTagContainer(CurrentArea));
	AvailableAreas = FilterPlayedLevels(AvailableAreas);

	const TArray<TSubclassOf<UUNDLevelDefinition>> CurrentAreaPlayedLevels =
		FilterLevelsWithTags(PlayedLevels, FGameplayTagContainer(CurrentArea));

	// 1. Check if we should jump to the final area instead.
	const int32 RandFinalArea = Stream.RandRange(FinalAreaPerPlayedLevel.GetLowerBoundValue(),
		FinalAreaPerPlayedLevel.GetUpperBoundValue());

	// We should load a final area.
	if (CurrentAreaPlayedLevels.Num() >= RandFinalArea)
	{
		TArray<TSubclassOf<UUNDLevelDefinition>> FinalAreas =
			FilterLevelsWithTags(AvailableAreas, FGameplayTagContainer(UNDNativeTags::LevelDescriptorFinalArea));
		if (FinalAreas.Num() == 0)
		{
			UE_LOG(UNDLevelGeneratorLog, Error, TEXT("'%s': No final area found for area '%s'"), ANSI_TO_TCHAR(__FUNCTION__), *CurrentArea.ToString());
			return TSubclassOf<UUNDLevelDefinition>();
		}
		const int32 RandomIndex = Stream.RandRange(0, FinalAreas.Num() - 1);
		return FinalAreas[RandomIndex];
	}

	// 2. Check if we should jump to a rest area.
	if (CurrentAreaPlayedLevels.Num() > 0)
	{
		const UUNDLevelDefinition* LastLevel = CurrentAreaPlayedLevels.Last().GetDefaultObject();
		if (LastLevel->GetDescriptorTags().HasTag(UNDNativeTags::LevelDescriptorBoss) && bForceRestAreaAfterBoss)
		{
			TArray<TSubclassOf<UUNDLevelDefinition>> RestAreas =
				FilterLevelsWithTags(AvailableAreas, FGameplayTagContainer(UNDNativeTags::LevelDescriptorRest));
			bool bCanSelectAnyRestArea = RestAreas.Num() == 0;
			if (!bCanSelectAnyRestArea)
			{
				if (CanReuseRestAreas())
				{
					UE_LOG(UNDLevelGeneratorLog, Warning, TEXT("'%s': No more unused rest areas were found. Will try to use an old one."), ANSI_TO_TCHAR(__FUNCTION__));
					FGameplayTagContainer Tags(CurrentArea);
					Tags.AddTag(UNDNativeTags::LevelDescriptorRest);
					RestAreas = FilterLevelsWithTags(Levels, Tags);
					if (RestAreas.Num() == 0)
					{
						UE_LOG(UNDLevelGeneratorLog, Warning, TEXT("'%s': There's no rest area available at all. Will try to select a combat or boss one."), ANSI_TO_TCHAR(__FUNCTION__));
					}
					else
					{
						bCanSelectAnyRestArea = true;
					}
				}
				else
				{
					UE_LOG(UNDLevelGeneratorLog, Warning, TEXT("'%s': No more unused rest areas were found. Will keep with boss or combat areas."), ANSI_TO_TCHAR(__FUNCTION__));
				}
			}

			if (bCanSelectAnyRestArea)
			{
				TSubclassOf<UUNDLevelDefinition> RestArea;
				if (bRandomizeRestAreas)
				{
					const int32 RandomRestAreaIndex = Stream.RandRange(0, RestAreas.Num() - 1);
					RestArea = RestAreas[RandomRestAreaIndex];
				}
				else
				{
					RestArea = RestAreas[0];
				}
				return RestArea;
			}
		}
		// See if we played enough levels from the last rest area to see if we should generate a rest area.
		else
		{
			const int32 RandRestArea = Stream.RandRange(RestAreaPerPlayedLevel.GetLowerBoundValue(),
				RestAreaPerPlayedLevel.GetUpperBoundValue());
			int32 PlayedAreasSinceLastRestArea = 0;
			for (int32 Index = CurrentAreaPlayedLevels.Num() - 1; Index >= 0; --Index)
			{
				const UUNDLevelDefinition* LevelDefinition = CurrentAreaPlayedLevels[Index].GetDefaultObject();
				if (!LevelDefinition->GetDescriptorTags().HasTag(UNDNativeTags::LevelDescriptorRest))
				{
					++PlayedAreasSinceLastRestArea;
				}
				else
				{
					break;
				}
			}

			if (PlayedAreasSinceLastRestArea >= RandRestArea)
			{
				TArray<TSubclassOf<UUNDLevelDefinition>> RestAreas =
					FilterLevelsWithTags(AvailableAreas, FGameplayTagContainer(UNDNativeTags::LevelDescriptorRest));
				bool bCanSelectAnyRestArea = RestAreas.Num() > 0;
				if (!bCanSelectAnyRestArea)
				{
					if (bCanReuseRestAreas)
					{
						FGameplayTagContainer Tags(CurrentArea);
						Tags.AddTag(UNDNativeTags::LevelDescriptorRest);
						RestAreas = FilterLevelsWithTags(AvailableAreas, Tags);
						bCanSelectAnyRestArea = RestAreas.Num() > 0;
					}
					else
					{
						UE_LOG(UNDLevelGeneratorLog, Warning, TEXT("'%s': There's no rest area available at all. Will try to select a combat or boss one."), ANSI_TO_TCHAR(__FUNCTION__));
					}
				}

				if (bCanSelectAnyRestArea)
				{
					TSubclassOf<UUNDLevelDefinition> RestArea;
					if (bRandomizeRestAreas)
					{
						const int32 RandomRestAreaIndex = Stream.RandRange(0, RestAreas.Num() - 1);
						RestArea = RestAreas[RandomRestAreaIndex];
					}
					else
					{
						RestArea = RestAreas[0];
					}
					return RestArea;
				}
			}
		}
	}

	// 3. Try to generate a boss area.
	if (CurrentAreaPlayedLevels.Num() > 0)
	{
		const int32 RandBossAreaNumber = Stream.RandRange(BossAreaPerPlayedLevel.GetLowerBoundValue(),
			BossAreaPerPlayedLevel.GetUpperBoundValue());
		
		int32 PlayedAreasSinceLastBossArea = 0;
		for (int32 Index = CurrentAreaPlayedLevels.Num() - 1; Index >= 0; --Index)
		{
			const UUNDLevelDefinition* LevelDefinition = CurrentAreaPlayedLevels[Index].GetDefaultObject();
			if (!LevelDefinition->GetDescriptorTags().HasTag(UNDNativeTags::LevelDescriptorBoss))
			{
				++PlayedAreasSinceLastBossArea;
			}
			else
			{
				break;
			}
		}

		if (PlayedAreasSinceLastBossArea >= RandBossAreaNumber)
		{
			TArray<TSubclassOf<UUNDLevelDefinition>> BossAreas =
				FilterLevelsWithTags(AvailableAreas, FGameplayTagContainer(UNDNativeTags::LevelDescriptorBoss));
			if (BossAreas.Num() > 0)
			{
				return BossAreas[0];
			}
		}
	}

	// 4. Finally, just generate a standard combat area.
	TArray<TSubclassOf<UUNDLevelDefinition>> CombatAreas =
		FilterLevelsWithTags(AvailableAreas, FGameplayTagContainer(UNDNativeTags::LevelDescriptorCombat));
	if (CombatAreas.Num() > 0)
	{
		int32 Index = 0;
		if (bRandomizeCombatAreas)
		{
			Index = Stream.RandRange(0, CombatAreas.Num() - 1);
		}
		return CombatAreas[Index];
	}

	// 5. If nothing found, then just grab the first one and choose that.
	if (AvailableAreas.Num() > 0)
	{
		return AvailableAreas[0];
	}
	return TSubclassOf<UUNDLevelDefinition>();
}

TSubclassOf<UUNDLevelDefinition> UUNDLevelGenerator::TryGenerateNextExteriorAreaToPlay(FRandomStream Stream) const
{
	TArray<TSubclassOf<UUNDLevelDefinition>> AvailableAreas =
		FilterLevelsWithTags(Levels, FGameplayTagContainer(CurrentArea));
	AvailableAreas = FilterPlayedLevels(AvailableAreas);

	const TArray<TSubclassOf<UUNDLevelDefinition>> CurrentAreaPlayedLevels =
		FilterLevelsWithTags(PlayedLevels, FGameplayTagContainer(CurrentArea));
	
	const int32 RandExteriorLevel = Stream.RandRange(ExteriorLevels.GetLowerBoundValue(),
		ExteriorLevels.GetUpperBoundValue());

	// Load world selector.
	if (CurrentAreaPlayedLevels.Num() >= RandExteriorLevel || AvailableAreas.Num() == 0)
	{
		return WorldSelectorLevel;
	}

	return AvailableAreas[0];
}

bool UUNDLevelGenerator::CanReuseRestAreas_Implementation() const
{
	return bCanReuseRestAreas;
}

TArray<TSubclassOf<UUNDLevelDefinition>> UUNDLevelGenerator::FilterLevelsWithTags(
	const TArray<TSubclassOf<UUNDLevelDefinition>>& InLevelsToFilter, const FGameplayTagContainer& Tags) const
{
	return InLevelsToFilter.FilterByPredicate([&Tags](TSubclassOf<UUNDLevelDefinition> Level)
	{
		return Level.GetDefaultObject()->GetDescriptorTags().HasAll(Tags);
	});
}

TArray<TSubclassOf<UUNDLevelDefinition>> UUNDLevelGenerator::FilterLevelsWithAnyTags(
	const TArray<TSubclassOf<UUNDLevelDefinition>>& InLevelsToFilter, const FGameplayTagContainer& Tags) const
{
	return InLevelsToFilter.FilterByPredicate([&Tags](TSubclassOf<UUNDLevelDefinition> Level)
	{
		return Level.GetDefaultObject()->GetDescriptorTags().HasAny(Tags);
	});
}

TArray<TSubclassOf<UUNDLevelDefinition>> UUNDLevelGenerator::FilterPlayedLevels(
	const TArray<TSubclassOf<UUNDLevelDefinition>>& InLevelsToFilter) const
{
	return InLevelsToFilter.FilterByPredicate([this](TSubclassOf<UUNDLevelDefinition> Level)
	{
		return !PlayedLevels.Contains(Level);
	});
}

UUNDLevelGenerationGISubsystem::UUNDLevelGenerationGISubsystem()
{
	LevelGeneratorClass = UUNDLevelGenerator::StaticClass();
}

void UUNDLevelGenerationGISubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	TSubclassOf<UUNDLevelGenerator> GeneratorClass = LevelGeneratorClass.IsValid() ?
		LoadClass<UUNDLevelGenerator>(NULL, *LevelGeneratorClass.ToString(), NULL, LOAD_None, NULL) : UUNDLevelGenerator::StaticClass();
	LevelGenerator = NewObject<UUNDLevelGenerator>(this, GeneratorClass);
	LevelGenerator->InitializeLevelGenerator();
}

void UUNDLevelGenerationGISubsystem::ResetLevelGenerationToDefaults()
{
	if (LevelGenerator)
	{
		LevelGenerator->ResetLevelGenerator();
	}
}
