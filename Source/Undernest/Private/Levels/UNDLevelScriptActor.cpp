// Copyright Super Chill Games. All Rights Reserved.


#include "Levels/UNDLevelScriptActor.h"

// UND Includes
#include "Corruption/UNDCorruptionManager.h"
#include "Levels/UNDLevelGenerator.h"
#include "Subsystems/UNDCorruptionWorldSubsystem.h"
#include "Subsystems/UNDGameFlowWorldSubsystem.h"
#include "Types/UNDNativeTags.h"

// UE Includes
#include "EngineUtils.h"
#include "LevelSequenceActor.h"
#include "LevelSequencePlayer.h"

AUNDLevelScriptActor::AUNDLevelScriptActor()
{
	InitialCorruptionState = UNDNativeTags::CorruptionStateCorrupted;
	GameFlowLevelLoadEvent = UNDNativeTags::GameFlowLevelLoaded;
}

void AUNDLevelScriptActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

#if !UE_BUILD_SHIPPING
	if (!GetWorld() || !GetWorld()->IsGameWorld())
	{
		return;
	}
#endif // !UE_BUILD_SHIPPING

#if WITH_EDITOR
	if (GetWorld()->IsPlayInEditor())
	{
		GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>()->GetLevelGenerator()->MarkLevelAsPlayed(*GetWorld());
	}
#endif // WITH_EDITOR

	if (InitialCorruptionState.IsValid())
	{
		if (UUNDCorruptionWorldSubsystem* CorruptionWorldSS = GetWorld()->GetSubsystem<UUNDCorruptionWorldSubsystem>())
		{
			CorruptionWorldSS->GetCorruptionManager()->SetCorruptionState(InitialCorruptionState);
		}
	}
}

void AUNDLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetSubsystem<UUNDGameFlowWorldSubsystem>()->NotifyGameFlowEvent(GameFlowLevelLoadEvent, false ,this);

	if (bPlayLevelSequenceOnBeginPlay)
	{
		if (ShouldPlayIntroSequence())
		{
			RequestPlayIntroLevelSequence();
		}
	}
}

void AUNDLevelScriptActor::RequestPlayIntroLevelSequence()
{
	if (ALevelSequenceActor* LevelSequence = GetIntroLevelSequence())
	{
		UUNDGameFlowWorldSubsystem* GameFlowSS = GetWorld()->GetSubsystem<UUNDGameFlowWorldSubsystem>();
		GameFlowSS->NotifyGameFlowEvent(UNDNativeTags::GameFlowLevelSequenceStarted, false, this);

		LevelSequence->SequencePlayer->OnFinished.AddDynamic(this, &AUNDLevelScriptActor::HandleIntroLevelSequenceFinished);
		LevelSequence->SequencePlayer->Play();
	}
}

ALevelSequenceActor* AUNDLevelScriptActor::GetIntroLevelSequence_Implementation() const
{
	for (TActorIterator<ALevelSequenceActor> It(GetWorld()); It; ++It)
	{
		return *It;
	}
	return nullptr;
}

bool AUNDLevelScriptActor::ShouldPlayIntroSequence_Implementation() const
{
	return GetIntroLevelSequence() != nullptr;
}

void AUNDLevelScriptActor::HandleIntroLevelSequenceFinished_Implementation()
{
	UUNDGameFlowWorldSubsystem* GameFlowSS = GetWorld()->GetSubsystem<UUNDGameFlowWorldSubsystem>();
	GameFlowSS->NotifyGameFlowEvent(UNDNativeTags::GameFlowLevelSequenceFinished, false, this);
}
