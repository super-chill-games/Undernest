// Copyright Super Chill Games. All Rights Reserved.


#include "Interfaces/UNDCorruptionInterface.h"

// UND Includes
#include "Corruption/UNDCorruptionManager.h"
#include "Subsystems/UNDCorruptionWorldSubsystem.h"
#include "Types/UNDNativeTags.h"

FGameplayTag IUNDCorruptionInterface::GetCorruptionState_Implementation() const
{
	if (const AActor* ThisAsActor = Cast<const AActor>(this))
	{
		if (UWorld* World = ThisAsActor->GetWorld())
		{
			World->GetSubsystem<UUNDCorruptionWorldSubsystem>()->GetCorruptionManager()->GetCorruptionState();
		}
	}
	return FGameplayTag();
}

bool IUNDCorruptionInterface::IsCorrupted() const
{
	const UObject* ThisAsObject = Cast<const UObject>(this);
	return IUNDCorruptionInterface::Execute_GetCorruptionState(ThisAsObject) == UNDNativeTags::CorruptionStateCorrupted;
}
