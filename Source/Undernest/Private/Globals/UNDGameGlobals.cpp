// Copyright Super Chill Games. All Rights Reserved.


#include "Globals/UNDGameGlobals.h"

// PMTNPC Includes
#include "Subsystems/PMTNPCTrackNPCGISubsystem.h"

// UND Includes
#include "Globals/UNDGameInstance.h"
#include "Levels/UNDLevelGenerator.h"

// UE Includes
#include "GameFramework/GameMode.h"
#include "Kismet/GameplayStatics.h"

UUNDGameGlobals::UUNDGameGlobals()
{

}

UUNDGameGlobals* UUNDGameGlobals::GetGameGlobals(UObject* WorldContextObject)
{
	if (::IsValid(WorldContextObject))
	{
		if (UWorld* World = WorldContextObject->GetWorld())
		{
			// TODO(Brian): This is only null while we are migrating to C++.
			if (UUNDGameInstance* GameInstance = World->GetGameInstance<UUNDGameInstance>())
			{
				return GameInstance->GetGameGlobals();
			}
		}
	}
	return nullptr;
}

bool UUNDGameGlobals::AreWeInHUBWorld(UObject* WorldContextObject)
{
	if (UUNDGameGlobals* GameGlobals = GetGameGlobals(WorldContextObject))
	{
		TSoftObjectPtr<UWorld> SoftWorld(WorldContextObject->GetWorld());
		return GameGlobals->HubLevel.GetAssetName() == SoftWorld.GetAssetName();
	}
	return false;
}

bool UUNDGameGlobals::AreWeInLaboratoryWorld(UObject* WorldContextObject)
{
	if (UUNDGameGlobals* GameGlobals = GetGameGlobals(WorldContextObject))
	{
		TSoftObjectPtr<UWorld> SoftWorld(WorldContextObject->GetWorld());
		return GameGlobals->LaboratoryLevel.GetAssetName() == SoftWorld.GetAssetName();
	}
	return false;
}

void UUNDGameGlobals::EnableDisableInputAndChangeTimeDilationForCharacter(
	UObject* WorldContextObject, bool bEnableInput, float TimeDilation, int32 PlayerIndex)
{
	if (WorldContextObject)
	{
		if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(WorldContextObject, PlayerIndex))
		{
			if (bEnableInput)
			{
				PlayerController->EnableInput(PlayerController);
			}
			else
			{
				PlayerController->DisableInput(PlayerController);
			}

			if (APawn* Pawn = PlayerController->GetPawn())
			{
				if (bEnableInput)
				{
					Pawn->EnableInput(PlayerController);
				}
				else
				{
					Pawn->DisableInput(PlayerController);
				}
				Pawn->CustomTimeDilation = TimeDilation;
			}
		}
	}
}

bool UUNDGameGlobals::TravelToHUB(bool bAbsolute)
{
	if (UWorld* World = GetWorld())
	{
		return World->ServerTravel(HubLevel.GetLongPackageName(), bAbsolute);
	}
	return false;
}

bool UUNDGameGlobals::TravelToLaboratory(bool bAbsolute, bool bFromDeath)
{
	if (UWorld* World = GetWorld())
	{
		if (bAbsolute)
		{
			// TODO(Brian): I think it would be better to have an interface that automatically resets
			// stuff.
			if (!bPersistDataOnTravelToLaboratoryAbsolute)
			{
				World->GetGameInstance()->GetSubsystem<UPMTNPCTrackNPCGISubsystem>()->ResetNPCToDefault();
				World->GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>()->ResetLevelGenerationToDefaults();
			}
			if (AGameMode* GameMode = Cast<AGameMode>(World->GetAuthGameMode()))
			{
				// For the moment, disable this so everything can be reset while opening the laboratory, otherwise
				// characters and all that will travel to the laboratory.
				const bool bWasUsingSeamlessTravel = GameMode->bUseSeamlessTravel;
				GameMode->bUseSeamlessTravel = false;
				const bool bCouldTravel = World->ServerTravel(LaboratoryLevel.GetLongPackageName(), bAbsolute);
				GameMode->bUseSeamlessTravel = bWasUsingSeamlessTravel;
				return bCouldTravel;
			}
		}
		else
		{
			return World->ServerTravel(LaboratoryLevel.GetLongPackageName(), bAbsolute);
		}
	}
	return false;
}

bool UUNDGameGlobals::TravelToTutorial()
{
	if (UWorld* World = GetWorld())
	{
		World->GetGameInstance()->GetSubsystem<UPMTNPCTrackNPCGISubsystem>()->ResetNPCToDefault();
		World->GetGameInstance()->GetSubsystem<UUNDLevelGenerationGISubsystem>()->ResetLevelGenerationToDefaults();

		if (AGameMode* GameMode = Cast<AGameMode>(World->GetAuthGameMode()))
		{
			// For the moment, disable this so everything can be reset while opening the tutorial, otherwise
			// characters and all that will travel to the HUB.
			const bool bWasUsingSeamlessTravel = GameMode->bUseSeamlessTravel;
			GameMode->bUseSeamlessTravel = false;
			const bool bCouldTravel = World->ServerTravel(TutorialLevel.GetLongPackageName(), true);
			GameMode->bUseSeamlessTravel = bWasUsingSeamlessTravel;
			return bCouldTravel;
		}
	}
	return false;
}

bool UUNDGameGlobals::TravelToMenu()
{
	if (UWorld* World = GetWorld())
	{
		if (AGameMode* GameMode = Cast<AGameMode>(World->GetAuthGameMode()))
		{
			// For the moment, disable this so everything can be reset while opening the tutorial, otherwise
			// characters and all that will travel to the HUB.
			const bool bWasUsingSeamlessTravel = GameMode->bUseSeamlessTravel;
			GameMode->bUseSeamlessTravel = false;
			const bool bCouldTravel = World->ServerTravel(MenuLevel.GetLongPackageName(), true);
			GameMode->bUseSeamlessTravel = bWasUsingSeamlessTravel;
			return bCouldTravel;
		}
	}
	return false;
}
