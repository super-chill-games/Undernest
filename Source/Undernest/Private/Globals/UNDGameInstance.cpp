// Copyright Super Chill Games. All Rights Reserved.


#include "Globals/UNDGameInstance.h"

// PMTS Includes
#include "Settings/PMTSSettingsConfig.h"

// UND Includes
#include "Globals/UNDGameGlobals.h"
#include "Globals/UNDPersistentData.h"

// UE Includes
#include "AbilitySystemGlobals.h"

UUNDGameInstance::UUNDGameInstance()
{
	GameGlobalsClass = UUNDGameGlobals::StaticClass();
	PersistentDataClass = UUNDPersistentData::StaticClass();
}

void UUNDGameInstance::Init()
{
	Super::Init();
	UAbilitySystemGlobals::Get().InitGlobalData();

	GameGlobals = NewObject<UUNDGameGlobals>(this, GameGlobalsClass);
	PersistentData = NewObject<UUNDPersistentData>(this, PersistentDataClass);

	UPMTSSettingsConfig::GetSettingsConfig()->InitializeSettingsConfig(this);
	UPMTSSettingsConfig::GetSettingsConfig()->ApplySettings();
}

UUNDPersistentData* UUNDGameInstance::GetPersistentData() const
{
	return PersistentData;
}
