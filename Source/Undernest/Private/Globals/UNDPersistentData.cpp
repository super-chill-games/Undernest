// Copyright Super Chill Games. All Rights Reserved.


#include "Globals/UNDPersistentData.h"

// PMTPR Includes
#include "ActorComponents/PMTPRPerkUserComponent.h"

// UND Includes
#include "Globals/UNDGameInstance.h"
#include "Interfaces/PMTINInventoryTrackerInterface.h"
#include "Inventory/PMTINBaseInventoryItem.h"
#include "Types/UNDNativeTags.h"
#include "UI/PMTUIMessageIntermediary.h"
#include "UI/Messages/Types/PMTUIIntMessageData.h"

UUNDPersistentData* UUNDPersistentData::GetPersistentData(const UObject* WorldContext)
{
	if (WorldContext)
	{
		if (const UWorld* World = WorldContext->GetWorld())
		{
			const UUNDGameInstance* GameInstance = World->GetGameInstanceChecked<UUNDGameInstance>();
			return GameInstance->GetPersistentData();
		}
	}
	return nullptr;
}

int32 UUNDPersistentData::TakePersistentFungiResource()
{
	const int32 CurrentValue = PersistentFungiResource;
	PersistentFungiResource = 0;
	return CurrentValue;
}

void UUNDPersistentData::HandleCharacterDeath(AActor* Character)
{
	if (bHasPlayerDied)
	{
		return;
	}
	bHasPlayerDied = true;
	PersistentFungiResource = 0;
	if (Character)
	{
		if (const UPMTPRPerkUserComponent* PerkUserComponent = Character->FindComponentByClass<UPMTPRPerkUserComponent>())
		{
			if (const UPMTPRPerkBase* Perk = PerkUserComponent->GetPerkByIdentifier(UNDNativeTags::PerkIdentifierFungiLostPerDeath))
			{
				if (IPMTINInventoryGetterInterface* InventoryGetter = Cast<IPMTINInventoryGetterInterface>(Character))
				{
					const int32 CurrentResource = InventoryGetter->GetInventoryTrackerInterface(FGameplayTag())->GetTotalQuantityForItemType(UNDNativeTags::InventoryItemQuantifiableFungiEnergy);
					const float PercentLoss = FMath::Clamp(Perk->GetPerkValue(), 0.0f, 1.0f);
					PersistentFungiResource = FMath::Floor(CurrentResource * (1.0f - PercentLoss));
				}
			}
		}
	}
}

void UUNDPersistentData::ApplyPersistentDataOnRevive(AActor* Character)
{
	if (!bHasPlayerDied)
	{
		return;
	}
	bHasPlayerDied = false;
	if (PersistentFungiResource > 0)
	{
		if (IPMTINInventoryGetterInterface* InventoryGetter = Cast<IPMTINInventoryGetterInterface>(Character))
		{
			TArray<UPMTINBaseInventoryItem*> FungiResourceItems = InventoryGetter->GetInventoryTrackerInterface(FGameplayTag())->GetAllItemsOfUniqueType(UNDNativeTags::InventoryItemQuantifiableFungiEnergy);
			if (FungiResourceItems.Num() > 0)
			{
				FungiResourceItems[0]->IncrementItemQuantity(TakePersistentFungiResource());
			}
		}
	}
}

void UUNDPersistentData::AddRescuedAnt()
{
	++PersistentRescuedAnts;
	UPMTUIMessageIntermediary::SendMessageToLocalControllers(this, UNDNativeTags::EventUIRescuedAnts,
		FPMTUIInt64MessageData(PersistentRescuedAnts));
}

int64 UUNDPersistentData::GetRescuedAnts() const
{
	return PersistentRescuedAnts;
}

bool UUNDPersistentData::WasHubLoadedAtLeastOnce() const
{
	return bHUBWorldLoadedAtLeastOnce;
}

bool UUNDPersistentData::WasTutorialLevelPlayed() const
{
	return bTutorialLevelPlayed;
}

bool UUNDPersistentData::HasPlayerDied() const
{
	return bHasPlayerDied;
}
