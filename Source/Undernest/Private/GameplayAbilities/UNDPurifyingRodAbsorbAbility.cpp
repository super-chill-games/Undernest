﻿// Copyright Super Chill Games. All Rights Reserved.


#include "GameplayAbilities/UNDPurifyingRodAbsorbAbility.h"

// PMTC Includes
#include "FunctionLibraries/PMTCCollisionHelpersFunLibrary.h"
#include "FunctionLibraries/PMTCCommonHelperFunctionLibrary.h"
#include "Interfaces/PMTCAbilitySystemInterface.h"
#include "Types/PMTCNativeTags.h"
#include "Utilities/PMTCUtilityFunctionLibrary.h"

// PMTMW Includes
#include "ActorComponents/PMTMWMeleeUserComponent.h"
#include "Interfaces/PMTMWMeleeUserInterface.h"

// UND Includes
#include "Melee/Behavior/UNDPurifyingBehaviorComponent.h"
#include "Types/UNDNativeTags.h"

// UE Includes
#include "AbilitySystemComponent.h"
#include "Abilities/Tasks/AbilityTask_WaitGameplayEvent.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "Attributes/UNDHealthAttributeSet.h"


UUNDPurifyingAbsorbAreaTask* UUNDPurifyingAbsorbAreaTask::StartPurifyTask(UGameplayAbility* OwningAbility,
                                                                          const FPMTCObjectFilter& InFilter,
                                                                          int32 InMaxAllowedTargets, const FPMTCTraceConfiguration& InAbsorbTraceConfig, FGameplayEffectSpecHandle InAbsorbGE,
                                                                          float InMaxRadius, float InInitialRadius, float InTimeToReachMaxRadius)
{
	UUNDPurifyingAbsorbAreaTask* PurifyTask = NewAbilityTask<UUNDPurifyingAbsorbAreaTask>(OwningAbility);
	PurifyTask->Filter = InFilter;
	PurifyTask->MaxAllowedTargets = InMaxAllowedTargets;
	PurifyTask->AbsorbTraceConfig = InAbsorbTraceConfig;
	PurifyTask->AbsorbGameplayEffectSpec = InAbsorbGE;
	PurifyTask->MaxRadius = InMaxRadius;
	PurifyTask->InitialRadius = InInitialRadius;
	PurifyTask->TimeToReachMaxRadius = InTimeToReachMaxRadius;
	return PurifyTask;
}

UUNDPurifyingAbsorbAreaTask::UUNDPurifyingAbsorbAreaTask()
{
	bTickingTask = true;
}

void UUNDPurifyingAbsorbAreaTask::Activate()
{
	Super::Activate();

	const UPMTMWMeleeUserComponent* MeleeUserComponent = IPMTMWMeleeUserInterface::Execute_NativeGetMeleeUserComponent(AbilitySystemComponent->GetAvatarActor());
	const APMTMWMeleeWeapon* RodWeapon = MeleeUserComponent->GetMeleeWeaponByHandle(Cast<UPMTCGameplayAbilityBase>(Ability.Get())->GetGrantHandle());
	if (UUNDPurifyingBehaviorComponent* PurifyAbsorbComponent = RodWeapon->GetBehaviorByClass<UUNDPurifyingBehaviorComponent>())
	{
		PurifyAbsorbComponent->HandleStartPurifying();
	}
}

void UUNDPurifyingAbsorbAreaTask::TickTask(float DeltaTime)
{
	Super::TickTask(DeltaTime);
	AccumulatedTime += DeltaTime;
	AbsorbGameplayEffectSpec.Data->SetSetByCallerMagnitude(PMTCNativeTags::SetByCallerDeltaTime, DeltaTime);

	const float NewRadius = TimeToReachMaxRadius > 0.0f ?
		FMath::Lerp(InitialRadius, MaxRadius, FMath::Min(AccumulatedTime / TimeToReachMaxRadius, 1.0f)) :
		MaxRadius;
	
	AbsorbTraceConfig.CapsuleOrSphereRadius = NewRadius;
	AbsorbTraceConfig.BoxExtent = AbsorbTraceConfig.BoxExtent.GetSafeNormal() * NewRadius;
	AbsorbTraceConfig.TraceStart = AbilitySystemComponent->GetAvatarActor()->GetActorLocation();
	AbsorbTraceConfig.TraceEnd = AbsorbTraceConfig.TraceStart + FVector(0.1);

	TArray<FHitResult> HitActors = AbsorbTraceConfig.MakeTrace(AbilitySystemComponent->GetAvatarActor());
	HitActors = UPMTCUtilityFunctionLibrary::RemoveDuplicateComponentHits(HitActors);
	HitActors = HitActors.FilterByPredicate([this](const FHitResult& Hit)
	{
		if (Filter.PassesFilter(*Hit.GetActor()))
		{
			if (const UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Hit.GetActor()))
			{
				return ASC->GetNumericAttribute(UUNDHealthAttributeSet::GetFungiHealthAttribute()) > 0.0f;
			}
		}
		return false;
	});
	const TArray<TObjectPtr<AActor>> ActorsNoLongerBeingAbsorbed = AbsorbedActors.FilterByPredicate([&HitActors](const TObjectPtr<AActor>& Actor)
	{
		for (const FHitResult& Hit : HitActors)
		{
			if (Hit.GetActor() == Actor)
			{
				return false;
			}
		}
		return true;
	});
	HitActors = UPMTCUtilityFunctionLibrary::RemoveDuplicateFromExistentActors(HitActors, ActorsNoLongerBeingAbsorbed);
	AbsorbedActors = AbsorbedActors.FilterByPredicate([&ActorsNoLongerBeingAbsorbed](const TObjectPtr<AActor>& Actor)
	{
		return !ActorsNoLongerBeingAbsorbed.Contains(Actor);
	});

	UPMTMWMeleeUserComponent* MeleeUserComponent = IPMTMWMeleeUserInterface::Execute_NativeGetMeleeUserComponent(AbilitySystemComponent->GetAvatarActor());
	APMTMWMeleeWeapon* RodWeapon = MeleeUserComponent->GetMeleeWeaponByHandle(Cast<UPMTCGameplayAbilityBase>(Ability.Get())->GetGrantHandle());
	UUNDPurifyingBehaviorComponent* PurifyAbsorbComponent = RodWeapon->GetBehaviorByClass<UUNDPurifyingBehaviorComponent>();

	// 1. Add new actors.
	TArray<TObjectPtr<AActor>> NewActors;
	while (AbsorbedActors.Num() < MaxAllowedTargets && HitActors.Num() > 0)
	{
		AActor* NewActor = HitActors.Pop().GetActor();
		if (UPMTCAbilitySystemFunctionLibrary::CanEverHaveAnASC(NewActor) && !AbsorbedActors.Contains(NewActor))
		{
			AbsorbedActors.Add(NewActor);
			NewActors.Add(NewActor);
		}
	}
	// 2. Apply hits and invoke cues for new actors.
	if (AbilitySystemComponent->IsOwnerActorAuthoritative())
	{
		TArray<TObjectPtr<AActor>> CopyAbsorbedActors = AbsorbedActors;
		for (const TObjectPtr<AActor>& Actor : CopyAbsorbedActors)
		{
			if (UAbilitySystemComponent* HitASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Actor))
			{
				AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*AbsorbGameplayEffectSpec.Data, HitASC);
				const bool bIsHitASCDestroyed = !HitASC || HitASC->GetOwner()->IsActorBeingDestroyed() || HitASC->HasMatchingGameplayTag(PMTCNativeTags::StateDead);
				const bool bIsNewActor = NewActors.Contains(Actor);
				if (!bIsHitASCDestroyed && bIsNewActor)
				{
					FGameplayCueParameters CueParameters;
					CueParameters.Instigator = HitASC->GetAvatarActor();
					CueParameters.EffectCauser = AbilitySystemComponent->GetAvatarActor();
					CueParameters.EffectContext = AbsorbGameplayEffectSpec.Data->GetEffectContext();
					HitASC->InvokeGameplayCueEvent(UNDNativeTags::GameplayCueAbsorbFungiRod,
						EGameplayCueEvent::WhileActive, CueParameters);
					PurifyAbsorbComponent->AddAffectedActor(Actor);
				}
				else if (bIsHitASCDestroyed && !bIsNewActor && HitASC)
				{
					FGameplayCueParameters CueParameters;
					HitASC->InvokeGameplayCueEvent(UNDNativeTags::GameplayCueAbsorbFungiRod, EGameplayCueEvent::Removed, CueParameters);
					PurifyAbsorbComponent->RemoveAffectedActor(Actor);
				}
			}
		}
	}

	// 3. Remove cues for those actors that are no longer being absorbed.
	for (const TObjectPtr<AActor>& Actor : ActorsNoLongerBeingAbsorbed)
	{
		// Can be null if actor was destroyed.
		if (UAbilitySystemComponent* HitASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Actor))
		{
			FGameplayCueParameters CueParameters;
			HitASC->InvokeGameplayCueEvent(UNDNativeTags::GameplayCueAbsorbFungiRod, EGameplayCueEvent::Removed, CueParameters);
		}
		PurifyAbsorbComponent->RemoveAffectedActor(Actor);
	}

	// 4. Remove any null or destroyed ASC.
	AbsorbedActors.RemoveAllSwap([](const TObjectPtr<AActor>& Actor)
	{
		if (!Actor || Actor->IsActorBeingDestroyed())
		{
			return true;
		}
		if (const UAbilitySystemComponent* ASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Actor))
		{
			return ASC->HasMatchingGameplayTag(PMTCNativeTags::StateDead);
		}
		return true;
	});
}

void UUNDPurifyingAbsorbAreaTask::OnDestroy(bool bInOwnerFinished)
{
	UPMTMWMeleeUserComponent* MeleeUserComponent = IPMTMWMeleeUserInterface::Execute_NativeGetMeleeUserComponent(AbilitySystemComponent->GetAvatarActor());
	APMTMWMeleeWeapon* RodWeapon = MeleeUserComponent ? MeleeUserComponent->GetMeleeWeaponByHandle(Cast<UPMTCGameplayAbilityBase>(Ability.Get())->GetGrantHandle()) : nullptr;
	UUNDPurifyingBehaviorComponent* PurifyAbsorbComponent = RodWeapon ? RodWeapon->GetBehaviorByClass<UUNDPurifyingBehaviorComponent>() : nullptr;

	if (PurifyAbsorbComponent)
	{
		for (const TObjectPtr<AActor>& Actor : AbsorbedActors)
		{
			if (Actor)
			{
				UAbilitySystemComponent* HitASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(Actor);
				HitASC->InvokeGameplayCueEvent(UNDNativeTags::GameplayCueAbsorbFungiRod, EGameplayCueEvent::Removed,
					FGameplayCueParameters());
			}
		}
		PurifyAbsorbComponent->StopPurify();
	}
	
	AbsorbedActors.Reset();
	Super::OnDestroy(bInOwnerFinished);
}

UUNDPurifyingRodAbsorbAbility::UUNDPurifyingRodAbsorbAbility()
{
	FlashSectionName = TEXT("Flash");
	AbsorbMaxRadius.ValueByLevel.SetValue(500.0f);
	AbsorbInitialRadius.SetValue(200.0f);
	TimeToReachMaxRadius.SetValue(2.0f);
	MaxTargetsToAbsorb.SetValue(4.0f);
	bEndAbilityOnMontageCancelOrInterrupted = true;
	bEndAbilityOnMontageEndOrBlendout = true;
	EventToStartAbsorbing = UNDNativeTags::EventStartPurifyAbsorb;
	EventToTriggerFlash = UNDNativeTags::EventPurifyRodFlash;
}

void UUNDPurifyingRodAbsorbAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                                    const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                                    const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	if (!IsAbilityActive(Handle, ActorInfo))
	{
		return;
	}

	if (UPMTMWMeleeUserComponent* MeleeUserComponent = IPMTMWMeleeUserInterface::Execute_NativeGetMeleeUserComponent(ActorInfo->AvatarActor.Get()))
	{
		PreviousMeleeWeapons = MeleeUserComponent->GetAllActiveMeleeWeaponHandles();
		MeleeUserComponent->RequestMeleeWeaponActivationOrDeactivation(GetAbilityGrantHandle(Handle, ActorInfo), true, true);
	}

	WaitInputReleaseTask = UAbilityTask_WaitInputRelease::WaitInputRelease(this, false);
	WaitInputReleaseTask->OnRelease.AddDynamic(this, &UUNDPurifyingRodAbsorbAbility::OnInputReleased);
	WaitInputReleaseTask->ReadyForActivation();

	if (EventToTriggerFlash.IsValid())
	{
		WaitTriggerFlashEventTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this,
			EventToTriggerFlash, nullptr, true, true);
		WaitTriggerFlashEventTask->EventReceived.AddDynamic(this, &UUNDPurifyingRodAbsorbAbility::OnTriggerFlashEvent);
		WaitTriggerFlashEventTask->ReadyForActivation();
	}

	if (EventToStartAbsorbing.IsValid())
	{
		WaitStartAbsorbTask = UAbilityTask_WaitGameplayEvent::WaitGameplayEvent(this,
			EventToStartAbsorbing, nullptr, true, true);
		WaitStartAbsorbTask->EventReceived.AddDynamic(this, &UUNDPurifyingRodAbsorbAbility::OnStartAbsorbingEvent);
		WaitStartAbsorbTask->ReadyForActivation();
	}
	else
	{
		OnStartAbsorbingEvent(FGameplayEventData());
	}
}

void UUNDPurifyingRodAbsorbAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	if (UPMTMWMeleeUserComponent* MeleeUserComponent = IPMTMWMeleeUserInterface::Execute_NativeGetMeleeUserComponent(ActorInfo->AvatarActor.Get()))
	{
		for (const FPMTCSimpleHandle& PreviousWeapon : PreviousMeleeWeapons)
		{
			MeleeUserComponent->RequestMeleeWeaponActivationOrDeactivation(PreviousWeapon, true, true);
		}
	}
	PreviousMeleeWeapons.Reset();
	if (WaitTriggerFlashEventTask)
	{
		WaitTriggerFlashEventTask->EventReceived.RemoveDynamic(this, &UUNDPurifyingRodAbsorbAbility::OnTriggerFlashEvent);
		WaitTriggerFlashEventTask->EndTask();
		WaitTriggerFlashEventTask = nullptr;
	}
	if (WaitStartAbsorbTask)
	{
		WaitStartAbsorbTask->EventReceived.RemoveDynamic(this, &UUNDPurifyingRodAbsorbAbility::OnStartAbsorbingEvent);
		WaitStartAbsorbTask->EndTask();
		WaitStartAbsorbTask = nullptr;
	}
	if (PurifyAbsorbTask)
	{
		PurifyAbsorbTask->EndTask();
		PurifyAbsorbTask = nullptr;
	}
	if (WaitInputReleaseTask)
	{
		WaitInputReleaseTask->OnRelease.RemoveDynamic(this, &UUNDPurifyingRodAbsorbAbility::OnInputReleased);
		WaitInputReleaseTask->EndTask();
		WaitInputReleaseTask = nullptr;
	}
	StoredTimeHeld = 0.0f;
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UUNDPurifyingRodAbsorbAbility::OnInputReleased(float TimeHeld)
{
	if (PurifyAbsorbTask)
	{
		PurifyAbsorbTask->EndTask();
		PurifyAbsorbTask = nullptr;
	}
	StoredTimeHeld = TimeHeld;
	GetAbilitySystemComponentFromActorInfo()->CurrentMontageJumpToSection(FlashSectionName);
	if (!WaitTriggerFlashEventTask || !FlashGE)
	{
		OnTriggerFlashEvent(FGameplayEventData());
	} 
}

void UUNDPurifyingRodAbsorbAbility::OnTriggerFlashEvent(FGameplayEventData Payload)
{
	if (FlashGE)
	{
		const int32 AbilityLevel = GetAbilityLevel(CurrentSpecHandle, CurrentActorInfo);
		const float MaxRadius = K2_CalculateNonSpecValue(AbsorbMaxRadius);
		const float InitialRadius = AbsorbInitialRadius.TryGetValueAtLevel(AbilityLevel);
		const float TimeForMaxRadius = TimeToReachMaxRadius.TryGetValueAtLevel(AbilityLevel);
	
		const float ActualRadius = FMath::Lerp(
			InitialRadius, MaxRadius, TimeForMaxRadius > 0.0f ? FMath::Min(StoredTimeHeld / TimeForMaxRadius, 1.0f) : 1.0f);
		AbsorbDetectionTrace.TraceStart = GetAvatarActorFromActorInfo()->GetActorLocation();
		AbsorbDetectionTrace.TraceEnd = AbsorbDetectionTrace.TraceStart + FVector(0.1);
		AbsorbDetectionTrace.CapsuleOrSphereRadius = ActualRadius;
		AbsorbDetectionTrace.BoxExtent = AbsorbDetectionTrace.BoxExtent.GetSafeNormal() * ActualRadius;
		FGameplayEffectSpecHandle SpecHandle = GetAbilitySystemComponentFromActorInfo()->MakeOutgoingSpec(
			FlashGE, AbilityLevel, FGameplayEffectContextHandle());

		TArray<FHitResult> HitActors = AbsorbDetectionTrace.MakeTrace(GetAvatarActorFromActorInfo());
		for (const FHitResult& HitResult : HitActors)
		{
			if (UAbilitySystemComponent* HitASC = UPMTCAbilitySystemFunctionLibrary::GetAbilitySystemComponent(HitResult.GetActor()))
			{
				GetAbilitySystemComponentFromActorInfo()->ApplyGameplayEffectSpecToTarget(*SpecHandle.Data, HitASC);
			}
		}
	}

	if (!IsPlayingMontage())
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
	}
}

void UUNDPurifyingRodAbsorbAbility::OnStartAbsorbingEvent(FGameplayEventData Payload)
{
	const int32 AbilityLevel = GetAbilityLevel(CurrentSpecHandle, CurrentActorInfo);
	const float InitialRadius = AbsorbInitialRadius.TryGetValueAtLevel(AbilityLevel);
	const float MaxRadius = K2_CalculateNonSpecValue(AbsorbMaxRadius);
	const float TimeForMaxRadius = TimeToReachMaxRadius.TryGetValueAtLevel(AbilityLevel);
	const FGameplayEffectSpecHandle SpecHandle = MakeSpecHandleFromSetByCallerConfig(AbsorbGE);
	PurifyAbsorbTask = UUNDPurifyingAbsorbAreaTask::StartPurifyTask(
	this, TargetFilter, MaxTargetsToAbsorb.TryGetValueAtLevel(AbilityLevel),
		AbsorbDetectionTrace, SpecHandle, MaxRadius,
		InitialRadius, TimeForMaxRadius);
	
	PurifyAbsorbTask->ReadyForActivation();

	FGameplayCueParameters CueParameters;
	CueParameters.Instigator = GetAvatarActorFromActorInfo();
	CueParameters.Location.X = InitialRadius;
	CueParameters.Location.Y = MaxRadius;
	CueParameters.RawMagnitude = TimeForMaxRadius;

	K2_AddGameplayCueWithParams(UNDNativeTags::GameplayCueAbsorbAreaFungiRod, CueParameters, true);
}
