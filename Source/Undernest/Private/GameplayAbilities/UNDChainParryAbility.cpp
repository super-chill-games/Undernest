﻿// Copyright Super Chill Games. All Rights Reserved.


#include "GameplayAbilities/UNDChainParryAbility.h"

// PMTC Includes
#include "Utilities/PMTCUtilityFunctionLibrary.h"

// UND Includes
#include "Abilities/Tasks/AbilityTask_WaitDelay.h"
#include "Abilities/Tasks/PMTCTickAbilityTask.h"
#include "ActorComponents/PMTCCharacterMovementComponent.h"
#include "FunctionLibraries/PMTCAbilitySystemBlueprintLib.h"
#include "GameFramework/Character.h"
#include "Gameplay/UNDParrySplineActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Movement/PMTCSplineMovementExtension.h"
#include "Types/UNDNativeTags.h"

UUNDChainParryAbility::UUNDChainParryAbility()
{
	bTickAbility = true;
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	TimeToPressInput.ValueByLevel.Value = 0.25f;
	DetectionConfig.TraceMode = EPMTCTraceMode::Overlap;
	DetectionConfig.TraceType = EPMTCTraceType::ObjectTypes;
	DetectionConfig.ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
	DetectionConfig.TraceShapeType = EPMTCTraceShapeType::Sphere;
	DetectionConfig.CapsuleOrSphereRadius = 500.0f;
	DetectionConfig.bMultiTrace = true;
	ActorFilter.WhitelistActorClasses.Add(APawn::StaticClass());
	ActorFilter.ExclusionTags.AddTag(UNDNativeTags::StateFungiStun);
	MaxChain.ValueByLevel.Value = 3;
	InputCue = UNDNativeTags::GameplayCueParryChainInput;
	SplineActorClass = AUNDParrySplineActor::StaticClass();

	FAbilityTriggerData TriggerData;
	TriggerData.TriggerTag = UNDNativeTags::TriggerAbilityParryChain;
	TriggerData.TriggerSource = EGameplayAbilityTriggerSource::GameplayEvent;
	AbilityTriggers.Emplace(TriggerData);
}

bool UUNDChainParryAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags,
	const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if (InputsToListen.Num() == 0)
	{
		return false;
	}
	if (ActorInfo->MovementComponent.IsValid())
	{
		if (const UPMTCCharacterMovementComponent* MovementComponent = Cast<UPMTCCharacterMovementComponent>(ActorInfo->MovementComponent.Get()))
		{
			if (MovementComponent->GetMovementExtensionByClass(UPMTCSplineMovementExtension::StaticClass()) == nullptr)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	return Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags);
}

void UUNDChainParryAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	DetectionConfig.TraceDistance = CalculateNonSpecValue(Handle, ActorInfo, MaxDistance);
	Target = FindNextAvailableActor();
	if (!Target)
	{
		CancelAbility(Handle, ActorInfo, ActivationInfo, false);
		return;
	}

	DisplayChainInput();

	WaitInputEvents = UPMTCWaitInputEvents::WaitMultipleInputs(this, GetEnhancedInputComponent(ActorInfo), InputsToListen, false);
	WaitInputEvents->OnInputEvent.AddDynamic(this, &UUNDChainParryAbility::OnInputPressed);
	WaitInputEvents->ReadyForActivation();

	if (bHasTimeLimit)
	{
		TimeLimitTask = UAbilityTask_WaitDelay::WaitDelay(this, K2_CalculateNonSpecValue(TimeLimit));
		TimeLimitTask->OnFinish.AddDynamic(this, &UUNDChainParryAbility::OnTimeLimitReached);
		TimeLimitTask->ReadyForActivation();
	}
}

void UUNDChainParryAbility::EndAbility(const FGameplayAbilitySpecHandle Handle,
	const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
	bool bReplicateEndAbility, bool bWasCancelled)
{
	UPMTCAbilitySystemBlueprintLib::RemoveActiveEffectsAndReset(GetAbilitySystemComponentFromActorInfo(), FlyingEffects);
	if (WaitInputEvents)
	{
		WaitInputEvents->OnInputEvent.RemoveDynamic(this, &UUNDChainParryAbility::OnInputPressed);
		WaitInputEvents->EndTask();
		WaitInputEvents = nullptr;
	}

	if (WaitInputPressTask)
	{
		WaitInputPressTask->OnFinish.RemoveDynamic(this, &UUNDChainParryAbility::OnInputTimeFinished);
		WaitInputPressTask->EndTask();
		WaitInputPressTask = nullptr;
	}

	if (TimeLimitTask)
	{
		TimeLimitTask->OnFinish.RemoveDynamic(this, &UUNDChainParryAbility::OnTimeLimitReached);
		TimeLimitTask->EndTask();
		TimeLimitTask = nullptr;
	}

	const UCharacterMovementComponent* MovementComponent = GetCharacterMovementComponent();
	if (MovementComponent->GetCharacterOwner()->MovementModeChangedDelegate.IsAlreadyBound(this, &UUNDChainParryAbility::OnSplineFlightFinished))
	{
		MovementComponent->GetCharacterOwner()->MovementModeChangedDelegate.RemoveDynamic(this, &UUNDChainParryAbility::OnSplineFlightFinished);
	}

	// In case something kill us, make sure to not have any remainder with the rotation.
	FRotator CurrentRotation = GetAvatarActorFromActorInfo()->GetActorRotation();
	CurrentRotation.Pitch = 0.0;
	CurrentRotation.Roll = 0.0;
	GetAvatarActorFromActorInfo()->SetActorRotation(CurrentRotation);

	if (SplineActor)
	{
		SplineActor->Destroy();
		SplineActor = nullptr;
	}
	Target = nullptr;
	bIgnoreInputs = false;
	bIsFlying = false;
	ChainCount = 0;
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

AActor* UUNDChainParryAbility::FindNextAvailableActor()
{
	if (ChainCount == K2_CalculateNonSpecValue(MaxChain))
	{
		return nullptr;
	}
	DetectionConfig.TraceStart = GetAvatarActorFromActorInfo()->GetActorLocation();
	DetectionConfig.TraceEnd = DetectionConfig.TraceStart + FVector(1.0f);
	TArray<FHitResult> ActorHits = DetectionConfig.MakeTrace(GetAvatarActorFromActorInfo());
	ActorHits = UPMTCUtilityFunctionLibrary::RemoveDuplicateComponentHits(ActorHits);
	
	for (const FHitResult& Hit : ActorHits)
	{
		if (ActorFilter.PassesFilter(*Hit.GetActor()))
		{
			return Hit.GetActor();
		}
	}
	return nullptr;
}

TSoftObjectPtr<UInputAction> UUNDChainParryAbility::RequestNextInput() const
{
	const int32 RandomIndex = FMath::RandRange(0, InputsToListen.Num() - 1);
	return InputsToListen[RandomIndex].Action;
}

void UUNDChainParryAbility::TickAbility_Implementation(float DeltaTime)
{
	Super::TickAbility_Implementation(DeltaTime);
	// If already flying, then do not do anything until we arrive to the target.
	if (SplineActor)
	{
		return;
	}
	bool bTargetChanged = false;
	if (Target)
	{
		if (!ActorFilter.PassesFilter(*Target))
		{
			Target = FindNextAvailableActor();
			if (!Target)
			{
				CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false);
				return;
			}
			bTargetChanged = true;
		}
	}
	else
	{
		Target = FindNextAvailableActor();
		if (!Target)
		{
			CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false);
			return;
		}
		bTargetChanged = true;
	}

	const bool bHasPreventChainTags = GetAbilitySystemComponentFromActorInfo()->HasAnyMatchingGameplayTags(PreventChainOwnerTags);
	bIgnoreInputs = bIsFlying || bHasPreventChainTags;
	if (bTargetChanged || bHasPreventChainTags)
	{
		K2_RemoveGameplayCue(InputCue);
	}

	if (bTargetChanged || (!bHasPreventChainTags && !TrackedGameplayCues.Contains(InputCue)))
	{
		const float TimeElapsed = GetAvatarActorFromActorInfo()->GetWorld()->GetTimeSeconds() - InputDisplayTimestamp;

		FGameplayCueParameters CueParameters;
		CueParameters.SourceObject = ChainInput.LoadSynchronous();
		CueParameters.EffectContext = CurrentActorInfo->AbilitySystemComponent->MakeEffectContext();
		CueParameters.EffectContext.AddInstigator(GetAvatarActorFromActorInfo(), Target);
		CueParameters.RawMagnitude = K2_CalculateNonSpecValue(TimeToPressInput) - TimeElapsed;

		K2_AddGameplayCueWithParams(InputCue, CueParameters);
	}
}

void UUNDChainParryAbility::FlyToTarget(AActor* InTarget)
{
	if (!InTarget || InTarget->IsActorBeingDestroyed())
	{
		CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false);
		return;
	}

	bIsFlying = true;
	bIgnoreInputs = true;
	UPMTCCharacterMovementComponent* MovementComponent = GetCharacterMovementComponent<UPMTCCharacterMovementComponent>();
	UPMTCSplineMovementExtension* SplineMovementExtension = MovementComponent->GetMovementExtensionByClass<UPMTCSplineMovementExtension>();
	MovementComponent->GetCharacterOwner()->MovementModeChangedDelegate.AddDynamic(this, &UUNDChainParryAbility::OnSplineFlightFinished);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = GetAvatarActorFromActorInfo();
	FTransform Transform = GetAvatarActorFromActorInfo()->GetActorTransform();
	Transform.SetRotation(FQuat::Identity);
	SplineActor = GetAvatarActorFromActorInfo()->GetWorld()->SpawnActor<AUNDParrySplineActor>(SplineActorClass,
		Transform, SpawnParameters);
	SplineMovementExtension->MoveThroughSpline(SplineActor->GenerateFlyingPath(GetAvatarActorFromActorInfo(), Target), FPMTCSplineMovementParams());
	++ChainCount;
	FlyingEffects = FPMTCSetByCallerGameplayEffectConfig::ApplyGenericEffects(*GetAbilitySystemComponentFromActorInfo(), EffectsWhileFlying);
}

void UUNDChainParryAbility::DisplayChainInput()
{
	const float TimeToPressKey = K2_CalculateNonSpecValue(TimeToPressInput);
	InputDisplayTimestamp = GetAvatarActorFromActorInfo()->GetWorld()->GetTimeSeconds();
	ChainInput = RequestNextInput();
	FGameplayCueParameters CueParameters;
	CueParameters.SourceObject = ChainInput.LoadSynchronous();
	CueParameters.EffectContext = CurrentActorInfo->AbilitySystemComponent->MakeEffectContext();
	CueParameters.EffectContext.AddInstigator(GetAvatarActorFromActorInfo(), Target);
	CueParameters.RawMagnitude = TimeToPressKey;

	K2_AddGameplayCueWithParams(InputCue, CueParameters);

	if (WaitInputPressTask)
	{
		WaitInputPressTask->OnFinish.RemoveDynamic(this, &ThisClass::OnInputTimeFinished);
		WaitInputPressTask->EndTask();
	}
	WaitInputPressTask = UAbilityTask_WaitDelay::WaitDelay(this, TimeToPressKey);
	WaitInputPressTask->OnFinish.AddDynamic(this, &ThisClass::OnInputTimeFinished);
	WaitInputPressTask->ReadyForActivation();
}

void UUNDChainParryAbility::OnInputPressed(const UInputAction* Input,
	ETriggerEvent Event, float EventTime, float TimeHeld)
{
	if (bIgnoreInputs || bIsFlying)
	{
		return;
	}
	if (TSoftObjectPtr<UInputAction>(Input) != ChainInput)
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
	}
	else
	{
		K2_RemoveGameplayCue(InputCue);
		FlyToTarget(Target);
	}
}

void UUNDChainParryAbility::OnSplineFlightFinished(ACharacter* Character, EMovementMode PrevMovementMode,
	uint8 PreviousCustomMode)
{
	bIsFlying = false;
	bool bFound = true;
	const uint8 SplineMovementMode =
		UPMTCMovementComponentExtension::GetCustomMovementModeFromExtensionClass(GetCharacterMovementComponent(),
			UPMTCSplineMovementExtension::StaticClass(), bFound);
	if (PrevMovementMode == EMovementMode::MOVE_Custom && PreviousCustomMode == SplineMovementMode)
	{
		Character->MovementModeChangedDelegate.RemoveDynamic(this, &UUNDChainParryAbility::OnSplineFlightFinished);
		HandleFlightFinished();
	}
	UPMTCAbilitySystemBlueprintLib::RemoveActiveEffectsAndReset(GetAbilitySystemComponentFromActorInfo(), FlyingEffects);
	if (Target)
	{
		FRotator Rotator =
			UKismetMathLibrary::FindLookAtRotation(GetAvatarActorFromActorInfo()->GetActorLocation(), Target->GetActorLocation());
		Rotator.Pitch = 0.0;
		Rotator.Roll = 0.0;
		GetAvatarActorFromActorInfo()->SetActorRotation(Rotator);
	}
}

void UUNDChainParryAbility::HandleFlightFinished()
{
	SplineActor->Destroy();
	SplineActor = nullptr;

	bIsFlying = false;
	Target = FindNextAvailableActor();
	if (!Target)
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
		return;
	}
	DisplayChainInput();
}

void UUNDChainParryAbility::OnInputTimeFinished()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}

void UUNDChainParryAbility::OnTimeLimitReached()
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, false, false);
}
