// Copyright Super Chill Games. All Rights Reserved.


#include "Characters/UNDCharacter.h"

// PMTC Includes
#include "Abilities/PMTCAbilitySystemComponent.h"
#include "Input/PMTCEnhancedInputComponent.h"
#include "Types/PMTCNativeTags.h"

// PMTW Includes
#include "UI/PMTWHUDBase.h"

// UND Includes
#include "Attributes/UNDDamageMetaAttributeSet.h"
#include "Attributes/UNDHealthAttributeSet.h"

// UE Includes
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Types/UNDNativeTags.h"

AUNDCharacter::AUNDCharacter(const FObjectInitializer& InObjectInitializer) : Super(InObjectInitializer)
{
	HealthAttributeSetClass = UUNDHealthAttributeSet::StaticClass();
	DamageAttributeSetClass = UUNDDamageMetaAttributeSet::StaticClass();
}

void AUNDCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (DefaultInputConfig)
	{
		if (UEnhancedInputComponent* EnhancedInputComp = Cast<UEnhancedInputComponent>(PlayerInputComponent))
		{
			UPMTCInputConfig::BindNativeAction(EnhancedInputComp, DefaultInputConfig, PMTCNativeTags::InputTagMove, this, &AUNDCharacter::Input_Move, ETriggerEvent::Triggered);
			UPMTCInputConfig::BindNativeAction(EnhancedInputComp, DefaultInputConfig, PMTCNativeTags::InputTagPause, this, &AUNDCharacter::Input_Pause, ETriggerEvent::Triggered);
		}
	}
}

bool AUNDCharacter::HasMovementBlocked() const
{
	return IPMTCDamageableInterface::Execute_IsDamageableDestroyed(this) ||
		GetAbilitySystemComponent()->HasMatchingGameplayTag(PMTCNativeTags::StateMovementBlocked) ||
		GetAbilitySystemComponent()->HasMatchingGameplayTag(UNDNativeTags::StateFungiStun);
}

void AUNDCharacter::Input_Move(const FInputActionValue& InputActionValue)
{
	if (HasMovementBlocked())
	{
		return;
	}
	const FVector2D Value = InputActionValue.Get<FVector2D>();
	if (Value.X != 0.0)
	{
		AddMovementInput(FVector::RightVector, Value.X);
	}
	if (Value.Y != 0.0)
	{
		AddMovementInput(FVector::ForwardVector, Value.Y);
	}
}

void AUNDCharacter::Input_Pause(const FInputActionValue& InputActionValue)
{
	if (APlayerController* OwningPC = GetController<APlayerController>())
	{
		if (APMTWHUDBase* HUD = OwningPC->GetHUD<APMTWHUDBase>())
		{
			HUD->CreatePauseWidget();
			UGameplayStatics::SetGamePaused(this, true);
		}
	}
}
