// Copyright Super Chill Games. All Rights Reserved.

using UnrealBuildTool;

public class Undernest : ModuleRules
{
	public Undernest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore"  });

		PrivateDependencyModuleNames.AddRange(new string[]
			{
				"AIModule",
				"CommonUI",
				"DeveloperSettings",
				"EnhancedInput",
				"GameplayAbilities",
				"GameplayTags",
				"GameplayTasks",
				"LevelSequence",
				"MotionWarping",
				"MovieScene",
				"NetCore",
				"Niagara",
				"PMTAI",
				"PMTAnim",
				"PMTAudio",
				"PMTCommon",
				"PMTDialogue",
				"PMTEnvironment",
				"PMTFiregunWeapons",
				"PMTGameplay",
				"PMTGASUtilities",
				"PMTGraphRuntime",
				"PMTInteractor",
				"PMTInventory",
				"PMTLocomotion",
				"PMTMeleeWeapons",
				"PMTMinimap",
				"PMTMissions",
				"PMTNPC",
				"PMTPickup",
				"PMTProgression",
				"PMTReadyToUse",
				"PMTSettings",
				"PMTTargetLock",
				"PMTTeams",
				"PMTUI",
				"PMTVFX",
				"PMTWidgets",
				"UMG",
				"Slate",
				"SlateCore",
			});
	}
}
