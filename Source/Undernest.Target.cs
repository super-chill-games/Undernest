// Copyright Super Chill Games. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UndernestTarget : TargetRules
{
	public UndernestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Undernest" } );
		IncludeOrderVersion = EngineIncludeOrderVersion.Latest;
	}
}
