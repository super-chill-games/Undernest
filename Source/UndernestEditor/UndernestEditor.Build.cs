// Copyright Super Chill Games. All Rights Reserved.

using UnrealBuildTool;

public class UndernestEditor : ModuleRules
{
	public UndernestEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine" });

		PrivateDependencyModuleNames.AddRange(new string[]
			{
                "DetailCustomizations",
                "EditorFramework",
				"EditorStyle",
				"PropertyEditor",
				"PMTGraphRuntime",
				"PMTGraphEditor",
                "Slate",
                "SlateCore",
				"Undernest",
                "UnrealEd",
			});
	}
}
