// Copyright Super Chill Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"

class UNDERNESTEDITOR_API FUndernestEditor : public IModuleInterface 
{
public:
	void StartupModule() override;
	void ShutdownModule() override;
};
