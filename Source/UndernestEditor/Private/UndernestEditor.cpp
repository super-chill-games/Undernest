// Copyright Super Chill Games. All Rights Reserved.

#include "UndernestEditor.h"

#include "Modules/ModuleManager.h"
#include "PropertyEditorModule.h"
#include "UnrealEdGlobals.h"

IMPLEMENT_MODULE(FUndernestEditor, UndernestEditor)

void FUndernestEditor::StartupModule()
{
	FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>(TEXT("PropertyEditor"));
	TSharedRef<FPropertySection> Section = PropertyModule.FindOrCreateSection("Object", "UND", NSLOCTEXT("UndernestEditor", "UNDSection", "UND"));
	Section->AddCategory(TEXT("UND"));
}

void FUndernestEditor::ShutdownModule()
{
}
