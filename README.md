# Undernest

## Link plugins

In our group, we have several plugins that have a collection of utilities and helpers to develop the game easier.
To avoid conflicts with git to push and update these plugins, we link those plugins and update the respective
repository. So:

1. Open CMD (Windows)
2. Go to the directory where you cloned the repository. For example, you can run `cd "C:\Undernest"`
3. Run `mklink /J Plugins <my_path_to_SCGPlugins>\Plugins`
4. That's it

## Build game

Once you have linked the plugins, you can now **right click Undernest.uproject** and select the option "**Generate Visual Studio project files**".

![Generating Visual Studio's solution](./Images/generate_solution.png)

Once that finishes, you can now open the file "**Undernest.sln**"